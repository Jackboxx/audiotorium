use core::panic;
use itertools::Itertools;
use reqwest::{header::CONTENT_TYPE, Client};
use std::{
    fmt::Display,
    net::{IpAddr, Ipv4Addr, SocketAddr},
    process::{exit, Command, Stdio},
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
    thread,
    time::Duration,
};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};
use uuid::Uuid;
use websocket::{ClientBuilder, OwnedMessage};

use audiotorium_core::{
    central_gateway::{
        commands::{CentralGatewayCommand, DownloadAudioParams},
        streams::CentralGatewayInfoStreamType,
    },
    download::{DownloadAudioRequest, DownloadRequestInfo},
    identifier::{youtube_content_type, AudioIdentifier, AudioKind, ItemUid, YoutubeContentType},
    node::{
        commands::{
            AddQueueItemParams, AudioNodeCommand, MoveQueueItemParams, PlaySelectedParams,
            RemoveQueueItemParams, SetAudioProgressParams, SetAudioVolumeParams,
        },
        streams::AudioNodeInfoStreamType,
    },
    tcp_messaging::{TcpMessage, TcpMessageSender, MESSAGE_MAX_SIZE},
};
use clap::{Parser, Subcommand, ValueEnum};

#[derive(Debug, Parser)]
#[command(author, version, about, long_about = None)]
pub struct CliArgs {
    #[command(subcommand)]
    pub action: Action,
    #[arg(short, long, default_value_t = String::from("0.0.0.0"))]
    /// IP address to connect to
    pub addr: String,
    #[arg(short, long)]
    /// Port to connect to
    pub port: u16,
    #[arg(short, long)]
    /// Only print URL and body instead of performing network actions
    pub dry_run: bool,
}

#[derive(Debug, Clone, Subcommand)]
pub enum Action {
    #[command(about = "Send a command")]
    Send {
        #[command(subcommand)]
        con_type: SendConnectionType,
    },
    #[command(about = "Send a command as a TCP message")]
    TcpMessage {
        #[command(subcommand)]
        target: TcpMessageTarget,
    },
    #[command(about = "Listen for data")]
    Listen {
        #[arg(short, long)]
        /// Command to run on received messages. None = print to stdout
        command: Option<String>,
        #[command(subcommand)]
        con_type: ListenConnectionType,
    },
    #[command(about = "Print the original value the uid was created from")]
    UidValue { uid: Arc<str> },
}

#[derive(Debug, Clone, Subcommand)]
pub enum ListenConnectionType {
    #[command(about = "Listen for information from an audio device")]
    Node {
        #[arg(short, long)]
        /// Name of the node to connect to
        source_name: String,
        #[arg(short, long, value_delimiter = ',')]
        /// List of information to listen for
        wanted_info: Vec<AudioNodeInfoStreamType>,
    },
    #[command(about = "Listen for information from the master server")]
    Gateway {
        #[arg(short, long, value_delimiter = ',')]
        /// List of information to listen for
        wanted_info: Vec<CentralGatewayInfoStreamType>,
    },
}

#[derive(Debug, Clone, Subcommand)]
pub enum SendConnectionType {
    #[command(about = "Send a command to the central gateway")]
    Gateway {
        #[command(subcommand)]
        cmd: CliCentralGatewayCommand,
    },
    #[command(about = "Send a command to an audio device")]
    Node {
        #[arg(short, long)]
        /// Name of the node to connect to
        source_name: String,
        #[command(subcommand)]
        cmd: CliNodeCommand,
    },
}

#[derive(Debug, Clone, Subcommand)]
pub enum TcpMessageTarget {
    #[command(about = "Send a command to the media downloader")]
    Downloader {
        #[command(subcommand)]
        msg: DownloaderMsg,
    },
    #[command(about = "Send a malformed tcp message to the targe handler")]
    Malformed {
        #[command(subcommand)]
        msg: MalformedMsg,
    },
}

#[derive(Debug, Clone, Subcommand)]
pub enum CliCentralGatewayCommand {
    DownloadAudio {
        #[arg(short, long)]
        identifier: String,
        #[arg(short, long)]
        local: bool,
    },
}

#[derive(Debug, Clone, Subcommand)]
pub enum CliNodeCommand {
    AddQueueItem {
        #[arg(short, long)]
        identifier: String,
        #[arg(short, long)]
        local: bool,
    },
    RemoveQueueItem {
        local_id: String,
    },
    MoveQueueItem {
        #[arg(short, long)]
        old_pos: usize,
        #[arg(short, long)]
        current_local_id: String,
        #[arg(short, long)]
        new_pos: usize,
        #[arg(short, long)]
        target_local_id: String,
    },
    ShuffleQueue,
    ClearQueue,
    SetAudioVolume {
        #[arg(short, long)]
        volume: f32,
    },
    SetAudioProgress {
        #[arg(short, long)]
        progress: f64,
    },
    PauseQueue,
    UnPauseQueue,
    PlayNext,
    PlayPrevious,
    PlaySelected {
        #[arg(short, long)]
        index: usize,
    },
}

#[derive(Debug, Clone, Subcommand)]
pub enum DownloaderMsg {
    DownloadAudioRequest {
        #[arg(short, long)]
        url: Arc<str>,
    },
}

#[derive(Debug, Clone, Subcommand)]
pub enum MalformedMsg {
    TooShort,
    TooLong,
    Mangled {
        #[arg(short, long)]
        valid_tag: Option<TcpRemoteType>,
    },
}

#[derive(Debug, Clone, ValueEnum)]
pub enum TcpRemoteType {
    Downloader,
}

impl Display for ListenConnectionType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let str = match self {
            Self::Gateway { wanted_info } => format!(
                "gateway?wanted_info={info}",
                info = wanted_info
                    .iter()
                    .map(|i| serde_json::to_string(i).unwrap().replace('"', ""))
                    .collect_vec()
                    .join(",")
            ),
            Self::Node {
                source_name,
                wanted_info,
            } => format!(
                "node/{source_name}?wanted_info={info}",
                info = wanted_info
                    .iter()
                    .map(|i| serde_json::to_string(i).unwrap().replace('"', ""))
                    .collect_vec()
                    .join(",")
            ),
        };

        write!(f, "{str}")
    }
}

impl Display for SendConnectionType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let str = match self {
            Self::Gateway { .. } => "gateway".to_owned(),
            Self::Node { source_name, .. } => format!("node/{source_name}"),
        };

        write!(f, "{str}")
    }
}

impl Action {
    fn get_prefix_and_endpoint(&self) -> (&str, &str) {
        match self {
            Self::Send { .. } => ("http", "commands"),
            Self::Listen { .. } => ("ws", "streams"),
            Self::TcpMessage { .. } => ("", ""),
            Self::UidValue { .. } => ("", ""),
        }
    }

    fn get_con_type_endpoint(&self) -> String {
        match self {
            Self::Listen { con_type, .. } => format!("{con_type}"),
            Self::Send { con_type } => format!("{con_type}"),
            Self::TcpMessage { .. } => Default::default(),
            Self::UidValue { .. } => Default::default(),
        }
    }
}

impl From<CliCentralGatewayCommand> for CentralGatewayCommand {
    fn from(value: CliCentralGatewayCommand) -> Self {
        match value {
            CliCentralGatewayCommand::DownloadAudio { identifier, local } => {
                if local {
                    CentralGatewayCommand::DownloadAudio(DownloadAudioParams {
                        identifier: AudioIdentifier::Local {
                            uid: identifier.into(),
                        },
                    })
                } else {
                    CentralGatewayCommand::DownloadAudio(DownloadAudioParams {
                        identifier: AudioIdentifier::Youtube {
                            raw_url: identifier.into(),
                        },
                    })
                }
            }
        }
    }
}

impl From<CliNodeCommand> for AudioNodeCommand {
    fn from(value: CliNodeCommand) -> Self {
        match value {
            CliNodeCommand::AddQueueItem { identifier, local } => {
                if local {
                    AudioNodeCommand::AddQueueItem(AddQueueItemParams {
                        identifier: AudioIdentifier::Local {
                            uid: identifier.into(),
                        },
                    })
                } else {
                    AudioNodeCommand::AddQueueItem(AddQueueItemParams {
                        identifier: AudioIdentifier::Youtube {
                            raw_url: identifier.into(),
                        },
                    })
                }
            }
            CliNodeCommand::RemoveQueueItem { local_id } => {
                AudioNodeCommand::RemoveQueueItem(RemoveQueueItemParams {
                    local_id: Uuid::parse_str(&local_id).unwrap(),
                })
            }
            CliNodeCommand::MoveQueueItem {
                old_pos,
                new_pos,
                target_local_id,
                current_local_id,
            } => AudioNodeCommand::MoveQueueItem(MoveQueueItemParams {
                old_pos,
                new_pos,
                target_local_id: Uuid::parse_str(&target_local_id).unwrap(),
                current_local_id: Uuid::parse_str(&current_local_id).unwrap(),
            }),
            CliNodeCommand::ShuffleQueue => AudioNodeCommand::ShuffleQueue,
            CliNodeCommand::ClearQueue => AudioNodeCommand::ClearQueue,
            CliNodeCommand::SetAudioVolume { volume } => {
                AudioNodeCommand::SetAudioVolume(SetAudioVolumeParams { volume })
            }
            CliNodeCommand::SetAudioProgress { progress } => {
                AudioNodeCommand::SetAudioProgress(SetAudioProgressParams { progress })
            }
            CliNodeCommand::PauseQueue => AudioNodeCommand::PauseQueue,
            CliNodeCommand::UnPauseQueue => AudioNodeCommand::UnPauseQueue,
            CliNodeCommand::PlayNext => AudioNodeCommand::PlayNext,
            CliNodeCommand::PlayPrevious => AudioNodeCommand::PlayPrevious,
            CliNodeCommand::PlaySelected { index } => {
                AudioNodeCommand::PlaySelected(PlaySelectedParams { index })
            }
        }
    }
}

fn get_url(action: &Action, addr: String, port: u16) -> String {
    let (prefix, action_endpoint) = action.get_prefix_and_endpoint();
    let con_endpoint = action.get_con_type_endpoint();

    format!(
        "{prefix}://{addr}:{port}/{action_endpoint}/{con_endpoint}",
        addr = addr,
        port = port,
    )
}
fn get_body_as_str(action: SendConnectionType) -> String {
    match action {
        SendConnectionType::Node { cmd, .. } => {
            serde_json::to_string::<AudioNodeCommand>(&cmd.into()).unwrap()
        }
        SendConnectionType::Gateway { cmd } => {
            serde_json::to_string::<CentralGatewayCommand>(&cmd.into()).unwrap()
        }
    }
}

async fn send_command(url: &str, body: String) -> Result<String, reqwest::Error> {
    let client = Client::new();
    let res = client
        .post(url)
        .body(body)
        .header(CONTENT_TYPE, "application/json")
        .send()
        .await?;

    res.text().await
}

fn listen_on_socket(url: &str, cmd_str: Option<String>) {
    let client = ClientBuilder::new(url)
        .unwrap()
        .add_protocol("rust-websocket")
        .connect_insecure()
        .unwrap();

    let (mut receiver, _) = client.split().unwrap();
    let heart_beat_received = Arc::new(AtomicBool::new(true));

    let heart_beat_received_clone = heart_beat_received.clone();
    let max_ms_without_heart_beat = 600;

    thread::spawn(move || loop {
        let received = heart_beat_received_clone.swap(false, Ordering::AcqRel);
        if !received {
            eprintln!("didn't reveice heart beat ping in the last {max_ms_without_heart_beat}ms closing session");
            exit(1);
        };

        thread::sleep(Duration::from_millis(max_ms_without_heart_beat));
    });

    for message in receiver.incoming_messages() {
        match message {
            Ok(OwnedMessage::Text(text)) => match cmd_str {
                Some(ref cmd_str) => {
                    let (cmd, args) = cmd_str.split_once(' ').unwrap_or((cmd_str, ""));

                    let echo_cmd = Command::new("echo")
                        .arg(text)
                        .stdout(Stdio::piped())
                        .spawn()
                        .unwrap();

                    let cmd = Command::new(cmd)
                        .arg(args)
                        .stdin(Stdio::from(echo_cmd.stdout.unwrap()))
                        .spawn()
                        .unwrap();

                    let out = cmd.wait_with_output().expect("Failed to read stdout");

                    println!("{}", String::from_utf8_lossy(&out.stdout))
                }
                None => {
                    println!("{text}");
                }
            },
            Ok(OwnedMessage::Ping(msg)) => {
                if msg == b"heart-beat" {
                    heart_beat_received.swap(true, Ordering::AcqRel);
                }
            }
            Ok(OwnedMessage::Close(_)) => return,
            _ => {}
        }
    }
}

async fn handle_tcp_msg(target: TcpMessageTarget, ip: String, port: u16) {
    let ip: Ipv4Addr = ip.parse().unwrap();
    let addr = SocketAddr::new(IpAddr::V4(ip), port);

    match target {
        TcpMessageTarget::Downloader { msg } => match msg {
            DownloaderMsg::DownloadAudioRequest { url } => {
                let request_info = match youtube_content_type(url.as_ref()) {
                    YoutubeContentType::Video(url) => DownloadRequestInfo::YoutubeVideo { url },
                    YoutubeContentType::Playlist(url) => {
                        DownloadRequestInfo::YoutubePlaylist { url }
                    }
                    YoutubeContentType::Invalid { reason } => panic!("invalid url\n{reason}"),
                };

                let request = DownloadAudioRequest::new(None, request_info);
                addr.send(&request).await.unwrap();
            }
        },
        TcpMessageTarget::Malformed { msg } => {
            use tokio::io::AsyncWriteExt;
            match msg {
                MalformedMsg::TooShort => {
                    let mut stream = tokio::net::TcpStream::connect(addr).await.unwrap();
                    stream.write_all(&[255]).await.unwrap();
                }
                MalformedMsg::TooLong => {
                    let mut stream = tokio::net::TcpStream::connect(addr).await.unwrap();
                    let bytes = [0; MESSAGE_MAX_SIZE + 1];
                    stream.write_all(&bytes).await.unwrap();
                }
                MalformedMsg::Mangled { valid_tag } => {
                    let mut stream = tokio::net::TcpStream::connect(addr).await.unwrap();
                    let mut bytes: [u8; 1024] = [0; 1024];
                    for byte in &mut bytes {
                        *byte = rand::random();
                    }

                    #[allow(clippy::single_match)]
                    match valid_tag {
                        Some(TcpRemoteType::Downloader) => {
                            let tag = DownloadAudioRequest::TAG;
                            let tag_bytes = tag.to_be_bytes();

                            bytes[0] = tag_bytes[0];
                            bytes[1] = tag_bytes[1];
                            bytes[2] = tag_bytes[2];
                            bytes[3] = tag_bytes[3];
                        }
                        _ => {}
                    };

                    stream.write_all(&bytes).await.unwrap();
                }
            }
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), &'static str> {
    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(tracing_subscriber::EnvFilter::from_default_env())
        .init();

    let args = CliArgs::parse();

    let url = get_url(&args.action, args.addr.clone(), args.port);

    if args.dry_run {
        println!("{url}");
        if let Action::Send { con_type } = args.action {
            let str_body = get_body_as_str(con_type);
            println!("{str_body}");
        }
    } else {
        match args.action {
            Action::Send { con_type } => {
                let body = get_body_as_str(con_type);

                let out = send_command(&url, body).await.unwrap();
                println!("{out}");
            }
            Action::Listen { command, .. } => {
                listen_on_socket(&url, command);
            }
            Action::TcpMessage { target } => {
                handle_tcp_msg(target, args.addr, args.port).await;
            }
            Action::UidValue { uid } => {
                let uid = ItemUid::new(uid);

                let kind = AudioKind::from_uid(&uid).expect("invalid uid provided");
                let prefix = kind.prefix();

                let uid_str = uid.as_ref().trim_start_matches(prefix);
                let decoded = hex::decode(uid_str).unwrap();
                let original = String::from_utf8_lossy(&decoded);

                println!("{original}")
            }
        }
    }

    Ok(())
}
