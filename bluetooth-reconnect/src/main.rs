use std::io;

use bluer::{DiscoveryFilter, DiscoveryTransport};

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<(), io::Error> {
    let session = bluer::Session::new().await?;
    let adapter = session.default_adapter().await?;
    println!("INFO: Using Bluetooth adapter '{}'", adapter.name());

    adapter.set_powered(true).await?;
    adapter
        .set_discovery_filter(DiscoveryFilter {
            transport: DiscoveryTransport::Auto,
            ..Default::default()
        })
        .await?;

    loop {
        tokio::time::sleep(std::time::Duration::from_secs(3)).await;

        for addr in adapter.device_addresses().await? {
            let dev = adapter.device(addr)?;

            let is_trusted = dev.is_trusted().await?;
            let is_connected = dev.is_connected().await?;
            if is_trusted && !is_connected {
                println!("INFO: Trying to reconnect device with addresse {addr}.");
                if let Err(err) = dev.connect().await {
                    eprintln!("ERROR: Failed to reconnect device with addresse {addr} - {err}")
                } else {
                    println!("INFO: Successfully reconnected to device with addresse {addr}")
                }
            }
        }
    }
}
