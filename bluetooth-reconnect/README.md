## Usage

Compile a binary with Rust and copy it to `/usr/bin/bluetooth-reconnect`

```sh
cargo build --release
cp ./target/release/bluetooth-reconnect /usr/bin/bluetooth-reconnect
```

Add a new systemd service to run `bluetooth-reconnect` in the background

```sh
cp bluetooth-reconnect.service /etc/systemd/system/bluetooth-reconnect.service
systemctl enable --now bluetooth-reconnect.service
```
