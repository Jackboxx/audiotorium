use std::sync::Arc;

use audiotorium_core::{
    error::{AppError, AppErrorKind, IntoAppError},
    external_apis::youtube::{
        YoutubeSearchResult, YoutubeSnippet, YoutubeThumbnail, YoutubeThumbnails,
    },
    identifier::{YoutubePlaylistUrl, YoutubeVideoUrl},
    wrappers::OptionArcStr,
};
use chromiumoxide::Browser;
use scraper::{Html, Selector};

const SEARCH_VIDEO_FILTER_PARAM: &str = "sp=EgIQAQ%253D%253D,";

const THUMBNAIL_QUALITIES: &[&str] = &[
    "maxresdefault.jpg",
    "sddefault.jpg",
    "hqdefault.jpg",
    "mqdefault.jpg",
    "default.jpg",
];

#[derive(Debug)]
struct YoutubeScrapeSearchItem {
    pub kind: ScrapedAudioSearchKind,
    pub url: Arc<str>,
    pub name: OptionArcStr,
    pub author: OptionArcStr,
    pub cover_art_url: OptionArcStr,
}

#[derive(Debug)]
enum ScrapedAudioSearchKind {
    Track,
    Playlist,
}

pub async fn scrape_youtube_search_results(
    browser: &Browser,
    search: &str,
    limit: usize,
    disable_filter: bool,
) -> Result<Vec<YoutubeSearchResult>, AppError> {
    let url = search_url_from_query(search, disable_filter);

    let page = browser.new_page(url).await.into_app_err(
        "failed to fetch data from youtube",
        AppErrorKind::WebScraper,
        &[&format!("SEARCH: {search}"), &format!("LIMIT: {limit}")],
    )?;

    page.wait_for_navigation().await.into_app_err(
        "failed to fetch data from youtube",
        AppErrorKind::WebScraper,
        &[&format!("SEARCH: {search}"), &format!("LIMIT: {limit}")],
    )?;

    let raw_html = page.content().await.into_app_err(
        "failed to fetch data from youtube",
        AppErrorKind::WebScraper,
        &[&format!("SEARCH: {search}"), &format!("LIMIT: {limit}")],
    )?;

    page.close().await.into_app_err(
        "failed to fetch data from youtube",
        AppErrorKind::WebScraper,
        &[&format!("SEARCH: {search}"), &format!("LIMIT: {limit}")],
    )?;

    let scraped_search_results = parse_search_results_page(raw_html, limit).await;
    Ok(scraped_search_results
        .into_iter()
        .map(into_search_result)
        .collect())
}

async fn parse_search_results_page(
    raw_html: String,
    limit: usize,
) -> Vec<Option<YoutubeScrapeSearchItem>> {
    let videos = {
        let html = Html::parse_document(&raw_html);
        let selector = Selector::parse("ytd-video-renderer,ytd-playlist-renderer")
            .expect("should be a valid selector");

        html.select(&selector)
            .take(limit)
            .map(|video| return video.html())
            .collect::<Vec<_>>()
    };

    let mut results = Vec::with_capacity(limit);

    for html in videos {
        let item = parse_search_result(html).await;

        results.push(item);
    }

    results
}

async fn parse_search_result(raw_html: String) -> Option<YoutubeScrapeSearchItem> {
    let (id, url, kind, name, author) = {
        let html = Html::parse_fragment(&raw_html);
        let (url, kind) = parse_search_result_kind(&html)?;

        match kind {
            ScrapedAudioSearchKind::Track => {
                let id = extract_watch_id(&url);

                let title = search_parsing::VIDEO_TITLE.parse(&html).ok().into();
                let channel_title = search_parsing::VIDEO_CHANNEL_TITLE.parse(&html).ok().into();
                (id, url, kind, title, channel_title)
            }
            ScrapedAudioSearchKind::Playlist => {
                let first_video_url = search_parsing::PLAYLIST_FIRST_VIDEO_URL
                    .parse(&html)
                    .ok()
                    .map(|url| format!("https://www.youtube.com{url}"));
                let video_id = first_video_url.as_deref().and_then(extract_watch_id);

                let title = search_parsing::VIDEO_TITLE.parse(&html).ok().into();
                let channel_title = search_parsing::PLAYLIST_CHANNEL_TITLE
                    .parse(&html)
                    .ok()
                    .and_then(|title| return title.split(" · Playlist").next().map(str::to_owned))
                    .into();

                (video_id, url, kind, title, channel_title)
            }
        }
    };

    let thumbnail_url = if let Some(id) = id {
        fetch_highest_quality_thumbnail_url(&id).await
    } else {
        None::<String>.into()
    };

    Some(YoutubeScrapeSearchItem {
        url,
        kind,
        name,
        author,
        cover_art_url: thumbnail_url,
    })
}

fn parse_search_result_kind(html: &Html) -> Option<(Arc<str>, ScrapedAudioSearchKind)> {
    if let Ok(url) = search_parsing::VIDEO_URL.parse(html) {
        return Some((
            Arc::from(format!("https://www.youtube.com{url}")),
            ScrapedAudioSearchKind::Track,
        ));
    }

    if let Ok(url) = search_parsing::PLAYLIST_URL.parse(html) {
        return Some((
            Arc::from(format!("https://www.youtube.com{url}")),
            ScrapedAudioSearchKind::Playlist,
        ));
    }

    return None;
}

fn search_url_from_query(query: &str, disable_filter: bool) -> String {
    let query = query.replace(' ', "+");
    let filter_param = if disable_filter {
        String::new()
    } else {
        format!("&{SEARCH_VIDEO_FILTER_PARAM}")
    };

    return format!("https://www.youtube.com/results?search_query={query}{filter_param}");
}

fn into_search_result(value: Option<YoutubeScrapeSearchItem>) -> YoutubeSearchResult {
    match value {
        Some(item) => match item.kind {
            ScrapedAudioSearchKind::Track => {
                let video_url = match YoutubeVideoUrl::try_new(item.url) {
                    Ok(url) => url,
                    Err(err) => {
                        return YoutubeSearchResult::Invalid {
                            reason: err.to_string().into(),
                        }
                    }
                };
                return YoutubeSearchResult::Video((
                    video_url,
                    YoutubeSnippet {
                        title: item.name.inner_as_ref().unwrap_or("").into(),
                        channel_title: item.author.inner_as_ref().unwrap_or("").into(),
                        thumbnails: YoutubeThumbnails {
                            maxres: item
                                .cover_art_url
                                .as_ref()
                                .map(|url| return YoutubeThumbnail { url: url.clone() }),
                            high: None,
                            medium: None,
                            default: None,
                        },
                    },
                ));
            }
            ScrapedAudioSearchKind::Playlist => {
                let playlist_url = match YoutubePlaylistUrl::try_new(item.url) {
                    Ok(url) => url,
                    Err(err) => {
                        return YoutubeSearchResult::Invalid {
                            reason: err.to_string().into(),
                        }
                    }
                };
                return YoutubeSearchResult::Playlist((
                    playlist_url,
                    YoutubeSnippet {
                        title: item.name.inner_as_ref().unwrap_or("").into(),
                        channel_title: item.author.inner_as_ref().unwrap_or("").into(),
                        thumbnails: YoutubeThumbnails {
                            maxres: item
                                .cover_art_url
                                .as_ref()
                                .map(|url| return YoutubeThumbnail { url: url.clone() }),
                            high: None,
                            medium: None,
                            default: None,
                        },
                    },
                ));
            }
        },
        None => {
            return YoutubeSearchResult::Invalid {
                reason: "failed to scrape youtube search result".into(),
            }
        }
    }
}

async fn fetch_highest_quality_thumbnail_url(id: &str) -> OptionArcStr {
    for quality in THUMBNAIL_QUALITIES {
        let url = format!("https://i.ytimg.com/vi/{id}/{quality}");
        if let Ok(resp) = reqwest::get(&url).await {
            if resp.status().is_success() || resp.status().is_redirection() {
                return Some(url).into();
            }
        }
    }

    None::<String>.into()
}

fn extract_watch_id(url: &str) -> Option<String> {
    let url = url::Url::parse(url).ok()?;
    return url
        .query_pairs()
        .flat_map(|(key, val)| return (key.eq("v")).then_some(val.to_string()))
        .next();
}

mod search_parsing {
    use crate::parser::{HtmlParsingSchema, HtmlSelector, ParsingEntry, TargetContent};

    pub const VIDEO_URL: HtmlParsingSchema = HtmlParsingSchema::new(ParsingEntry::Selector {
        selector: HtmlSelector::Id { id: "video-title" },
        next_entry: &ParsingEntry::Content {
            target: TargetContent::Attribute { attr: "href" },
        },
    });

    pub const PLAYLIST_URL: HtmlParsingSchema = HtmlParsingSchema::new(ParsingEntry::Selector {
        selector: HtmlSelector::Id { id: "view-more" },
        next_entry: &ParsingEntry::Selector {
            selector: HtmlSelector::Raw { selector: "a" },
            next_entry: &ParsingEntry::Content {
                target: TargetContent::Attribute { attr: "href" },
            },
        },
    });

    pub const VIDEO_TITLE: HtmlParsingSchema = HtmlParsingSchema::new(ParsingEntry::Selector {
        selector: HtmlSelector::Id { id: "video-title" },
        next_entry: &ParsingEntry::Content {
            target: TargetContent::Attribute { attr: "title" },
        },
    });

    pub const VIDEO_CHANNEL_TITLE: HtmlParsingSchema =
        HtmlParsingSchema::new(ParsingEntry::Selector {
            selector: HtmlSelector::Id { id: "channel-name" },
            next_entry: &ParsingEntry::Selector {
                selector: HtmlSelector::Raw {
                    selector: "yt-formatted-string",
                },
                next_entry: &ParsingEntry::Content {
                    target: TargetContent::Attribute { attr: "title" },
                },
            },
        });

    pub const PLAYLIST_FIRST_VIDEO_URL: HtmlParsingSchema =
        HtmlParsingSchema::new(ParsingEntry::Selector {
            selector: HtmlSelector::Id { id: "content" },
            next_entry: &ParsingEntry::Selector {
                selector: HtmlSelector::Raw { selector: "a" },
                next_entry: &ParsingEntry::Content {
                    target: TargetContent::Attribute { attr: "href" },
                },
            },
        });

    pub const PLAYLIST_CHANNEL_TITLE: HtmlParsingSchema =
        HtmlParsingSchema::new(ParsingEntry::Selector {
            selector: HtmlSelector::Raw {
                selector: "#text-container .ytd-channel-name",
            },
            next_entry: &ParsingEntry::Selector {
                selector: HtmlSelector::Raw {
                    selector: "yt-formatted-string",
                },
                next_entry: &ParsingEntry::Content {
                    target: TargetContent::Attribute { attr: "title" },
                },
            },
        });
}
