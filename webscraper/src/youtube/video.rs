use audiotorium_core::{
    error::{AppError, AppErrorKind, IntoAppError},
    external_apis::youtube::parse_iso_8601_duration,
    identifier::{Identifier, YoutubeVideoId, YoutubeVideoUrl},
    schema::audio::AudioTrackMetadata,
};
use scraper::{Html, Selector};

/// 'id' has to be a valid youtube video id
pub async fn scrape_youtube_video_metadata(id: &str) -> Result<AudioTrackMetadata, AppError> {
    let err_msg = "failed to fetch video metadata data from youtube";
    let err_kind = AppErrorKind::WebScraper;
    let detail = format!("VIDEO ID: {id}");
    let err_details = [detail.as_str()];

    let video_url = video_url_from_id(id);
    let raw_html = reqwest::get(&video_url)
        .await
        .into_app_err(err_msg, err_kind.clone(), &err_details)?
        .text()
        .await
        .into_app_err(err_msg, err_kind, &err_details)?;

    scrape_youtube_video(&raw_html, id)
}

fn scrape_youtube_video(raw_html: &str, id: &str) -> Result<AudioTrackMetadata, AppError> {
    let html = Html::parse_document(raw_html);
    let selector = Selector::parse("#watch7-content").expect("should be a valid selector");

    let Some(metadata_element) = html.select(&selector).next() else {
        return Err(AppError::new(
            AppErrorKind::WebScraper,
            "failed to scrape video metadata",
            &[
                "REASON: no video container found",
                &format!("SELECTOR: {selector:?}"),
                &format!("VIDEO ID: {id}"),
            ],
        ));
    };

    let metadata_html = Html::parse_fragment(&metadata_element.html());

    let title = video_parsing::TITLE.parse(&metadata_html)?;
    let channel_title = video_parsing::CHANNEL_TITLE.parse(&metadata_html)?;
    let thumbnail_url = video_parsing::THUMBNAIL_URL.parse(&metadata_html)?;
    let duration_iso_8601 = video_parsing::DURATION.parse(&metadata_html)?;
    let duration = parse_iso_8601_duration(&duration_iso_8601);

    return Ok(AudioTrackMetadata {
        identifier: YoutubeVideoUrl::from(YoutubeVideoId::new(id)).uid(),
        name: Some(title).into(),
        author: Some(channel_title).into(),
        duration: duration.and_then(|millis| return millis.try_into().ok()),
        cover_art_url: Some(thumbnail_url).into(),
    });
}

fn video_url_from_id(id: &str) -> String {
    return format!("https://www.youtube.com/watch?v={id}");
}

mod video_parsing {
    use crate::parser::{HtmlParsingSchema, HtmlSelector, ParsingEntry, TargetContent};
    pub const TITLE: HtmlParsingSchema = HtmlParsingSchema::new(ParsingEntry::Selector {
        selector: HtmlSelector::Attribute {
            attr: "itemprop",
            value: "name",
        },
        next_entry: &ParsingEntry::Content {
            target: TargetContent::Attribute { attr: "content" },
        },
    });

    pub const CHANNEL_TITLE: HtmlParsingSchema = HtmlParsingSchema::new(ParsingEntry::Selector {
        selector: HtmlSelector::Attribute {
            attr: "itemprop",
            value: "author",
        },
        next_entry: &ParsingEntry::Selector {
            selector: HtmlSelector::Attribute {
                attr: "itemprop",
                value: "name",
            },
            next_entry: &ParsingEntry::Content {
                target: TargetContent::Attribute { attr: "content" },
            },
        },
    });

    pub const THUMBNAIL_URL: HtmlParsingSchema = HtmlParsingSchema::new(ParsingEntry::Selector {
        selector: HtmlSelector::Attribute {
            attr: "itemprop",
            value: "thumbnailUrl",
        },
        next_entry: &ParsingEntry::Content {
            target: TargetContent::Attribute { attr: "href" },
        },
    });

    pub const DURATION: HtmlParsingSchema = HtmlParsingSchema::new(ParsingEntry::Selector {
        selector: HtmlSelector::Attribute {
            attr: "itemprop",
            value: "duration",
        },
        next_entry: &ParsingEntry::Content {
            target: TargetContent::Attribute { attr: "content" },
        },
    });
}

#[cfg(test)]
mod tests {
    use pretty_assertions::assert_eq;

    use super::scrape_youtube_video;

    #[test]
    fn test_scrape_youtube_video() {
        let raw_html = include_str!("test_data/youtube_video_scraping_data.html");

        let video = scrape_youtube_video(raw_html, "fake").unwrap();
        assert_eq!(video.name.inner_as_ref(), Some("Foo Fighters - My Hero"));
        assert_eq!(video.author.inner_as_ref(), Some("0foofighter0"));
    }
}
