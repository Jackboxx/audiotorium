use std::sync::Arc;

use audiotorium_core::{
    error::{AppError, AppErrorKind, IntoAppError},
    identifier::{Identifier, YoutubePlaylistUrl, YoutubeVideoUrl},
    schema::audio::AudioPlaylistMetadata,
};
use chromiumoxide::{cdp::browser_protocol::network::CookieParamBuilder, Browser};
use scraper::{Html, Selector};

/// 'id' has to be a valid youtube playlist id
pub async fn scrape_youtube_playlist_metadata(
    browser: &Browser,
    id: &str,
) -> Result<AudioPlaylistMetadata, AppError> {
    skip_youtube_consent_screen(browser).await?;

    let url = playlist_url_from_id(id);

    let page = browser.new_page(&url).await.into_app_err(
        "failed to fetch data from youtube",
        AppErrorKind::WebScraper,
        &[&format!("PLAYLIST ID: {id}")],
    )?;

    page.wait_for_navigation().await.into_app_err(
        "failed to fetch data from youtube",
        AppErrorKind::WebScraper,
        &[&format!("PLAYLIST ID: {id}")],
    )?;

    let raw_html = page.content().await.into_app_err(
        "failed to fetch data from youtube",
        AppErrorKind::WebScraper,
        &[&format!("PLAYLIST ID: {id}")],
    )?;

    page.close().await.into_app_err(
        "failed to fetch data from youtube",
        AppErrorKind::WebScraper,
        &[&format!("PLAYLIST ID: {id}")],
    )?;

    return parse_playlist_metadata(&url, &raw_html);
}

pub async fn scrape_youtube_playlist_urls(
    browser: &Browser,
    id: &str,
) -> Result<Arc<[YoutubeVideoUrl<Arc<str>>]>, AppError> {
    skip_youtube_consent_screen(browser).await?;

    let url = playlist_url_from_id(id);

    let page = browser.new_page(url).await.into_app_err(
        "failed to fetch data from youtube",
        AppErrorKind::WebScraper,
        &[&format!("PLAYLIST ID: {id}")],
    )?;

    page.wait_for_navigation().await.into_app_err(
        "failed to fetch data from youtube",
        AppErrorKind::WebScraper,
        &[&format!("PLAYLIST ID: {id}")],
    )?;

    let raw_html = page.content().await.into_app_err(
        "failed to fetch data from youtube",
        AppErrorKind::WebScraper,
        &[&format!("PLAYLIST ID: {id}")],
    )?;

    page.close().await.into_app_err(
        "failed to fetch data from youtube",
        AppErrorKind::WebScraper,
        &[&format!("PLAYLIST ID: {id}")],
    )?;

    return Ok(parse_playlist_urls(&raw_html));
}

fn parse_playlist_metadata(url: &str, raw_html: &str) -> Result<AudioPlaylistMetadata, AppError> {
    let url_validated = YoutubePlaylistUrl::try_new(Arc::from(url)).into_app_err(
        "failed to fetch data from youtube",
        AppErrorKind::WebScraper,
        &[&format!("PLAYLIST URL: {url}")],
    )?;

    let html = Html::parse_document(raw_html);

    let cover_art_url = playlist_parsing::COVER_ART_URL.parse(&html)?;
    let title = playlist_parsing::TITLE.parse(&html)?;
    let author = playlist_parsing::AUTHOR.parse(&html)?;

    return Ok(AudioPlaylistMetadata {
        identifier: url_validated.uid(),
        name: Some(title).into(),
        author: Some(author).into(),
        cover_art_url: Some(cover_art_url).into(),
    });
}

async fn skip_youtube_consent_screen(browser: &Browser) -> Result<(), AppError> {
    let page = browser.new_page("https://youtube.com").await.into_app_err(
        "failed to fetch data from youtube",
        AppErrorKind::WebScraper,
        &[],
    )?;

    let magic_value = "CAESEwgDEgk0ODE3Nzk3MjQaAmVuIAEaBgiA_LyaBg";
    let skip_consent_cookie = CookieParamBuilder::default()
        .name("SOCS")
        .value(magic_value)
        .domain(".youtube.com")
        .path("/")
        .secure(true)
        .build()
        .into_app_err(
            "failed to fetch data from youtube",
            AppErrorKind::WebScraper,
            &[],
        )?;

    page.set_cookie(skip_consent_cookie).await.into_app_err(
        "failed to fetch data from youtube",
        AppErrorKind::WebScraper,
        &[],
    )?;

    page.close().await.into_app_err(
        "failed to fetch data from youtube",
        AppErrorKind::WebScraper,
        &[],
    )?;

    return Ok(());
}

fn parse_playlist_urls(raw_html: &str) -> Arc<[YoutubeVideoUrl<Arc<str>>]> {
    let html = Html::parse_document(raw_html);
    let selector = Selector::parse("#video-title").expect("should be a valid selector");

    return html
        .select(&selector)
        .filter_map(|url| return url.attr("href"))
        .flat_map(|href| return YoutubeVideoUrl::try_new(format!("https://youtube.com{href}")))
        .collect();
}

fn playlist_url_from_id(id: &str) -> String {
    return format!("https://www.youtube.com/playlist?list={id}");
}

mod playlist_parsing {
    use crate::parser::{HtmlParsingSchema, HtmlSelector, ParsingEntry, TargetContent};

    pub const TITLE: HtmlParsingSchema = HtmlParsingSchema::new(ParsingEntry::Selector {
        selector: HtmlSelector::Id { id: "text" },
        next_entry: &ParsingEntry::Content {
            target: TargetContent::Text,
        },
    });

    pub const AUTHOR: HtmlParsingSchema = HtmlParsingSchema::new(ParsingEntry::Selector {
        selector: HtmlSelector::Id { id: "owner-text" },
        next_entry: &ParsingEntry::Content {
            target: TargetContent::Text,
        },
    });

    pub const COVER_ART_URL: HtmlParsingSchema = HtmlParsingSchema::new(ParsingEntry::Selector {
        selector: HtmlSelector::Id { id: "img" },
        next_entry: &ParsingEntry::Content {
            target: TargetContent::Attribute { attr: "src" },
        },
    });
}
