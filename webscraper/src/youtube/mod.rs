mod playlist;
mod search;
mod video;

use actix_web::{web, HttpResponse};
use serde::Deserialize;

use crate::headless_browser;

#[actix_web::get("youtube/video/{id}")]
pub async fn scrape_youtube_video_metadata(id: web::Path<String>) -> HttpResponse {
    match video::scrape_youtube_video_metadata(&id).await {
        Ok(metadata) => HttpResponse::Ok().json(metadata),
        Err(err) => HttpResponse::InternalServerError().json(err),
    }
}

#[actix_web::get("youtube/playlist/{id}")]
pub async fn scrape_youtube_playlist_metadata(id: web::Path<String>) -> HttpResponse {
    match playlist::scrape_youtube_playlist_metadata(headless_browser(), &id).await {
        Ok(metadata) => HttpResponse::Ok().json(metadata),
        Err(err) => HttpResponse::InternalServerError().json(err),
    }
}

#[actix_web::get("youtube/playlist/{id}/urls")]
pub async fn scrape_youtube_playlist_urls(id: web::Path<String>) -> HttpResponse {
    match playlist::scrape_youtube_playlist_urls(headless_browser(), &id).await {
        Ok(urls) => HttpResponse::Ok().json(urls),
        Err(err) => HttpResponse::InternalServerError().json(err),
    }
}

#[derive(Debug, Deserialize)]
struct SearchQueryParams {
    search: String,
    limit: usize,
}

#[actix_web::get("youtube/search")]
pub async fn scrape_youtube_search_results(
    web::Query(SearchQueryParams { search, limit }): web::Query<SearchQueryParams>,
) -> HttpResponse {
    match search::scrape_youtube_search_results(headless_browser(), &search, limit, false).await {
        Ok(results) => HttpResponse::Ok().json(results),
        Err(err) => HttpResponse::InternalServerError().json(err),
    }
}
