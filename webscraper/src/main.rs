#![deny(clippy::implicit_return)]
#![allow(clippy::needless_return)]

mod parser;
mod youtube;

use std::sync::OnceLock;

use actix_web::{App, HttpServer};
use audiotorium_core::error::{AppError, AppErrorKind, IntoAppError};
use chromiumoxide::{Browser, BrowserConfig};
use futures::StreamExt;
use tokio::task::JoinHandle;
use youtube::{
    scrape_youtube_playlist_metadata, scrape_youtube_playlist_urls, scrape_youtube_search_results,
    scrape_youtube_video_metadata,
};

use env::{init_envs, envs};

mod env;

pub static HEADLESS_BROWSER: OnceLock<Browser> = OnceLock::new();

pub fn headless_browser<'a>() -> &'a Browser {
    return HEADLESS_BROWSER
        .get()
        .expect("headless browser should be set at server start");
}

#[actix_web::main]
async fn main() -> Result<(), std::io::Error> {
    init_envs();

    let (browser, _handle) = create_headless_browser().await.unwrap();
    HEADLESS_BROWSER.set(browser).unwrap();

    HttpServer::new(|| {
        return App::new()
            .service(scrape_youtube_video_metadata)
            .service(scrape_youtube_playlist_metadata)
            .service(scrape_youtube_playlist_urls)
            .service(scrape_youtube_search_results);
    })
    .bind((envs().webscraper_host, envs().webscraper_http_port))?
    .run()
    .await
}

async fn create_headless_browser() -> Result<(Browser, JoinHandle<()>), AppError> {
    let config = BrowserConfig::builder().no_sandbox().build().into_app_err(
        "failed to create headless browser for web scraping",
        AppErrorKind::WebScraper,
        &[],
    )?;

    let (browser, mut handler) = Browser::launch(config).await.into_app_err(
        "failed to create headless browser for web scraping",
        AppErrorKind::WebScraper,
        &[],
    )?;

    let handle = tokio::task::spawn(async move {
        loop {
            if let Some(_event) = handler.next().await {};
        }
    });

    return Ok((browser, handle));
}
