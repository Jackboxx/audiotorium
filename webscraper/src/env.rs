use std::{net::Ipv4Addr, str::FromStr, sync::OnceLock};

static ENVS: OnceLock<EnvVars> = OnceLock::new();

pub fn envs<'a>() -> &'a EnvVars {
    return ENVS.get_or_init(|| {
        return EnvVars::init();
    });
}

pub fn init_envs() {
    if ENVS.get().is_some() {
        return;
    }

    ENVS.set(EnvVars::init())
        .expect("Failed to set global env vars");
}

#[derive(Debug)]
pub struct EnvVars {
    pub webscraper_host: Ipv4Addr,
    pub webscraper_http_port: u16,
}

impl EnvVars {
    const KEY_WEBSCRAPER_HOST: &'static str = "A_WEBSCRAPER_HOST";
    const KEY_WEBSCRAPER_HTTP_PORT: &'static str = "A_WEBSCRAPER_HTTP_PORT";

    fn init() -> Self {
        // replace `bool` with `!` when/if it is stabilized (<https://github.com/rust-lang/rust/issues/35121>)
        return Self {
            webscraper_host: load_env_or_panic(Self::KEY_WEBSCRAPER_HOST, |value| {
                return Ipv4Addr::from_str(&value);
            }),
            webscraper_http_port: load_env_or_panic(Self::KEY_WEBSCRAPER_HTTP_PORT, |value| {
                return value.parse::<u16>();
            }),
        };

        fn load_env_or_panic<T, E: std::fmt::Display>(
            key: &str,
            parse_fn: fn(value: String) -> Result<T, E>,
        ) -> T {
            return std::env::var(key)
                .map(|value| {
                    return parse_fn(value).unwrap_or_else(|err| {
                        return panic_with_info(key, err);
                    });
                })
                .unwrap_or_else(|err| {
                    return panic_with_info(key, err);
                });
        }

        fn panic_with_info<ErrMsg: std::fmt::Display, T>(key: &str, err: ErrMsg) -> T {
            panic!("Invalid value for key '{key}'\n{err}")
        }
    }
}
