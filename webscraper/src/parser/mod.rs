#![allow(unused)]

mod parsing_errors;
mod schema;

pub use parsing_errors::ParsingError;
pub use schema::{HtmlParsingSchema, HtmlSelector, ParsingEntry, TargetContent};
