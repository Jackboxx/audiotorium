use audiotorium_core::error::{AppError, AppErrorKind, IntoAppError};
use thiserror::Error;

#[derive(Debug, Error)]
pub enum ParsingError {
    #[error("No root element in selected html")]
    NoRootElement,
    #[error("Target element is missing text content")]
    MissingTextContent,
    #[error("Target element is missing attribute with name '{0}'")]
    MissingAttribute(String),
    #[error("No element matching selector '{0}'found ")]
    NoSelectorMatch(String),
    #[error("Invalid selector string '{0}'")]
    InvalidSelector(String),
}

impl From<ParsingError> for AppError {
    fn from(value: ParsingError) -> Self {
        return value.into_app_err(
            "failed to scrape playlist metadata",
            AppErrorKind::WebScraper,
            &[],
        );
    }
}
