use super::parsing_errors::ParsingError;
use scraper::{Element, ElementRef, Html, Selector};

pub struct HtmlParsingSchema<'a> {
    entry: ParsingEntry<'a>,
}

pub enum ParsingEntry<'a> {
    Content {
        target: TargetContent<'a>,
    },
    Selector {
        selector: HtmlSelector<'a>,
        next_entry: &'a ParsingEntry<'a>,
    },
}

pub enum HtmlSelector<'a> {
    Class { class: &'a str },
    Id { id: &'a str },
    Attribute { attr: &'a str, value: &'a str },
    Raw { selector: &'a str },
}
pub enum TargetContent<'a> {
    Text,
    Attribute { attr: &'a str },
}

impl<'a> HtmlParsingSchema<'a> {
    pub const fn new(entry: ParsingEntry<'a>) -> Self {
        return Self { entry };
    }

    pub fn parse(&self, html: &Html) -> Result<String, ParsingError> {
        return self.entry.parse(html);
    }
}

impl ParsingEntry<'_> {
    fn parse(&self, html: &Html) -> Result<String, ParsingError> {
        match self {
            Self::Content { target } => return target.parse(html),
            Self::Selector {
                selector: ident,
                next_entry: entry,
            } => {
                let html = ident.select(html)?;
                return entry.parse(&html);
            }
        }
    }
}

impl TargetContent<'_> {
    fn parse(&self, html: &Html) -> Result<String, ParsingError> {
        match self {
            Self::Text => {
                let element = get_root_as_element_from_html(html)?;
                let text_nodes = element.text();
                let text_content = text_nodes.map(|t| return t.to_string()).collect::<Vec<_>>();

                if text_content.is_empty() {
                    return Err(ParsingError::MissingTextContent);
                }

                return Ok(text_content.join(""));
            }
            Self::Attribute { attr } => {
                let element = get_root_as_element_from_html(html)?;
                return element
                    .attr(attr)
                    .map(|content| return content.to_owned())
                    .ok_or_else(|| return ParsingError::MissingAttribute(attr.to_string()));
            }
        };
    }
}

fn get_root_as_element_from_html(html: &Html) -> Result<ElementRef<'_>, ParsingError> {
    return html
        .root_element()
        .first_element_child()
        .ok_or_else(|| return ParsingError::NoRootElement);
}

impl HtmlSelector<'_> {
    fn select(&self, html: &Html) -> Result<Html, ParsingError> {
        let selector_str = match self {
            Self::Class { class } => format!(".{class}"),
            Self::Id { id } => format!("#{id}"),
            Self::Attribute { attr, value } => format!(r#"[{attr}="{value}"]"#),
            Self::Raw { selector } => selector.to_string(),
        };

        let Ok(selector) = Selector::parse(&selector_str) else {
            return Err(ParsingError::InvalidSelector(selector_str));
        };

        return html
            .select(&selector)
            .next()
            .map(|e| return Html::parse_fragment(&e.html()))
            .ok_or_else(|| return ParsingError::NoSelectorMatch(selector_str));
    }
}
