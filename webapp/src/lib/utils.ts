import type { CentralGatewayCommand } from '$api/CentralGatewayCommand';
import type { AudioKind } from '$api/AudioKind';
import type { AudioNodeCommand } from '$api/AudioNodeCommand';
import type { UserErrorMessage } from '$api/UserErrorMessage';
import { browser } from '$app/environment';
import { page } from '$app/stores';

import { toast, type ToasterProps } from 'svelte-sonner';

import { writable } from 'svelte/store';

const { set: setPosition, subscribe } = writable<ToasterProps['position']>('top-right');

export const toastPosition = { subscribe };

const positionsPerPage = new Map<string, ToasterProps['position']>([
	['/', 'top-right'],
	['/queue', 'top-right'],
	['/search', 'bottom-right']
]);

if (browser) {
	page.subscribe((page) => {
		const key = page?.url?.pathname;
		let pos = positionsPerPage.get(key) ?? 'top-right';
		setPosition(pos);
	});
}

export type AudioSearchType =
	| 'youtube-audio'
	| 'youtube-audio-playlist'
	| 'local-audio'
	| 'local-audio-playlist'
	| 'basic-user-search';

export function detectAudioSearchType(search: string): AudioSearchType {
	if (search.startsWith('https://www.youtube.com/watch?v=')) {
		return 'youtube-audio';
	}

	if (search.startsWith('https://www.youtube.com/playlist?list=')) {
		return 'youtube-audio-playlist';
	}

	let playlist_audio_prefix: AudioKind = 'youtube_audio_playlist';
	if (search.startsWith(playlist_audio_prefix)) {
		return 'local-audio-playlist';
	}

	let audio_prefix: AudioKind = 'youtube_audio';
	if (search.startsWith(audio_prefix)) {
		return 'local-audio';
	}

	return 'basic-user-search';
}

export function unixMillisToMinutesString(millis: number): string {
	const total_seconds = Math.trunc(millis / 1000);
	const seconds = total_seconds % 60;
	const minutes = Math.trunc(total_seconds / 60);

	const str_seconds = seconds < 10 ? `0${seconds}` : `${seconds}`;

	return `${minutes}:${str_seconds}`;
}

export function callKeypressOnEnter(e: KeyboardEvent, calback: () => void) {
	if (e.key === 'Enter') {
		calback();
	}
}

export async function sendNodeCommandWithTimeout(
	cmd: AudioNodeCommand,
	nodeName: string,
	timeoutMs: number,
	toastMsgs?: {
		loading: string;
		error: string;
		success: string;
	}
): Promise<{ timeout: boolean }> {
	const timeoutIdentifier = {};

	try {
		const promise = new Promise(async (res, rej) => {
			setTimeout(() => rej(timeoutIdentifier), timeoutMs);
			res(await sendNodeCommand(cmd, nodeName));
		});

		if (toastMsgs) {
			const minTimeBeforeToast = 500;
			await promiseToastWithDelay(promise, minTimeBeforeToast, toastMsgs);
		} else {
			await promise;
		}
	} catch (e) {
		if (e === timeoutIdentifier) {
			return { timeout: true };
		}
	}
	return { timeout: false };
}

async function promiseToastWithDelay<T>(
	promise: Promise<T>,
	delay: number,
	msgs: {
		loading: string;
		error: string;
		success: string;
	}
) {
	const delayIdentifier = {};
	const result = await Promise.race([
		promise,
		new Promise((res) => setTimeout(() => res(delayIdentifier), delay))
	]);

	if (result === delayIdentifier) {
		toast.promise(promise, msgs);
	}
}

export async function sendCentralGatewayCommand(cmd: CentralGatewayCommand) {
	try {
		const resp = await fetch(`${import.meta.env.VITE_API_PREFIX}/commands/gateway`, {
			method: 'POST',
			body: JSON.stringify(cmd),
			headers: { 'Content-Type': 'application/json' }
		});

		if (resp.status !== 200) {
			throw await resp.json();
		}
	} catch (err) {
		toastError(err as UserErrorMessage);
	}
}

export async function sendNodeCommand(cmd: AudioNodeCommand, nodeName: string) {
	try {
		const resp = await fetch(
			`${import.meta.env.VITE_API_PREFIX}/commands/node/${nodeName}`,
			{
				method: 'POST',
				body: JSON.stringify(cmd),
				headers: { 'Content-Type': 'application/json' }
			}
		);

		if (resp.status !== 200) {
			throw await resp.json();
		}
	} catch (err) {
		toastError(err as UserErrorMessage);
	}
}

export function toastError(err: UserErrorMessage) {
	const logMsg = `ERROR\n\n${err?.USER_ERROR?.kind}: ${err?.USER_ERROR?.info}`;
	console.error(logMsg);

	if ('USER_ERROR' in err) {
		toast.error(err.USER_ERROR?.info);
	}
}
