import type { SearchAudioResult } from '$api/SearchAudioResult';
import type { SearchAudioResultKind } from '$api/SearchAudioResultKind';

export type YoutubeContentId = { playlistId: string } | { videoId: string };

/**
 * this will return undefined if the youtube rate limit is exceeded
 */
export async function getYoutubeMetadataByContentId(
	contentId: YoutubeContentId,
	apiKey: string
): Promise<SearchAudioResult | undefined> {
	if ('playlistId' in contentId) {
		const apiUrl = `https://youtube.googleapis.com/youtube/v3/playlists?part=snippet&maxResults=1&id=${contentId.playlistId}&key=${apiKey}`;
		return await getFirstVideoOrPlaylist(apiUrl, 'playlist');
	} else {
		const apiUrl = `https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails&maxResults=1&id=${contentId.videoId}&key=${apiKey}`;
		return await getFirstVideoOrPlaylist(apiUrl, 'track');
	}
}

export function extractYoutubeContentId(url: string): YoutubeContentId | undefined {
	const playlistUrlParts = url.split('https://www.youtube.com/playlist?list=');
	if (playlistUrlParts.length > 1) {
		const playlistId = playlistUrlParts[1].split('&')[0];
		if (playlistId) {
			return { playlistId };
		}
	}

	const videoUrlParts = url.split('https://www.youtube.com/watch?v=');
	if (videoUrlParts.length > 1) {
		const videoId = videoUrlParts[1].split('&')[0];
		if (videoId) {
			return { videoId };
		}
	}

	return undefined;
}

function youtubeUrlFromId(id: { playlistId: string } | { videoId: string }): string {
	if ('playlistId' in id) {
		return `https://www.youtube.com/playlist?list=${id.playlistId}`;
	} else {
		return `https://www.youtube.com/watch?v=${id.videoId}`;
	}
}

function extractYoutubeSearchResultKind(kind: string): SearchAudioResultKind {
	if (kind.includes('playlist')) {
		return 'playlist';
	} else {
		return 'track';
	}
}

async function getFirstVideoOrPlaylist(
	apiUrl: string,
	kind: SearchAudioResultKind
): Promise<SearchAudioResult | undefined> {
	let data;
	try {
		const resp = await fetch(apiUrl, {
			headers: { 'Content-Type': 'application/json' }
		});

		data = await resp.json();
	} catch (err) {
		console.error(err);
		return undefined;
	}

	const firstVideo = data.items[0];

	if (!firstVideo || !firstVideo.id) {
		return undefined;
	}

	const thumbnails = firstVideo.snippet?.thumbnails;
	const url = youtubeUrlFromId(firstVideo.id);

	return {
		kind,
		score: 0,
		identifier: { youtube: { url } },
		name: firstVideo.snippet?.title,
		author: firstVideo.snippet?.channelTitle,
		coverArtUrl:
			thumbnails?.maxres?.url ??
			thumbnails?.high?.url ??
			thumbnails?.medium?.url ??
			thumbnails?.default?.url
	};
}
