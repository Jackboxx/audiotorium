import { expect, test } from 'vitest';
import { extractYoutubeContentId } from './youtube';

test.each([
	{
		url: 'https://www.youtube.com/watch?v=fdsakfl7834',
		expected: { videoId: 'fdsakfl7834' }
	},
	{
		url: 'https://www.youtube.com/playlist?list=12346532',
		expected: { playlistId: '12346532' }
	},
	{
		url: 'https://www.not-youtube.com/watch?v=fdsakfl7834',
		expected: undefined
	},
	{ url: 'http://invalid.com', expected: undefined },
	{ url: 'https://youtube.com/invalid', expected: undefined },
	{ url: 'invalid text', expected: undefined }
])('extract id from url: $name -> expected $expected', ({ url, expected }) => {
	expect(extractYoutubeContentId(url)).toEqual(expected);
});
