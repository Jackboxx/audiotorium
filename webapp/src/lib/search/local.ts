import type { AudioTrackMetadata } from '$api/AudioTrackMetadata';
import type { SearchAudioResultKind } from '$api/SearchAudioResultKind';

export async function getLocalAudioByUid(
	uid: string,
	kind: SearchAudioResultKind
): Promise<AudioTrackMetadata | undefined> {
	const endpoint = kind === 'playlist' ? 'audio_playlists' : 'audio';
	const resp = await fetch(
		`${import.meta.env.VITE_API_PREFIX}/data/${endpoint}/${uid}`,
		{
			headers: new Headers({ 'content-type': 'application/json' })
		}
	);

	if (resp.status != 200) {
		return undefined;
	}

	try {
		const data: AudioTrackMetadata = await resp.json();
		return data;
	} catch (err) {
		console.error(err);
		return undefined;
	}
}
