import type { SearchAudioResult } from '$api/SearchAudioResult';
import type { AudioOrigin } from '$api/AudioOrigin';

export async function searchAudio(
	search: string,
	origins: AudioOrigin[]
): Promise<SearchAudioResult[] | undefined> {
	const originsString = origins.join(',');
	const resp = await fetch(
		`${
			import.meta.env.VITE_API_PREFIX
		}/data/search/audio?search=${search}&origins=${originsString}&minScore=0.03`,
		{
			headers: new Headers({ 'content-type': 'application/json' })
		}
	);

	if (resp.status != 200) {
		return undefined;
	}

	try {
		const data: SearchAudioResult[] = await resp.json();

		return data;
	} catch (err) {
		console.error(err);
		return undefined;
	}
}
