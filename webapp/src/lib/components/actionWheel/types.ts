import type { ComponentType } from 'svelte';

export type ActionWheelItem = {
	id: string;
	action: () => void;
	icon: string | { component: ComponentType; props?: Record<string, any> };
	hintText?: string;
};

export type WheelContext = {
	items: ActionWheelItem[];
	disable: ActionWheelItem['id'][];
};

export type PointerPosition = { x: number; y: number };
