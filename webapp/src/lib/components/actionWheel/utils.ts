import { writable } from 'svelte/store';
import type { ActionWheelItem, PointerPosition, WheelContext } from './types';

export const wheelContext = writable(new Map<ActionWheelItem['id'], WheelContext>());

export const dataAttributes = {
	blockActionWheel: 'blockActionWheel',
	actionWheelContextId: 'actionWheelContextId'
} as const;

export function distanceBetweenPointers(
	p1: PointerPosition,
	p2: PointerPosition
): number {
	const x = p2.x - p1.x;
	const y = p2.y - p1.y;

	return Math.sqrt(x * x + y * y);
}

export function degreesBetweenPointers(p1: PointerPosition, p2: PointerPosition): number {
	const x = p2.x - p1.x;
	const y = p2.y - p1.y;

	const degs = (Math.atan2(-y, x) * 180) / Math.PI;
	return degs >= 0 ? degs : degs + 360;
}
