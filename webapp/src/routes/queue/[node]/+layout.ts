export const load = ({ params }: { params: { node: string } }) => {
	return { node: params.node };
};
