FROM rust:1.76

WORKDIR /app/gateway

RUN mkdir /app/core

RUN apt update && apt install -y curl \
                    software-properties-common apt-transport-https ca-certificates \
                    libasound2-dev ffmpeg alsa-utils bluez-alsa-utils

RUN curl -L https://github.com/yt-dlp/yt-dlp/releases/download/2023.12.30/yt-dlp -o /usr/bin/yt-dlp
RUN chmod a+rx /usr/bin/yt-dlp

ARG SQLX_OFFLINE
ENV SQLX_OFFLINE $SQLX_OFFLINE

# core lib
COPY ./core/Cargo.toml ./core/Cargo.lock /app/core/
COPY ./core/src /app/core/src

# source code
COPY ./gateway/Cargo.toml ./gateway/Cargo.lock /app/gateway/
COPY ./gateway/src /app/gateway/src

# database config
COPY ./gateway/migrations /app/gateway/migrations
COPY ./gateway/.sqlx /app/gateway/.sqlx

RUN --mount=type=cache,target=/usr/local/cargo/registry \
    --mount=type=cache,target=/app/gateway/target \
    if [ "$SQLX_OFFLINE" = "true" ]; then \
        SQLX_OFFLINE=true \
        cargo install --debug --path . ; \
    else \
        SQLX_OFFLINE=true \
        cargo install --path . ; \
    fi

# server config
COPY ./gateway/config/audio-devices.toml /app/gateway/config/
COPY ./gateway/config/asound.conf /etc/asound.conf

ENTRYPOINT ["audiotorium-gateway"]
