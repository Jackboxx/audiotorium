use std::{net::Ipv4Addr, str::FromStr, sync::OnceLock};

static ENVS: OnceLock<EnvVars> = OnceLock::new();

pub fn envs<'a>() -> &'a EnvVars {
    return ENVS.get_or_init(|| {
        return EnvVars::init();
    });
}

pub fn init_envs() {
    if ENVS.get().is_some() {
        return;
    }

    ENVS.set(EnvVars::init())
        .expect("Failed to set global env vars");
}

#[derive(Debug)]
pub struct EnvVars {
    pub downloader_host: Ipv4Addr,
    pub downloader_http_port: u16,
    pub downloader_msg_port: u16,
    pub webscraper_host: Ipv4Addr,
    pub webscraper_http_port: u16,
    pub db_host: Ipv4Addr,
    pub db_user: String,
    pub db_password: String,
    pub youtube_api_key: String,
}

impl EnvVars {
    const KEY_WEBSCRAPER_HOST: &'static str = "A_WEBSCRAPER_HOST";
    const KEY_WEBSCRAPER_HTTP_PORT: &'static str = "A_WEBSCRAPER_HTTP_PORT";
    const KEY_DOWNLOADER_HOST: &'static str = "A_DL_HOST";
    const KEY_DOWNLOADER_HTTP_PORT: &'static str = "A_DL_HTTP_PORT";
    const KEY_DOWNLOADER_TCP_MSG_PORT: &'static str = "A_DL_TCP_MSG_PORT";
    const KEY_DB_HOST: &'static str = "A_DB_HOST";
    const KEY_DB_USER: &'static str = "A_DB_USER";
    const KEY_DB_PASSWORD: &'static str = "A_DB_PASSWORD";
    const KEY_YOUTUBE_API_KEY: &'static str = "A_YOUTUBE_API_KEY";

    fn init() -> Self {
        // replace `bool` with `!` when/if it is stabilized (<https://github.com/rust-lang/rust/issues/35121>)
        return Self {
            youtube_api_key: load_env_or_panic::<_, bool>(Self::KEY_YOUTUBE_API_KEY, |value| {
                return Ok(value);
            }),
            webscraper_host: load_env_or_panic(Self::KEY_WEBSCRAPER_HOST, |value| {
                return Ipv4Addr::from_str(&value);
            }),
            webscraper_http_port: load_env_or_panic(Self::KEY_WEBSCRAPER_HTTP_PORT, |value| {
                return value.parse::<u16>();
            }),
            downloader_host: load_env_or_panic(Self::KEY_DOWNLOADER_HOST, |value| {
                return Ipv4Addr::from_str(&value);
            }),
            downloader_http_port: load_env_or_panic(Self::KEY_DOWNLOADER_HTTP_PORT, |value| {
                return value.parse::<u16>();
            }),
            downloader_msg_port: load_env_or_panic(Self::KEY_DOWNLOADER_TCP_MSG_PORT, |value| {
                return value.parse::<u16>();
            }),
            db_host: load_env_or_panic(Self::KEY_DB_HOST, |value| {
                return Ipv4Addr::from_str(&value);
            }),
            db_user: load_env_or_panic::<_, bool>(Self::KEY_DB_USER, |value| {
                return Ok(value);
            }),
            db_password: load_env_or_panic::<_, bool>(Self::KEY_DB_PASSWORD, |value| {
                return Ok(value);
            }),
        };

        fn load_env_or_panic<T, E: std::fmt::Display>(
            key: &str,
            parse_fn: fn(value: String) -> Result<T, E>,
        ) -> T {
            return std::env::var(key)
                .map(|value| {
                    return parse_fn(value).unwrap_or_else(|err| {
                        return panic_with_info(key, err);
                    });
                })
                .unwrap_or_else(|err| {
                    return panic_with_info(key, err);
                });
        }

        fn panic_with_info<ErrMsg: std::fmt::Display, T>(key: &str, err: ErrMsg) -> T {
            panic!("Invalid value for key '{key}'\n{err}")
        }
    }
}
