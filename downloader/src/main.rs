#![deny(clippy::implicit_return)]
#![allow(clippy::needless_return)]
#![allow(clippy::items_after_statements)]

use actix_cors::Cors;
use actix_web::{get, web, App, HttpServer};
use env::envs;
use std::{
    net::{IpAddr, SocketAddr},
    sync::Arc,
};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

use actix::Actor;
use path::{event_data_dir, setup_paths};
use request_handler::{AudioDownload, AudioDownloader};
use sqlx::postgres::PgPoolOptions;

use audiotorium_core::{
    events::{EventQueue, PersistStrategy},
    identifier::{Identifier, ItemUid},
    path::data_shared_audio,
    tags::EventQueueIds,
    tcp_messaging::{Mutex, TcpMessageGateway},
};

mod env;
mod path;
mod request_handler;
mod youtube_api;
mod youtube_downloads;

#[actix_web::main]
async fn main() -> Result<(), std::io::Error> {
    tracing_subscriber::registry()
        .with(
            tracing_subscriber::fmt::layer()
                .with_file(true)
                .with_line_number(true),
        )
        .with(tracing_subscriber::EnvFilter::from_default_env())
        .init();

    env::init_envs();

    globals::FORCE_USING_YOUTUBE_WEB_SCRAPING
        .set(false)
        .unwrap();

    setup_paths().expect("failed to setup required directories");
    setup_db_con().await;

    actix_rt::spawn(async {
        let mut gateway = setup_tcp_gateway().await;
        gateway.listen().await.unwrap();
    });

    return HttpServer::new(move || {
        let cors = Cors::default()
            .allow_any_origin()
            .allow_any_method()
            .allow_any_header();

        return App::new().wrap(cors).service(download_media);
    })
    .bind((envs().downloader_host, envs().downloader_http_port))?
    .run()
    .await;
}

async fn setup_tcp_gateway() -> TcpMessageGateway {
    let mut gateway = TcpMessageGateway::new(SocketAddr::new(
        IpAddr::V4(envs().downloader_host),
        envs().downloader_msg_port,
    ));

    let download_event_queue = EventQueue::<AudioDownload>::new(
        EventQueueIds::DOWNLOADER_DOWNLOAD_QUEUE,
        event_data_dir(),
        PersistStrategy::EveryMessage,
    )
    .unwrap();

    let addr = download_event_queue.start();
    let (downloader, tcp) = AudioDownloader::new(addr.recipient());
    let _ = downloader.start();

    let download_handler = Arc::new(Mutex::new(tcp));
    gateway.bind(&download_handler).await;

    return gateway;
}

async fn setup_db_con() {
    let db_user = &envs().db_user;
    let db_password = &envs().db_password;
    let db_host = &envs().db_host;

    let db_url = format!("postgres://{db_user}:{db_password}@{db_host}");
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&db_url)
        .await
        .expect("should be able to connect to database");

    globals::POOL.set(pool).expect("should never fail");
}

#[get("/media/{uid}")]
pub async fn download_media(
    uid: web::Path<Arc<str>>,
) -> Result<actix_files::NamedFile, actix_web::Error> {
    tracing::debug!(info = "Received download request.", %uid);

    let uid = ItemUid::new(uid.into_inner());
    let path = uid.to_path_with_ext(&data_shared_audio());
    let file = actix_files::NamedFile::open_async(path).await?;

    return Ok(file);
}

pub mod globals {
    use sqlx::PgPool;
    use std::sync::OnceLock;

    pub static POOL: OnceLock<PgPool> = OnceLock::new();
    pub static FORCE_USING_YOUTUBE_WEB_SCRAPING: OnceLock<bool> = OnceLock::new();

    #[cfg(debug_assertions)]
    pub fn force_using_youtube_web_scraping() -> bool {
        return *FORCE_USING_YOUTUBE_WEB_SCRAPING
            .get()
            .expect("force use web scraping should be set at startup in debug mode");
    }

    #[cfg(not(debug_assertions))]
    pub fn force_using_youtube_web_scraping() -> bool {
        return false;
    }

    pub fn db_pool<'a>() -> &'a PgPool {
        return POOL.get().expect("pool should be set at startup");
    }
}
