use std::sync::Arc;

use crate::env::envs;
use crate::globals::force_using_youtube_web_scraping;
use audiotorium_core::{
    error::{AppError, AppErrorKind, IntoAppError},
    external_apis::youtube::{YoutubeApiDataFetcher, YoutubeApiResponse},
    identifier::{YoutubePlaylistUrl, YoutubeVideoUrl},
    schema::audio::{AudioPlaylistMetadata, AudioTrackMetadata},
};

pub async fn fetch_audio_metadata_with_fallback(
    url: &YoutubeVideoUrl<Arc<str>>,
) -> Result<AudioTrackMetadata, AppError> {
    if force_using_youtube_web_scraping() {
        return webscrape_audio_metadata(url).await;
    };

    match YoutubeApiDataFetcher::fetch_audio_metadata(&envs().youtube_api_key, url).await? {
        YoutubeApiResponse::Ok(metadata) => return Ok(metadata),
        _ => return webscrape_audio_metadata(url).await,
    };
}

pub async fn fetch_audio_playlist_metadata_with_fallback(
    url: &YoutubePlaylistUrl<Arc<str>>,
) -> Result<AudioPlaylistMetadata, AppError> {
    if force_using_youtube_web_scraping() {
        return webscrape_audio_playlist_metadata(url).await;
    };

    match YoutubeApiDataFetcher::fetch_audio_playlist_metadata(&envs().youtube_api_key, url).await?
    {
        YoutubeApiResponse::Ok(metadata) => return Ok(metadata),
        _ => return webscrape_audio_playlist_metadata(url).await,
    };
}

pub async fn fetch_audio_playlist_urls_with_fallback(
    url: &YoutubePlaylistUrl<Arc<str>>,
) -> Result<Arc<[YoutubeVideoUrl<Arc<str>>]>, AppError> {
    if force_using_youtube_web_scraping() {
        return webscrape_audio_playlist_urls(url).await;
    };

    match YoutubeApiDataFetcher::fetch_audio_playlist_urls(&envs().youtube_api_key, url).await? {
        YoutubeApiResponse::Ok(urls) => return Ok(urls),
        _ => return webscrape_audio_playlist_urls(url).await,
    };
}

async fn webscrape_audio_metadata(
    url: &YoutubeVideoUrl<Arc<str>>,
) -> Result<AudioTrackMetadata, AppError> {
    let base_url = webscraper_api_url();
    let id = url.id();

    let resp = reqwest::get(format!("{base_url}/youtube/video/{id}"))
        .await
        .into_app_err(
            "failed to fetch youtube video metadata",
            AppErrorKind::Api,
            &[&format!("URL {url}")],
        )?;

    let metadata: AudioTrackMetadata = resp.json().await.into_app_err(
        "failed to fetch youtube video metadata",
        AppErrorKind::Api,
        &[&format!("URL {url}")],
    )?;

    return Ok(metadata);
}

async fn webscrape_audio_playlist_metadata(
    url: &YoutubePlaylistUrl<Arc<str>>,
) -> Result<AudioPlaylistMetadata, AppError> {
    let base_url = webscraper_api_url();
    let id = url.id();
    let resp = reqwest::get(format!("{base_url}/youtube/playlist/{id}"))
        .await
        .into_app_err(
            "failed to fetch youtube playlist metadata",
            AppErrorKind::Api,
            &[&format!("URL {url}")],
        )?;

    let metadata: AudioPlaylistMetadata = resp.json().await.into_app_err(
        "failed to fetch youtube playlist metadata",
        AppErrorKind::Api,
        &[&format!("URL {url}")],
    )?;

    return Ok(metadata);
}

async fn webscrape_audio_playlist_urls(
    url: &YoutubePlaylistUrl<Arc<str>>,
) -> Result<Arc<[YoutubeVideoUrl<Arc<str>>]>, AppError> {
    let base_url = webscraper_api_url();
    let id = url.id();

    let resp = reqwest::get(format!("{base_url}/youtube/playlist/{id}/urls"))
        .await
        .into_app_err(
            "failed to fetch youtube playlist urls",
            AppErrorKind::Api,
            &[&format!("URL {url}")],
        )?;

    let urls: Arc<[YoutubeVideoUrl<Arc<str>>]> = resp.json().await.into_app_err(
        "failed to fetch youtube playlist urls",
        AppErrorKind::Api,
        &[&format!("URL {url}")],
    )?;

    return Ok(urls);
}

fn webscraper_api_url() -> String {
    let host = envs().webscraper_host;
    let port = envs().webscraper_http_port;
    return format!("http://{host}:{port}");
}
