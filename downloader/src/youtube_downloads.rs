use std::{process::Command, sync::Arc};

use audiotorium_core::{
    download::{DownloadClientView, DownloadUpdateNotification},
    error::{AppError, AppErrorKind, IntoAppError},
    identifier::{Identifier, ItemState, YoutubeVideoUrl},
    path::data_shared_audio,
    schema::audio::AudioTrackMetadata,
};
use sqlx::PgPool;

use crate::youtube_api;

#[tracing::instrument]
pub async fn download_single_youtube_video(
    url: &YoutubeVideoUrl<Arc<str>>,
    pool: &PgPool,
) -> DownloadUpdateNotification {
    let view = DownloadClientView::YoutubeVideo { url: url.clone() };

    let tx = match pool.begin().await.into_app_err(
        "failed to start transaction",
        AppErrorKind::Database,
        &[],
    ) {
        Ok(tx) => tx,
        Err(err) => {
            return DownloadUpdateNotification::SingleFailed {
                id: url.uid(),
                view,
                err,
            }
        }
    };

    let path = url.to_path_with_ext(&data_shared_audio());
    let state = url.query_state(&data_shared_audio(), pool).await;
    match state {
        Ok(ItemState::ExistsEverywhere) => {
            return DownloadUpdateNotification::SingleFinished {
                id: url.uid(),
                view,
            };
        }
        Ok(ItemState::ExistsOnlyOnDisk) => {
            match download_and_store_youtube_audio_with_metadata(url, tx).await {
                Ok(metadata) => {
                    return DownloadUpdateNotification::SingleFinished {
                        id: metadata.identifier,
                        view,
                    };
                }
                Err(err) => {
                    return DownloadUpdateNotification::SingleFailed {
                        id: url.uid(),
                        view,
                        err,
                    }
                }
            };
        }
        Ok(ItemState::ExistsOnlyInDatabase) => {
            match download_youtube_audio(url.as_ref(), &path.to_string_lossy()) {
                Ok(()) => {
                    return DownloadUpdateNotification::SingleFinished {
                        id: url.uid(),
                        view,
                    };
                }
                Err(err) => {
                    return DownloadUpdateNotification::SingleFailed {
                        id: url.uid(),
                        view,
                        err,
                    }
                }
            };
        }
        Ok(ItemState::DoesNotExist) => {
            let metadata = match download_and_store_youtube_audio_with_metadata(url, tx).await {
                Ok(metadata) => metadata,
                Err(err) => {
                    return DownloadUpdateNotification::SingleFailed {
                        id: url.uid(),
                        view,
                        err,
                    }
                }
            };

            match download_youtube_audio(url.as_ref(), &path.to_string_lossy()) {
                Ok(()) => {
                    return DownloadUpdateNotification::SingleFinished {
                        id: metadata.identifier,
                        view,
                    };
                }
                Err(err) => {
                    return DownloadUpdateNotification::SingleFailed {
                        id: url.uid(),
                        view,
                        err,
                    }
                }
            };
        }
        Err(err) => {
            return DownloadUpdateNotification::SingleFailed {
                id: url.uid(),
                view,
                err,
            }
        }
    }
}

#[tracing::instrument]
async fn download_and_store_youtube_audio_with_metadata(
    url: &YoutubeVideoUrl<Arc<str>>,
    mut tx: sqlx::Transaction<'_, sqlx::Postgres>,
) -> Result<AudioTrackMetadata, AppError> {
    tracing::info!(
        "download for youtube video with url '{url}' has started",
        url = url.as_ref()
    );

    let metadata = youtube_api::fetch_audio_metadata_with_fallback(url).await?;
    sqlx::query!("INSERT INTO audio_metadata (identifier, name, author, duration, cover_art_url) values ($1, $2, $3, $4, $5)",
                    metadata.identifier.as_ref(),
                    metadata.name.inner_as_ref(),
                    metadata.author.inner_as_ref(),
                    metadata.duration,
                    metadata.cover_art_url.inner_as_ref()
                )
                .execute(&mut *tx)
                .await.into_app_err("failed to store audio metadata", AppErrorKind::Database,
                                    &[&format!("UID: {:?}", metadata.identifier)]
                                    )?;

    tx.commit()
        .await
        .into_app_err("failed to commit transaction", AppErrorKind::Database, &[])?;

    Ok(metadata)
}

#[tracing::instrument]
pub fn download_youtube_audio(url: &str, download_location: &str) -> Result<(), AppError> {
    let out = Command::new("yt-dlp")
        .args([
            "-f",
            "bestaudio",
            "-x",
            "--audio-format",
            "wav",
            "-o",
            download_location,
            url,
        ])
        .output()
        .into_app_err(
            "failed to download youtube video",
            AppErrorKind::Download,
            &[
                &format!("URL: {url}",),
                &format!("PATH: {download_location}"),
            ],
        )?;

    let status_code = out.status.code();
    if status_code.unwrap_or(1) != 0 {
        return Err(AppError::new(
            AppErrorKind::Download,
            "failed to download youtube video",
            &[
                &format!("yt-dlp EXIT CODE: {status_code:?}"),
                &format!("STDERR: {:?}", String::from_utf8(out.stderr)),
            ],
        ));
    }

    return Ok(());
}
