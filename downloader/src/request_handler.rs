use std::{collections::HashSet, sync::Arc};

use actix::{
    Actor, ActorFutureExt, AsyncContext, Context, Handler, Recipient, ResponseActFuture, WrapFuture,
};

use async_trait::async_trait;
use audiotorium_core::{
    database::{store_playlist_if_not_exists, store_playlist_item_relation_if_not_exists},
    download::{
        DownloadAudioRequest, DownloadClientView, DownloadRequestInfo, DownloadRequestSubscriber,
        DownloadUpdateNotification, NotifyDownloadUpdate,
    },
    error::AppError,
    events::{EventQueueMessage, EventQueuePop},
    identifier::{Identifier, YoutubePlaylistUrl, YoutubeVideoUrl},
    logging_utils::log_msg_received,
    tcp_messaging::{TcpMessageHandler, TcpMessageSender},
};
use serde::{Deserialize, Serialize};
use sqlx::PgPool;
use tracing::warn;

use crate::{
    globals::db_pool,
    youtube_api::{self, fetch_audio_playlist_metadata_with_fallback},
    youtube_downloads::download_single_youtube_video,
};

#[derive(Debug)]
pub struct AudioDownloader {
    event_queue: Recipient<EventQueueMessage<AudioDownload>>,
}

#[derive(Debug)]
pub struct AudioDownloaderTcpHandler {
    event_queue: Recipient<EventQueueMessage<AudioDownload>>,
}

impl AudioDownloader {
    const MAX_BATCH_SIZE: usize = 10;

    #[must_use]
    pub fn new(
        event_queue: Recipient<EventQueueMessage<AudioDownload>>,
    ) -> (Self, AudioDownloaderTcpHandler) {
        let tcp_handler = AudioDownloaderTcpHandler {
            event_queue: event_queue.clone(),
        };

        let downloader = AudioDownloader { event_queue };

        return (downloader, tcp_handler);
    }
}

impl Actor for AudioDownloader {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        let self_addr = ctx.address().recipient();

        let res_send = self
            .event_queue
            .try_send(EventQueueMessage::Poll(self_addr));
        debug_assert!(res_send.is_ok(), "Failed to send poll message {res_send:?}");
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AudioDownload {
    /// List of clients to notify about the downloads exit status. Currently this will only ever be
    /// the sender of the download request.
    pub subscribers: HashSet<DownloadRequestSubscriber>,
    pub data: AudioDownloadData,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum AudioDownloadData {
    YoutubeVideo { url: YoutubeVideoUrl<Arc<str>> },
    YoutubePlaylist(YoutubePlaylistDownloadInfo),
}

#[derive(Debug, Clone, PartialEq, Deserialize, Serialize)]
pub struct YoutubePlaylistDownloadInfo {
    pub playlist_url: YoutubePlaylistUrl<Arc<str>>,
    pub video_urls: Arc<[YoutubeVideoUrl<Arc<str>>]>,
}

impl Handler<EventQueuePop<AudioDownload>> for AudioDownloader {
    type Result = ResponseActFuture<Self, ()>;

    fn handle(
        &mut self,
        msg: EventQueuePop<AudioDownload>,
        ctx: &mut Self::Context,
    ) -> Self::Result {
        let event_queue = self.event_queue.clone();
        let self_addr = ctx.address().recipient();

        return Box::pin(
            async move {
                process_queue_item(msg.into_inner(), event_queue, self_addr, db_pool()).await;
            }
            .into_actor(self)
            .map(|_, act, ctx| {
                let res_send = act
                    .event_queue
                    .try_send(EventQueueMessage::Poll(ctx.address().recipient()));

                debug_assert!(res_send.is_ok(), "Failed to send poll message {res_send:?}");
            }),
        );
    }
}

impl AudioDownload {
    async fn construct(request: &DownloadAudioRequest) -> Result<Self, AppError> {
        let info = request.required_info();
        match info {
            DownloadRequestInfo::YoutubeVideo { url } => {
                let data = AudioDownloadData::YoutubeVideo { url: url.clone() };
                return Ok(Self {
                    subscribers: HashSet::from_iter(
                        request.notification_subscriber().into_iter().cloned(),
                    ),
                    data,
                });
            }
            DownloadRequestInfo::YoutubePlaylist { url } => {
                let urls = youtube_api::fetch_audio_playlist_urls_with_fallback(url).await?;
                let data = AudioDownloadData::YoutubePlaylist(YoutubePlaylistDownloadInfo {
                    playlist_url: url.clone(),
                    video_urls: urls,
                });
                return Ok(Self {
                    subscribers: HashSet::from_iter(
                        request.notification_subscriber().into_iter().cloned(),
                    ),

                    data,
                });
            }
        }
    }
}

#[async_trait]
impl TcpMessageHandler<DownloadAudioRequest> for AudioDownloaderTcpHandler {
    #[allow(clippy::implicit_return)]
    async fn handle(&mut self, msg: DownloadAudioRequest) {
        log_msg_received(self, &msg);

        let download = match AudioDownload::construct(&msg).await {
            Ok(download) => download,
            Err(err) => {
                tracing::error!(
                    info = "Failed to construct download from download request",
                    ?err,
                    ?msg
                );
                return;
            }
        };

        let info = DownloadClientView::from(download.data.clone());
        let notification = match &info {
            DownloadClientView::YoutubeVideo { url } => DownloadUpdateNotification::SingleAdded {
                id: url.uid(),
                view: info,
            },
            DownloadClientView::YoutubePlaylist { playlist_url, .. } => {
                DownloadUpdateNotification::BatchAdded {
                    id: playlist_url.uid(),
                    view: info,
                }
            }
        };

        notify_subscribers(&download.subscribers, notification);

        let res_send = self.event_queue.try_send(EventQueueMessage::Push(download));
        debug_assert!(res_send.is_ok(), "Failed to send push message {res_send:?}");
    }
}

impl From<AudioDownloadData> for DownloadClientView {
    fn from(value: AudioDownloadData) -> Self {
        return match value {
            AudioDownloadData::YoutubeVideo { url } => Self::YoutubeVideo { url },
            AudioDownloadData::YoutubePlaylist(YoutubePlaylistDownloadInfo {
                playlist_url,
                video_urls,
            }) => Self::YoutubePlaylist {
                playlist_url,
                video_urls,
            },
        };
    }
}

#[tracing::instrument]
async fn process_queue_item(
    download: AudioDownload,
    event_queue: Recipient<EventQueueMessage<AudioDownload>>,
    self_addr: Recipient<EventQueuePop<AudioDownload>>,
    pool: &PgPool,
) {
    match download.data {
        AudioDownloadData::YoutubeVideo { url } => {
            let notification = download_single_youtube_video(&url, pool).await;
            notify_subscribers(&download.subscribers, notification);
        }
        AudioDownloadData::YoutubePlaylist(YoutubePlaylistDownloadInfo {
            ref playlist_url,
            video_urls,
        }) => {
            match store_playlist_if_not_exists(
                playlist_url,
                db_pool(),
                fetch_audio_playlist_metadata_with_fallback,
            )
            .await
            {
                Ok(_) => {}
                Err(err) => {
                    let notification = DownloadUpdateNotification::BatchDownloadFailedToStart {
                        id: playlist_url.uid(),
                        batch_view: DownloadClientView::YoutubePlaylist {
                            playlist_url: playlist_url.clone(),
                            video_urls,
                        },
                        err,
                    };
                    notify_subscribers(&download.subscribers, notification);
                    return;
                }
            }

            let (videos_to_process, videos_for_next_batch) =
                if AudioDownloader::MAX_BATCH_SIZE > video_urls.len() {
                    (video_urls.as_ref(), Default::default())
                } else {
                    video_urls.split_at(AudioDownloader::MAX_BATCH_SIZE)
                };

            for url in videos_to_process {
                let view = DownloadClientView::YoutubeVideo { url: url.clone() };

                let notification = download_single_youtube_video(url, pool).await;
                let notification = match notification {
                    DownloadUpdateNotification::SingleFailed { .. } => notification,
                    _ => {
                        let res = store_playlist_item_relation_if_not_exists(
                            &playlist_url.uid(),
                            &url.uid(),
                            db_pool(),
                        )
                        .await;
                        match res {
                            Ok(()) => notification,
                            Err(err) => DownloadUpdateNotification::SingleFailed {
                                id: url.uid(),
                                err,
                                view,
                            },
                        }
                    }
                };

                notify_subscribers(&download.subscribers, notification);
            }

            if videos_for_next_batch.is_empty() {
                let notification = DownloadUpdateNotification::BatchFinished {
                    id: playlist_url.uid(),
                };
                notify_subscribers(&download.subscribers, notification);
            } else {
                let next_batch = AudioDownloadData::YoutubePlaylist(YoutubePlaylistDownloadInfo {
                    playlist_url: playlist_url.clone(),
                    video_urls: videos_for_next_batch.into(),
                });

                let notification = DownloadUpdateNotification::BatchUpdated {
                    id: playlist_url.uid(),
                    batch_view: DownloadClientView::YoutubePlaylist {
                        playlist_url: playlist_url.clone(),
                        video_urls,
                    },
                };
                notify_subscribers(&download.subscribers, notification);

                let download_next = AudioDownload {
                    subscribers: download.subscribers,
                    data: next_batch,
                };

                let res_send = event_queue.try_send(EventQueueMessage::Push(download_next));
                debug_assert!(res_send.is_ok(), "Failed to send push message {res_send:?}");
            }
        }
    }
}

fn notify_subscribers(
    subscribers: &HashSet<DownloadRequestSubscriber>,
    update: DownloadUpdateNotification,
) {
    for subscriber in subscribers {
        let res = subscriber.addr().do_send(&NotifyDownloadUpdate {
            identifier: Some(subscriber.identifier()),
            update: update.clone(),
        });

        if let Err(err) = res {
            warn!(info = "Failed to encode download update notification message. Didn't send message.", %err);
        }
    }
}
