use std::{fs, path::PathBuf};

use audiotorium_core::path;

#[tracing::instrument]
pub fn setup_paths() -> Result<(), std::io::Error> {
    path::init()?;
    fs::create_dir_all(crate_dir())?;
    fs::create_dir_all(event_data_dir())?;

    return Ok(());
}

pub fn event_data_dir() -> PathBuf {
    return crate_dir().join("events");
}

fn crate_dir() -> PathBuf {
    return path::base().join(std::env!("CARGO_BIN_NAME"));
}
