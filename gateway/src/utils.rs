use std::{collections::HashMap, fmt::Debug, fs};

use actix::Addr;
use anyhow::{anyhow, Context};
use cpal::{
    traits::{DeviceTrait, HostTrait},
    Device, SampleRate, StreamConfig,
};
use serde::{Deserialize, Serialize};

use crate::{
    gateway::server::{CentralGateway, GetAudioNodeMessage},
    node::node_server::{AudioNode, SourceName},
    path::audio_devices_file,
};

const DEFAULT_SAMPLE_RATE: u32 = 48000;

pub async fn get_node_by_source_name(
    source_name: SourceName,
    addr: &Addr<CentralGateway>,
) -> Option<Addr<AudioNode>> {
    addr.send(GetAudioNodeMessage { source_name }).await.ok()?
}

pub fn setup_device(source_name: &str) -> anyhow::Result<(Device, StreamConfig)> {
    let host = cpal::default_host();
    let device = host
        .output_devices()?
        .find(|dev| return dev.name().map(|v| return v == source_name).unwrap_or(false))
        .ok_or(anyhow!("no device with source name {source_name} found"))?;

    let mut supported_configs_range = device.supported_output_configs()?;

    let supported_config = supported_configs_range
        .next()
        .ok_or(anyhow!("no config found"))?;

    let channel_count = 2; // I choose to make this assumption not because it is good
                           // but because it is easy

    let config = supported_config
        .with_sample_rate(SampleRate(DEFAULT_SAMPLE_RATE * channel_count))
        .into();

    return Ok((device, config));
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AudioSourceInfo {
    pub human_readable_name: String,
}

pub type Sources = HashMap<SourceName, AudioSourceInfo>;

pub fn get_audio_sources() -> Sources {
    let path = audio_devices_file();
    let source_str = fs::read_to_string(&path)
        .with_context(|| format!("No config file at {path:#?}"))
        .expect("Audio device file must exists.");

    return toml::from_str(&source_str).expect("Sources file must be valid toml.");
}
