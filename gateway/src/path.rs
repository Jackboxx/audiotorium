use std::{fs, path::PathBuf};

use audiotorium_core::path;

pub fn setup_paths() -> Result<(), std::io::Error> {
    path::init()?;

    fs::create_dir_all(crate_dir())?;
    fs::create_dir_all(config_dir())?;

    return Ok(());
}

pub fn state_recovery_file_path() -> PathBuf {
    return crate_dir().join("state-recovery-info");
}

pub fn audio_devices_file() -> PathBuf {
    return config_dir().join("audio-devices.toml");
}

fn config_dir() -> PathBuf {
    return crate_dir().join("config");
}

fn crate_dir() -> PathBuf {
    return path::base().join(std::env!("CARGO_BIN_NAME"));
}
