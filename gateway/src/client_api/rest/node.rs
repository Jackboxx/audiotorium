use actix_web::{http::StatusCode, post, web, HttpResponse};
use audiotorium_core::node::commands::AudioNodeCommand;

use crate::{gateway_addr, node::node_server::SourceName, utils::get_node_by_source_name};

#[post("/commands/node/{source_name}")]
pub async fn receive_node_cmd(
    source_name: web::Path<SourceName>,
    cmd: web::Json<AudioNodeCommand>,
) -> HttpResponse {
    let node_addr = match get_node_by_source_name(source_name.into_inner(), gateway_addr()).await {
        Some(addr) => addr,
        None => {
            return HttpResponse::new(StatusCode::NOT_FOUND);
        }
    };

    match node_addr.send(cmd.into_inner()).await {
        Ok(()) => HttpResponse::new(StatusCode::OK),
        Err(_) => HttpResponse::new(StatusCode::INTERNAL_SERVER_ERROR),
    }
}
