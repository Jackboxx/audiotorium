use std::sync::Arc;

use actix_web::{get, web, HttpResponse};
use audiotorium_core::{
    database::{
        select_all_audio_metadata_from_db, select_all_playlist_metadata_from_db,
        select_audio_metadata_from_db, select_audio_playlist_metadata_from_db,
        select_playlist_items_from_db,
    },
    identifier::ItemUid,
};
use serde::Deserialize;

use crate::db_pool;

pub mod search;
mod youtube_search;

#[derive(Deserialize)]
struct OffsetLimitParams {
    limit: Option<i64>,
    offset: Option<i64>,
}

#[get("/data/audio_playlists")]
pub async fn get_audio_playlists(
    web::Query(OffsetLimitParams { limit, offset }): web::Query<OffsetLimitParams>,
) -> HttpResponse {
    match select_all_playlist_metadata_from_db(limit, offset, db_pool()).await {
        Ok(items) => HttpResponse::Ok()
            .body(serde_json::to_string(&items).unwrap_or("oops something went wrong".to_owned())),
        Err(err) => HttpResponse::InternalServerError()
            .body(serde_json::to_string(&err).unwrap_or("oops something went wrong".to_owned())),
    }
}

#[get("/data/audio")]
pub async fn get_audio(
    web::Query(OffsetLimitParams { limit, offset }): web::Query<OffsetLimitParams>,
) -> HttpResponse {
    match select_all_audio_metadata_from_db(limit, offset, db_pool()).await {
        Ok(items) => HttpResponse::Ok()
            .body(serde_json::to_string(&items).unwrap_or("oops something went wrong".to_owned())),
        Err(err) => HttpResponse::InternalServerError()
            .body(serde_json::to_string(&err).unwrap_or("oops something went wrong".to_owned())),
    }
}

#[get("/data/audio/{audio_uid}")]
pub async fn get_single_audio(audio_uid: web::Path<Arc<str>>) -> HttpResponse {
    let uid = ItemUid::new(audio_uid.into_inner());
    match select_audio_metadata_from_db(&uid, db_pool()).await {
        Ok(Some(metadata)) => HttpResponse::Ok().body(
            serde_json::to_string(&metadata).unwrap_or("oops something went wrong".to_owned()),
        ),
        Ok(None) => {
            HttpResponse::NotFound().body(format!("no metadata found for uid {}", uid.as_ref()))
        }
        Err(err) => HttpResponse::InternalServerError()
            .body(serde_json::to_string(&err).unwrap_or("oops something went wrong".to_owned())),
    }
}

#[get("/data/audio_playlists/{playlist_uid}")]
pub async fn get_single_audio_playlist(playlist_uid: web::Path<Arc<str>>) -> HttpResponse {
    let uid = ItemUid::new(playlist_uid.into_inner());
    match select_audio_playlist_metadata_from_db(&uid, db_pool()).await {
        Ok(Some(metadata)) => HttpResponse::Ok().body(
            serde_json::to_string(&metadata).unwrap_or("oops something went wrong".to_owned()),
        ),
        Ok(None) => {
            HttpResponse::NotFound().body(format!("no metadata found for uid {}", uid.as_ref()))
        }
        Err(err) => HttpResponse::InternalServerError()
            .body(serde_json::to_string(&err).unwrap_or("oops something went wrong".to_owned())),
    }
}

#[get("/data/audio_playlists/{playlist_uid}/items")]
pub async fn get_audio_in_audio_playlist(
    playlist_uid: web::Path<Arc<str>>,
    web::Query(OffsetLimitParams { limit, offset }): web::Query<OffsetLimitParams>,
) -> HttpResponse {
    let uid = ItemUid::new(playlist_uid.into_inner());
    match select_playlist_items_from_db(&uid, limit, offset, db_pool()).await {
        Ok(items) => HttpResponse::Ok()
            .body(serde_json::to_string(&items).unwrap_or("oops something went wrong".to_owned())),
        Err(err) => HttpResponse::InternalServerError()
            .body(serde_json::to_string(&err).unwrap_or("oops something went wrong".to_owned())),
    }
}
