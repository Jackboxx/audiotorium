use audiotorium_core::{
    error::{AppError, AppErrorKind, IntoAppError},
    external_apis::youtube::{YoutubeApiDataFetcher, YoutubeApiResponse, YoutubeSearchResult},
};

use crate::env::envs;

pub async fn fetch_search_results_with_fallback(
    search: &str,
    limit: usize,
) -> Result<Vec<YoutubeSearchResult>, AppError> {
    match YoutubeApiDataFetcher::fetch_search_results(&envs().youtube_api_key, search, limit)
        .await?
    {
        YoutubeApiResponse::Ok(results) => return Ok(results),
        _ => return webscrape_search_results(search, limit).await,
    };
}

async fn webscrape_search_results(
    search: &str,
    limit: usize,
) -> Result<Vec<YoutubeSearchResult>, AppError> {
    let base_url = webscraper_api_url();
    let search_as_param = urlencoding::encode(search);
    let resp = reqwest::get(format!(
        "{base_url}/youtube/search?search={search_as_param}&limit={limit}"
    ))
    .await
    .into_app_err(
        "failed to fetch youtube search results",
        AppErrorKind::Api,
        &[&format!("SEARCH {search}"), &format!("LIMIT {limit}")],
    )?;

    let results: Vec<YoutubeSearchResult> = resp.json().await.into_app_err(
        "failed to fetch youtube search results",
        AppErrorKind::Api,
        &[&format!("SEARCH {search}"), &format!("LIMIT {limit}")],
    )?;

    return Ok(results);
}

fn webscraper_api_url() -> String {
    let host = envs().webscraper_host;
    let port = envs().webscraper_http_port;
    return format!("http://{host}:{port}");
}
