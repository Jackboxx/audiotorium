use std::collections::HashSet;

use itertools::Itertools;

use actix_web::{get, web, HttpResponse};
use audiotorium_core::{
    error::AppError, identifier::AudioOrigin, parsing::deserialize_stringified_set,
    schema::audio::SearchAudioResult,
};
use serde::Deserialize;

use crate::database::search::{search_for_local_audio, yt_into_audio_search_result};

use super::youtube_search::fetch_search_results_with_fallback;

async fn search_audio(
    origin: AudioOrigin,
    search: &str,
    limit: usize,
) -> Result<Vec<Option<SearchAudioResult>>, AppError> {
    match origin {
        AudioOrigin::Local => search_for_local_audio(search, limit)
            .await
            .map(|results| return results.into_iter().map(Some).collect()),
        AudioOrigin::Youtube => {
            let search_results = fetch_search_results_with_fallback(search, limit).await?;

            let mut results = Vec::with_capacity(limit);
            for res in search_results {
                let audio_search_result = yt_into_audio_search_result(res, search).await.ok();
                results.push(audio_search_result)
            }

            Ok(results)
        }
    }
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub(super) struct SearchParams {
    search: String,
    limit: Option<usize>,
    min_score: Option<f32>,
    #[serde(deserialize_with = "deserialize_stringified_set")]
    origins: HashSet<AudioOrigin>,
}

#[get("/data/search/audio")]
pub async fn search_audio_data(
    web::Query(SearchParams {
        search,
        limit,
        min_score,
        origins,
    }): web::Query<SearchParams>,
) -> HttpResponse {
    let limit = limit.unwrap_or(25);
    let per_origin_limits: Vec<usize> = origins
        .iter()
        .enumerate()
        .map(|(i, _)| {
            let is_last_element = i == origins.len() - 1;
            if is_last_element {
                return limit / origins.len() + limit % origins.len();
            }
            return limit / origins.len();
        })
        .collect();

    assert_eq!(per_origin_limits.iter().sum::<usize>(), limit);

    let mut results = Vec::with_capacity(limit);
    for (origin, limit) in origins.into_iter().zip(per_origin_limits) {
        match search_audio(origin, &search, limit).await {
            Ok(mut items) => {
                results.append(&mut items);
            }
            Err(err) => {
                return HttpResponse::InternalServerError().body(
                    serde_json::to_string(&err).unwrap_or("oops something went wrong".to_owned()),
                );
            }
        }
    }

    let min_score = min_score.unwrap_or(0.0);
    let results = results.iter().flatten().collect();
    let items = filter_search_results(results, min_score);

    HttpResponse::Ok()
        .body(serde_json::to_string(&items).unwrap_or("oops something went wrong".to_owned()))
}

fn filter_search_results(
    items: Vec<&SearchAudioResult>,
    min_score: f32,
) -> Vec<&SearchAudioResult> {
    let deduped_items = items
        .into_iter()
        .filter(|x| return x.score >= min_score)
        .sorted_by(|a, b| return b.identifier.cmp(&a.identifier))
        .unique_by(|x| return x.identifier.try_uid());

    return deduped_items.sorted_by(|a, b| return b.cmp(a)).collect();
}
