use actix_web::{post, web, HttpResponse};
use audiotorium_core::central_gateway::commands::CentralGatewayCommand;
use reqwest::StatusCode;

use crate::gateway_addr;

#[post("/commands/gateway")]
pub async fn receive_gateway_cmd(cmd: web::Json<CentralGatewayCommand>) -> HttpResponse {
    match gateway_addr().send(cmd.into_inner()).await {
        Ok(res) => match res {
            Ok(()) => HttpResponse::new(StatusCode::OK),
            Err(err) => HttpResponse::InternalServerError().body(
                serde_json::to_string(&err).unwrap_or("oops something went wrong".to_owned()),
            ),
        },
        Err(_) => HttpResponse::new(StatusCode::INTERNAL_SERVER_ERROR),
    }
}
