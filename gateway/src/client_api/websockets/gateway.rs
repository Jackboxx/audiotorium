use std::sync::Arc;

use actix_web::{get, http::StatusCode, web, HttpRequest, HttpResponse};
use actix_web_actors::ws;
use audiotorium_core::{
    central_gateway::streams::CentralGatewayInfoStreamType, parsing::deserialize_stringified_list,
};
use serde::Deserialize;

use crate::{gateway::session::CentralGatewaySession, gateway_addr};

#[derive(Debug, Clone, Deserialize)]
struct StreamWantedInfoParams {
    #[serde(deserialize_with = "deserialize_stringified_list")]
    wanted_info: Arc<[CentralGatewayInfoStreamType]>,
}

#[get("/streams/gateway")]
pub async fn start_gateway_websocket_con(
    query: web::Query<StreamWantedInfoParams>,
    req: HttpRequest,
    stream: web::Payload,
) -> HttpResponse {
    match ws::start(
        CentralGatewaySession::new(gateway_addr().clone(), query.into_inner().wanted_info),
        &req,
        stream,
    ) {
        Ok(res) => res,
        Err(_) => HttpResponse::new(StatusCode::INTERNAL_SERVER_ERROR),
    }
}
