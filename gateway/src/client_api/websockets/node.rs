use std::sync::Arc;

use actix_web::{get, web, HttpRequest, HttpResponse};
use actix_web_actors::ws;
use audiotorium_core::{
    node::streams::AudioNodeInfoStreamType, parsing::deserialize_stringified_list,
};
use reqwest::StatusCode;
use serde::Deserialize;

use crate::{
    gateway_addr,
    node::{node_server::SourceName, node_session::AudioNodeSession},
    utils::get_node_by_source_name,
};

#[derive(Debug, Clone, Deserialize)]
struct StreamWantedInfoParams {
    #[serde(deserialize_with = "deserialize_stringified_list")]
    wanted_info: Arc<[AudioNodeInfoStreamType]>,
}

#[get("/streams/node/{source_name}")]
pub async fn start_node_websocket_con(
    source_name: web::Path<SourceName>,
    query: web::Query<StreamWantedInfoParams>,
    req: HttpRequest,
    stream: web::Payload,
) -> HttpResponse {
    let node_addr = match get_node_by_source_name(source_name.into_inner(), gateway_addr()).await {
        Some(addr) => addr,
        None => {
            return HttpResponse::new(StatusCode::NOT_FOUND);
        }
    };

    match ws::start(
        AudioNodeSession::new(node_addr, query.into_inner().wanted_info),
        &req,
        stream,
    ) {
        Ok(res) => res,
        Err(_) => HttpResponse::new(StatusCode::INTERNAL_SERVER_ERROR),
    }
}
