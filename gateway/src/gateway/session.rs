use std::sync::Arc;

use actix::{
    Actor, ActorContext, ActorFutureExt, Addr, AsyncContext, ContextFutureSpawner, Handler,
    ResponseActFuture, Running, StreamHandler, WrapFuture,
};

use actix_web_actors::ws;
use audiotorium_core::{
    central_gateway::streams::{CentralGatewayInfoStreamMessage, CentralGatewayInfoStreamType},
    error::AppError,
    stream_utils::HeartBeat,
};

use crate::gateway::server::{CentralGatewayConnectMessage, CentralGatewayDisconnect};

use super::server::CentralGateway;

#[derive(Debug, Clone)]
pub struct CentralGatewaySession {
    id: usize,
    server_addr: Addr<CentralGateway>,
    wanted_info: Arc<[CentralGatewayInfoStreamType]>,
}

impl CentralGatewaySession {
    pub fn new(
        server_addr: Addr<CentralGateway>,
        wanted_info: Arc<[CentralGatewayInfoStreamType]>,
    ) -> Self {
        return Self {
            id: usize::MAX,
            server_addr,
            wanted_info,
        };
    }
}

impl Actor for CentralGatewaySession {
    type Context = ws::WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        tracing::info!("Stared new 'CentralGatewaySession'.");

        let addr = ctx.address();
        self.server_addr
            .send(CentralGatewayConnectMessage {
                addr,
                wanted_info: Arc::clone(&self.wanted_info),
            })
            .into_actor(self)
            .then(|res, act, ctx| {
                match res {
                    Ok(res) => {
                        tracing::info!("'CentralGatewaySession' connected.");
                        act.id = res.id;

                        ctx.text(
                            serde_json::to_string(&res.connection_response)
                                .unwrap_or("failed to serialize on server".to_owned()),
                        );

                        ctx.notify(HeartBeat);
                    }

                    Err(err) => {
                        tracing::error!(
                            info = "'CentralGatewaySession' failed to connect to 'CentralGateway'.",
                            ?err,
                        );
                        ctx.stop();
                    }
                }

                return actix::fut::ready(());
            })
            .wait(ctx);
    }

    fn stopping(&mut self, _: &mut Self::Context) -> Running {
        tracing::info!(info = "'CentralGatewaySession' stopping", %self.id);

        self.server_addr
            .do_send(CentralGatewayDisconnect { id: self.id });
        return Running::Stop;
    }
}

impl Handler<HeartBeat> for CentralGatewaySession {
    type Result = ResponseActFuture<Self, ()>;

    fn handle(&mut self, _msg: HeartBeat, ctx: &mut Self::Context) -> Self::Result {
        ctx.ping(b"heart-beat");
        return Box::pin(
            async {
                actix_rt::time::sleep(std::time::Duration::from_millis(333)).await;
            }
            .into_actor(self)
            .map(|_res, _act, ctx| ctx.notify(HeartBeat)),
        );
    }
}

impl Handler<CentralGatewayInfoStreamMessage> for CentralGatewaySession {
    type Result = ();

    /// used to receive multicast messages from nodes
    fn handle(
        &mut self,
        msg: CentralGatewayInfoStreamMessage,
        ctx: &mut Self::Context,
    ) -> Self::Result {
        if self.wanted_info.contains(&msg.msg_type()) {
            ctx.text(
                serde_json::to_string(&msg)
                    .unwrap_or(String::from("failed to serialize on server")),
            )
        }
    }
}

impl Handler<AppError> for CentralGatewaySession {
    type Result = ();

    /// used to receive multicast messages from nodes
    fn handle(&mut self, msg: AppError, ctx: &mut Self::Context) -> Self::Result {
        ctx.text(
            serde_json::to_string(&msg).unwrap_or(String::from("failed to serialize on server")),
        )
    }
}

impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for CentralGatewaySession {
    fn handle(&mut self, msg: Result<ws::Message, ws::ProtocolError>, ctx: &mut Self::Context) {
        match &msg {
            Ok(ws::Message::Text(_text)) => {}
            Ok(ws::Message::Close(reason)) => {
                ctx.close(reason.clone());
                ctx.stop();
            }
            _ => {}
        }
    }
}
