use std::{collections::HashMap, sync::Arc};

use actix::{Actor, Addr, AsyncContext, Context, Handler, Message};
use audiotorium_core::{
    central_gateway::streams::{CentralGatewayInfoStreamMessage, RunningDownload},
    download::{DownloadState, DownloadUpdateNotification},
    identifier::ItemUid,
    logging_utils::log_msg_received,
    node::{AudioNodeHealth, AudioNodeInfo},
    schema::audio::AudioInfoFull,
};

use crate::{
    audio_playback::audio_player::AudioPlayer,
    node::node_server::{AudioNode, SourceName},
    state_storage::{restore_state_actor::RestoreStateActor, AppStateRecoveryInfo, AudioStateInfo},
    utils::get_audio_sources,
};

use super::session::CentralGatewaySession;

mod commands;
mod connect;

pub use commands::request_audio_from_downloader;
pub use connect::{CentralGatewayConnectMessage, CentralGatewayDisconnect};

#[derive(Debug)]
pub struct CentralGateway {
    restore_state_addr: Addr<RestoreStateActor>,
    restored_state: AppStateRecoveryInfo,
    nodes: HashMap<SourceName, (Addr<AudioNode>, AudioNodeInfo)>,
    running_downloads: HashMap<(u64, ItemUid<Arc<str>>), RunningDownload>,
    sessions: HashMap<usize, Addr<CentralGatewaySession>>,
}

#[derive(Debug, Clone, Message)]
#[rtype(result = "()")]
pub enum MulticastToNodes {
    DownloadUpdate {
        node_id: u32,
        request_id: u64,
        notification: DownloadUpdateNotification,
    },
}

#[derive(Debug, Clone, Message)]
#[rtype(result = "Option<Addr<AudioNode>>")]
pub struct GetAudioNodeMessage {
    pub source_name: SourceName,
}

#[derive(Debug, Clone, Message)]
#[rtype(result = "()")]
pub enum AudioNodeToCentralGatewayMessage {
    NodeHealthUpdate((SourceName, AudioNodeHealth)),
}

impl CentralGateway {
    pub fn new(
        restore_state_addr: Addr<RestoreStateActor>,
        restored_state: AppStateRecoveryInfo,
    ) -> Self {
        return Self {
            restore_state_addr,
            restored_state,
            nodes: HashMap::default(),
            sessions: HashMap::default(),
            running_downloads: HashMap::default(),
        };
    }

    fn multicast<M>(&self, msg: M)
    where
        M: Message + Send + Clone + 'static,
        M::Result: Send,
        CentralGatewaySession: Handler<M>,
    {
        for addr in self.sessions.values() {
            addr.do_send(msg.clone());
        }
    }
}

impl Actor for CentralGateway {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        ctx.set_mailbox_capacity(64);
        tracing::info!("Stared new 'CentralGateway'.");

        for (source_name, info) in get_audio_sources().into_iter() {
            let (restored_state, restored_queue) =
                match self.restored_state.audio_info.get(&source_name).cloned() {
                    Some(AudioStateInfo {
                        playback_state,
                        current_queue_index,
                        audio_progress,
                        audio_volume,
                        restored_queue,
                        ..
                    }) => (
                        AudioInfoFull {
                            playback_state,
                            current_queue_index,
                            audio_progress,
                            audio_volume,
                        },
                        restored_queue,
                    ),
                    None => Default::default(),
                };

            if let Ok(player) =
                AudioPlayer::try_new(source_name.to_owned(), None, restored_state, restored_queue)
            {
                let node = AudioNode::new(
                    source_name.to_owned(),
                    player,
                    ctx.address(),
                    self.restore_state_addr.clone(),
                );
                let node_addr = node.start();

                self.nodes.insert(
                    source_name.to_owned(),
                    (
                        node_addr,
                        AudioNodeInfo {
                            source_name,
                            human_readable_name: info.human_readable_name.clone(),
                            health: AudioNodeHealth::Good,
                        },
                    ),
                );
            }
        }
    }
}

impl Handler<AudioNodeToCentralGatewayMessage> for CentralGateway {
    type Result = ();

    fn handle(
        &mut self,
        msg: AudioNodeToCentralGatewayMessage,
        _ctx: &mut Self::Context,
    ) -> Self::Result {
        log_msg_received(&self, &msg);

        match &msg {
            AudioNodeToCentralGatewayMessage::NodeHealthUpdate(params) => {
                let (source_name, health) = params;

                if let Some((_, node_info)) = self.nodes.get_mut(source_name) {
                    node_info.health = health.clone();

                    let msg = CentralGatewayInfoStreamMessage::NodeInfo(
                        self.nodes
                            .values()
                            .map(|(_, info)| return info.to_owned())
                            .collect(),
                    );

                    self.multicast(msg)
                }
            }
        }
    }
}

impl Handler<GetAudioNodeMessage> for CentralGateway {
    type Result = Option<Addr<AudioNode>>;

    fn handle(&mut self, msg: GetAudioNodeMessage, _ctx: &mut Self::Context) -> Self::Result {
        log_msg_received(&self, &msg);

        return self
            .nodes
            .get(&msg.source_name)
            .and_then(|v| match v.1.health {
                AudioNodeHealth::Poor(_) => return None,
                _ => return Some(v.0.clone()),
            });
    }
}

impl Handler<MulticastToNodes> for CentralGateway {
    type Result = ();

    fn handle(&mut self, msg: MulticastToNodes, _ctx: &mut Self::Context) -> Self::Result {
        log_msg_received(&self, &msg);

        match &msg {
            MulticastToNodes::DownloadUpdate {
                request_id,
                notification,
                ..
            } => {
                update_running_downloads(self, *request_id, notification.clone());

                let all_downloads = self.running_downloads.values().cloned().collect();
                self.multicast(CentralGatewayInfoStreamMessage::Downloads(all_downloads));
            }
        };

        for node in self.nodes.values() {
            node.0.do_send(msg.clone());
        }

        fn update_running_downloads(
            gateway: &mut CentralGateway,
            request_id: u64,
            notification: DownloadUpdateNotification,
        ) {
            match notification {
                DownloadUpdateNotification::SingleAdded { view, id }
                | DownloadUpdateNotification::BatchAdded { view, id } => {
                    gateway.running_downloads.insert(
                        (request_id, id),
                        RunningDownload {
                            view,
                            state: DownloadState::Pending,
                        },
                    );
                }
                DownloadUpdateNotification::BatchUpdated {
                    ref batch_view,
                    ref id,
                } => {
                    if let Some(download) =
                        gateway.running_downloads.get_mut(&(request_id, id.clone()))
                    {
                        download.state = DownloadState::Active;
                        download.view = batch_view.clone();
                    } else {
                        tracing::warn!(info = "Received notification of download update for unkown download.", %request_id,  ?notification);
                    }
                }
                DownloadUpdateNotification::SingleFinished { id, .. } => {
                    gateway.running_downloads.remove(&(request_id, id));
                }
                DownloadUpdateNotification::BatchFinished { id } => {
                    gateway.running_downloads.remove(&(request_id, id));
                }
                DownloadUpdateNotification::SingleFailed {
                    ref err, ref id, ..
                } => {
                    if let Some(download) =
                        gateway.running_downloads.get_mut(&(request_id, id.clone()))
                    {
                        download.state = DownloadState::Failed {
                            reason: err.to_string().into(),
                        };
                    } else {
                        tracing::warn!(info = "Received notification of download failure for unkown download.", %request_id, ?err, ?notification);
                    }
                }
                DownloadUpdateNotification::BatchDownloadFailedToStart { id, .. } => {
                    gateway.running_downloads.remove(&(request_id, id));
                }
            }
        }
    }
}
