use std::net::{IpAddr, SocketAddr, SocketAddrV4};

use actix::Handler;
use audiotorium_core::{
    central_gateway::commands::CentralGatewayCommand,
    download::{DownloadAudioRequest, DownloadRequestInfo, DownloadRequestSubscriber},
    error::AppError,
    identifier::{AudioIdentifier, YoutubePlaylistUrl, YoutubeVideoUrl},
    logging_utils::log_msg_received,
    tcp_messaging::TcpMessageSender,
};

use crate::{env::envs, node::node_server::NodeRequestId};

use super::CentralGateway;

impl Handler<CentralGatewayCommand> for CentralGateway {
    type Result = Result<(), AppError>;

    fn handle(&mut self, msg: CentralGatewayCommand, _ctx: &mut Self::Context) -> Self::Result {
        log_msg_received(&self, &msg);

        match msg {
            CentralGatewayCommand::DownloadAudio(params) => {
                request_audio_from_downloader(params.identifier, None);
            }
        }

        return Ok(());
    }
}

/// Query the downloader service to start an audio download.
pub fn request_audio_from_downloader(identifier: AudioIdentifier, node_id: Option<NodeRequestId>) {
    let Some(download_info) = parse_ident_into_download(identifier) else {
        return;
    };

    let node_subscriber = match node_id {
        Some(node_id) => {
            let addr_self = SocketAddr::V4(SocketAddrV4::new(
                envs().gateway_host,
                envs().gateway_tcp_msg_port,
            ));
            let node_subscriber = DownloadRequestSubscriber::new(addr_self, node_id.as_u128());
            Some(node_subscriber)
        }
        None => None,
    };

    let download = DownloadAudioRequest::new(node_subscriber, download_info);

    let addr_downloader = SocketAddr::new(
        IpAddr::V4(envs().downloader_host),
        envs().downloader_tcp_msg_port,
    );

    let res_send = addr_downloader.do_send(&download);
    debug_assert!(
        res_send.is_ok(),
        "Failed to send audio download msg {res_send:?}"
    );

    fn parse_ident_into_download(ident: AudioIdentifier) -> Option<DownloadRequestInfo> {
        match ident {
            AudioIdentifier::Youtube { raw_url } => {
                if let Ok(yt_url) = YoutubeVideoUrl::try_new(&raw_url) {
                    return Some(DownloadRequestInfo::YoutubeVideo { url: yt_url });
                }

                if let Ok(yt_url) = YoutubePlaylistUrl::try_new(&raw_url) {
                    return Some(DownloadRequestInfo::YoutubePlaylist { url: yt_url });
                }

                tracing::warn!(
                    info = "Received invalid audio identifier. Youtube URL is not valid.",
                    %raw_url
                );

                return None;
            }
            AudioIdentifier::Local { .. } => {
                return None;
            }
        };
    }
}
