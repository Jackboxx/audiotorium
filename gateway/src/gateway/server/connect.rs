use std::sync::Arc;

use actix::{Addr, Handler, Message, MessageResponse};
use audiotorium_core::{
    central_gateway::streams::{
        CentralGatewayInfoStreamType, CentralGatewaySessionConnectedMessage,
    },
    logging_utils::log_msg_received,
    wrappers::ArcSlice,
};

use crate::gateway::session::CentralGatewaySession;

use super::CentralGateway;

#[derive(Debug, Clone, Message)]
#[rtype(result = "CentralGatewayConnectResponse")]
pub struct CentralGatewayConnectMessage {
    pub addr: Addr<CentralGatewaySession>,
    pub wanted_info: Arc<[CentralGatewayInfoStreamType]>,
}

#[derive(Debug, Clone, MessageResponse)]
pub struct CentralGatewayConnectResponse {
    pub id: usize,
    pub connection_response: CentralGatewaySessionConnectedMessage,
}

#[derive(Debug, Clone, Message)]
#[rtype(result = "()")]
pub struct CentralGatewayDisconnect {
    pub id: usize,
}

impl Handler<CentralGatewayConnectMessage> for CentralGateway {
    type Result = CentralGatewayConnectResponse;

    fn handle(
        &mut self,
        msg: CentralGatewayConnectMessage,
        _ctx: &mut Self::Context,
    ) -> Self::Result {
        log_msg_received(&self, &msg);

        let CentralGatewayConnectMessage { addr, wanted_info } = msg;
        let id = self.sessions.keys().max().unwrap_or(&0) + 1;

        self.sessions.insert(id, addr);

        let mut node_info = None;
        let mut downloads = None;
        if wanted_info.contains(&CentralGatewayInfoStreamType::NodeInfo) {
            node_info = Some(
                self.nodes
                    .values()
                    .map(|(_, info)| return info.to_owned())
                    .collect(),
            );
        };

        if wanted_info.contains(&CentralGatewayInfoStreamType::Downloads) {
            downloads = Some(ArcSlice::from_iter(
                self.running_downloads.values().cloned(),
            ));
        };

        return CentralGatewayConnectResponse {
            id,
            connection_response: CentralGatewaySessionConnectedMessage::SessionConnectedResponse {
                node_info,
                downloads,
            },
        };
    }
}

impl Handler<CentralGatewayDisconnect> for CentralGateway {
    type Result = ();
    fn handle(&mut self, msg: CentralGatewayDisconnect, _ctx: &mut Self::Context) -> Self::Result {
        log_msg_received(&self, &msg);

        let CentralGatewayDisconnect { id } = msg;
        self.sessions.remove(&id);
    }
}
