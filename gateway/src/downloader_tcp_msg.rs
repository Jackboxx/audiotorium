use async_trait::async_trait;
use audiotorium_core::{
    download::NotifyDownloadUpdate, logging_utils::log_msg_received,
    tcp_messaging::TcpMessageHandler,
};

use crate::{
    env::envs, gateway::server::MulticastToNodes, gateway_addr, node::node_server::NodeRequestId,
};

pub struct DownloaderNotificationHandler;

#[async_trait]
impl TcpMessageHandler<NotifyDownloadUpdate> for DownloaderNotificationHandler {
    #[allow(clippy::implicit_return)]
    async fn handle(&mut self, msg: NotifyDownloadUpdate) {
        log_msg_received(self, &msg);

        let NotifyDownloadUpdate { update, identifier } = msg;
        let Some(identifier) = identifier else {
            return;
        };

        let download_id = NodeRequestId::from(identifier);
        debug_assert_eq!(envs().device_id, download_id.device_id());

        gateway_addr().do_send(MulticastToNodes::DownloadUpdate {
            node_id: download_id.node_id(),
            request_id: download_id.request_id(),
            notification: update,
        })
    }
}
