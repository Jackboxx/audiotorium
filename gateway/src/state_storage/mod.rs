use std::{collections::HashMap, sync::Arc};

use audiotorium_core::{
    database::select_audio_metadata_from_db,
    identifier::ItemUid,
    schema::audio::{AudioQueueItem, PlaybackState},
};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::{
    audio_playback::audio_item::AudioPlayerQueueItem, db_pool, node::node_server::SourceName,
};

pub mod restore_state_actor;

#[derive(Debug, Clone, Default, Deserialize, Serialize)]
pub struct AppStateRecoveryInfo {
    pub audio_info: HashMap<SourceName, AudioStateInfo>,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct AudioStateInfo {
    pub playback_state: PlaybackState,
    pub current_queue_index: usize,
    pub audio_progress: f64,
    pub audio_volume: f32,
    pub queue: Vec<ItemUid<Arc<str>>>,

    #[serde(skip_serializing, skip_deserializing)]
    pub restored_queue: Vec<AudioPlayerQueueItem>,
}

impl Default for AudioStateInfo {
    fn default() -> Self {
        return Self {
            audio_volume: 1.0,
            playback_state: Default::default(),
            current_queue_index: Default::default(),
            audio_progress: Default::default(),
            queue: Default::default(),
            restored_queue: Default::default(),
        };
    }
}

impl AudioStateInfo {
    async fn restore_queue(&mut self) {
        let mut queue = Vec::with_capacity(self.queue.len());

        for uid in self.queue.iter() {
            match select_audio_metadata_from_db(uid, db_pool()).await {
                Ok(Some(metadata)) => queue.push(AudioPlayerQueueItem {
                    item: AudioQueueItem {
                        metadata,
                        local_id: Uuid::new_v4(),
                    },
                    uid: uid.clone(),
                }),
                Ok(None) => {
                    tracing::warn!(
                        info = "Failed to find audio metadata.",
                        %uid,
                    );
                }
                Err(err) => {
                    tracing::warn!(info = "Failed to select audio metadata.", %uid, ?err);
                }
            };
        }

        self.restored_queue = queue;
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use audiotorium_core::schema::audio::PlaybackState;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_state_serialization() {
        let state = AppStateRecoveryInfo {
            audio_info: HashMap::from([(
                "test".into(),
                AudioStateInfo {
                    playback_state: PlaybackState::Paused,
                    current_queue_index: 3,
                    audio_progress: 0.43,
                    audio_volume: 0.23,
                    queue: vec![ItemUid::new("uid".into())],
                    restored_queue: vec![],
                },
            )]),
        };

        let bin = bincode::serialize(&state).unwrap();
        let decoded: AppStateRecoveryInfo = bincode::deserialize(&bin).unwrap();

        assert_eq!(
            state.audio_info.get("test").unwrap().current_queue_index,
            decoded.audio_info.get("test").unwrap().current_queue_index
        );
        assert_eq!(
            state.audio_info.get("test").unwrap().audio_volume,
            decoded.audio_info.get("test").unwrap().audio_volume
        );
        assert_eq!(
            state.audio_info.get("test").unwrap().audio_progress,
            decoded.audio_info.get("test").unwrap().audio_progress
        );
    }
}
