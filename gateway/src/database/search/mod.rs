mod rank_audio;
mod search_audio;

use audiotorium_core::{
    error::{AppError, AppErrorKind, IntoAppError},
    external_apis::youtube::YoutubeSearchResult,
    identifier::AudioIdentifier,
    schema::audio::{SearchAudioResult, SearchAudioResultKind},
    wrappers::OptionArcStr,
};

pub use rank_audio::RankableData;
pub use search_audio::search_for_local_audio;

pub async fn yt_into_audio_search_result(
    yt_result: YoutubeSearchResult,
    search: &str,
) -> Result<SearchAudioResult, AppError> {
    let (url, kind, snippet) = match yt_result {
        YoutubeSearchResult::Video((url, snippet)) => {
            let kind = SearchAudioResultKind::Track;

            (url.into_inner(), kind, snippet)
        }
        YoutubeSearchResult::Playlist((url, snippet)) => {
            let kind = SearchAudioResultKind::Playlist;
            (url.into_inner(), kind, snippet)
        }
        YoutubeSearchResult::Invalid { reason } => {
            return Err(reason).into_app_err(
                "failed process youtube search result",
                AppErrorKind::Api,
                &[&format!("SEARCH: {search}")],
            )
        }
    };

    let name: OptionArcStr = Some(snippet.title).into();
    let author: OptionArcStr = Some(snippet.channel_title).into();

    let score = RankableData {
        query: search.into(),
        name: name.clone(),
        author: author.clone(),
    }
    .rank()
    .await?;

    Ok(SearchAudioResult {
        score,
        kind,
        identifier: AudioIdentifier::Youtube { raw_url: url },
        name,
        author,
        cover_art_url: snippet
            .thumbnails
            .max_res()
            .map(|thumbnail| return thumbnail.url.clone())
            .into(),
    })
}

fn sanitize_search(search: &str) -> String {
    return search
        .split_whitespace()
        .map(|str| {
            return str
                .chars()
                .filter(|c| return is_valid_search_char(*c))
                .collect::<String>();
        })
        .filter(|str| return !str.is_empty())
        .collect::<Vec<String>>()
        .join(" & ");
}

fn is_valid_search_char(c: char) -> bool {
    return c.is_ascii_lowercase()
        || c.is_ascii_uppercase()
        || c.is_ascii_digit()
        || [
            '?', '-', '_', '+', '#', '~', '/', '%', '$', '@', '[', ']', '{', '}',
        ]
        .contains(&c);
}
