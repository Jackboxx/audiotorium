use std::sync::Arc;

use audiotorium_core::{
    error::{AppError, AppErrorKind, IntoAppError},
    identifier::{AudioIdentifier, ItemUid},
    schema::audio::{SearchAudioResult, SearchAudioResultKind},
    wrappers::OptionArcStr,
};

use crate::db_pool;

use super::{rank_audio::BareSearchRanking, sanitize_search};

#[derive(Debug)]
pub struct BareSearchAudioResult {
    ranking: BareSearchRanking,

    identifier: ItemUid<Arc<str>>,
    name: OptionArcStr,
    author: OptionArcStr,
    cover_art_url: OptionArcStr,
}

#[derive(Debug)]
struct FlatBareSearchAudioResult {
    rank_name: Option<f32>,
    rank_author: Option<f32>,
    similarity: Option<f32>,

    identifier: ItemUid<Arc<str>>,
    name: OptionArcStr,
    author: OptionArcStr,
    cover_art_url: OptionArcStr,
}

impl From<FlatBareSearchAudioResult> for BareSearchAudioResult {
    fn from(value: FlatBareSearchAudioResult) -> Self {
        return Self {
            ranking: BareSearchRanking::new(value.rank_name, value.rank_author, value.similarity),
            identifier: value.identifier,
            name: value.name,
            author: value.author,
            cover_art_url: value.cover_art_url,
        };
    }
}

impl BareSearchAudioResult {
    fn into_result(self, kind: SearchAudioResultKind) -> SearchAudioResult {
        let score = self.ranking.calculate_score();
        return SearchAudioResult {
            score,
            kind,
            identifier: AudioIdentifier::Local {
                uid: self.identifier,
            },
            name: self.name,
            author: self.author,
            cover_art_url: self.cover_art_url,
        };
    }
}
pub async fn search_for_local_audio(
    search: &str,
    limit: usize,
) -> Result<Vec<SearchAudioResult>, AppError> {
    let search = sanitize_search(search);

    let metadata = search_for_audio_metadata(&search)
        .await?
        .into_iter()
        .map(|res| return res.into_result(SearchAudioResultKind::Track));

    let playlists = search_for_audio_playlists(&search)
        .await?
        .into_iter()
        .map(|res| return res.into_result(SearchAudioResultKind::Playlist));

    let mut results: Vec<SearchAudioResult> = metadata.chain(playlists).collect();
    results.sort();

    Ok(results.into_iter().rev().take(limit).collect())
}

async fn search_for_audio_metadata(search: &str) -> Result<Vec<BareSearchAudioResult>, AppError> {
    let results = sqlx::query_as!(
        FlatBareSearchAudioResult,
        "SELECT
                am.identifier,
                am.name,
                am.author,
                am.cover_art_url,
                rank_name,
                rank_author,
                similarity
            FROM 
                audio_metadata am, 
                to_tsvector(am.name || am.author) document,
                to_tsquery($1) query,
                NULLIF(ts_rank(to_tsvector(am.name), query), 0) rank_name,
                NULLIF(ts_rank(to_tsvector(am.author), query), 0) rank_author,
                SIMILARITY($1, am.name || am.author) similarity
            WHERE query @@ document OR similarity > 0
            ORDER BY rank_name, rank_author, similarity DESC NULLS LAST",
        search,
    )
    .fetch_all(db_pool())
    .await
    .into_app_err(
        "failed to search database for audio tracks",
        AppErrorKind::Database,
        &[&format!("SEARCH: {search}")],
    )?;

    Ok(results.into_iter().map(Into::into).collect())
}

async fn search_for_audio_playlists(search: &str) -> Result<Vec<BareSearchAudioResult>, AppError> {
    let results = sqlx::query_as!(
        FlatBareSearchAudioResult,
        "SELECT
                ap.identifier,
                ap.name,
                ap.author,
                ap.cover_art_url,
                rank_name,
                rank_author,
                similarity
            FROM 
                audio_playlist ap,
                to_tsvector(ap.name || ap.author) document,
                to_tsquery($1) query,
                NULLIF(ts_rank(to_tsvector(ap.name), query), 0) rank_name,
                NULLIF(ts_rank(to_tsvector(ap.author), query), 0) rank_author,
                SIMILARITY($1, ap.name || ap.author) similarity
            WHERE query @@ document OR similarity > 0
            ORDER BY rank_name, rank_author, similarity DESC NULLS LAST",
        search,
    )
    .fetch_all(db_pool())
    .await
    .into_app_err(
        "failed to search database for audio tracks",
        AppErrorKind::Database,
        &[&format!("SEARCH: {search}")],
    )?;

    Ok(results.into_iter().map(Into::into).collect())
}
