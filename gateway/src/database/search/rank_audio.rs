use std::sync::Arc;

use audiotorium_core::{
    error::{AppError, AppErrorKind, IntoAppError},
    wrappers::OptionArcStr,
};

use crate::db_pool;

use super::sanitize_search;

pub struct RankableData {
    pub query: Arc<str>,
    pub name: OptionArcStr,
    pub author: OptionArcStr,
}

#[derive(Debug)]
pub(super) struct BareSearchRanking {
    rank_name: Option<f32>,
    rank_author: Option<f32>,
    similarity: Option<f32>,
}

impl RankableData {
    pub async fn rank(&self) -> Result<f32, AppError> {
        Ok(self.search_text().await?.calculate_score())
    }

    async fn search_text(&self) -> Result<BareSearchRanking, AppError> {
        let search = sanitize_search(&self.query);
        let name = sanitize_search(self.name.inner_as_ref().unwrap_or(""));
        let author = sanitize_search(self.author.inner_as_ref().unwrap_or(""));
        let rank = sqlx::query_as!(
            BareSearchRanking,
            "SELECT
                rank_name,
                rank_author,
                similarity
            FROM 
                to_tsvector($2 || $3) document,
                to_tsquery($1) query,
                NULLIF(ts_rank(to_tsvector($2), query), 0) rank_name,
                NULLIF(ts_rank(to_tsvector($3), query), 0) rank_author,
                SIMILARITY($1, $2 || $3) similarity
            WHERE query @@ document OR similarity > 0
            ORDER BY rank_name, rank_author, similarity DESC NULLS LAST",
            search,
            name,
            author,
        )
        .fetch_optional(db_pool())
        .await
        .into_app_err(
            "failed to rank audio data",
            AppErrorKind::Database,
            &[
                &format!("SEARCH: {}", self.query),
                &format!("NAME: {:?}", self.name),
                &format!("AUTHOR: {:?}", self.author),
                &format!("SANITIZED SEARCH: {}", search),
                &format!("SANITIZED NAME: {:?}", name),
                &format!("SANITIZED AUTHOR: {:?}", author),
            ],
        )?;

        return Ok(rank.unwrap_or_else(|| return BareSearchRanking::new(None, None, None)));
    }
}

impl BareSearchRanking {
    const RANK_MULTIPLIER: f32 = 8.0;

    pub(super) fn new(
        rank_name: Option<f32>,
        rank_author: Option<f32>,
        similarity: Option<f32>,
    ) -> Self {
        return Self {
            rank_name,
            rank_author,
            similarity,
        };
    }

    pub(super) fn calculate_score(&self) -> f32 {
        let name_score = (self.rank_name.unwrap_or(0.0) * Self::RANK_MULTIPLIER).clamp(0.0, 1.0);
        let author_score =
            (self.rank_author.unwrap_or(0.0) * Self::RANK_MULTIPLIER).clamp(0.0, 1.0);
        let similarity_score = self.similarity.unwrap_or(0.0);

        return [name_score, author_score, similarity_score]
            .into_iter()
            .sum::<f32>()
            / 3.0;
    }
}
