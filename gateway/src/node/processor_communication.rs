use actix::{Handler, Message};
use audiotorium_core::{
    logging_utils::log_msg_received,
    node::{streams::AudioNodeInfoStreamMessage, AudioNodeHealth},
    schema::audio::{AudioInfoFull, AudioInfoPartial},
};

use crate::{
    audio_playback::audio_player::{AudioPlayerAction, ProcessorInfo},
    gateway::server::AudioNodeToCentralGatewayMessage,
    state_storage::{restore_state_actor::AudioInfoStateUpdateMessage, AudioStateInfo},
};

use super::node_server::AudioNode;

/// Used to communicate between the audio player and the audio node.
#[derive(Debug, Clone, Message, PartialEq)]
#[rtype(result = "()")]
pub enum AudioProcessorToNodeMessage {
    AudioStateInfo(ProcessorInfo),
    Health(AudioNodeHealth),
}

impl Handler<AudioProcessorToNodeMessage> for AudioNode {
    type Result = ();

    fn handle(&mut self, msg: AudioProcessorToNodeMessage, _: &mut Self::Context) -> Self::Result {
        match msg {
            AudioProcessorToNodeMessage::AudioStateInfo(_) => {}
            _ => {
                log_msg_received(&self, &msg);
            }
        }
        match msg {
            AudioProcessorToNodeMessage::Health(health) => {
                self.health = health.clone();

                self.server_addr
                    .do_send(AudioNodeToCentralGatewayMessage::NodeHealthUpdate((
                        self.source_name.to_owned(),
                        health.clone(),
                    )));

                self.multicast(AudioNodeInfoStreamMessage::Health(health));

                match self.health {
                    AudioNodeHealth::Good => {}
                    _ => self.player.r#do(AudioPlayerAction::TryRecoverDevice {
                        progress_before_device_error: self.audio_info.audio_progress,
                        wait_since_last_attempt: None,
                    }),
                };
            }
            AudioProcessorToNodeMessage::AudioStateInfo(processor_info) => {
                self.restore_state_addr
                    .do_send(AudioInfoStateUpdateMessage((
                        self.source_name.clone(),
                        AudioStateInfo {
                            current_queue_index: self.player.queue_head(),
                            audio_volume: processor_info.audio_volume(),
                            audio_progress: processor_info.audio_progress(),
                            playback_state: processor_info.playback_state().clone(),
                            restored_queue: vec![],
                            queue: self.player.queue_items_mapped(|x| {
                                return x.item.metadata.identifier.clone();
                            }),
                        },
                    )));

                self.audio_info = AudioInfoFull {
                    current_queue_index: self.player.queue_head(),
                    audio_volume: processor_info.audio_volume(),
                    audio_progress: processor_info.audio_progress(),
                    playback_state: processor_info.playback_state().clone(),
                };

                let msg = AudioNodeInfoStreamMessage::AudioStateInfo(AudioInfoPartial::from(
                    self.audio_info.clone(),
                ));

                self.multicast(msg);
            }
        }
    }
}
