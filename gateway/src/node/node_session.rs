use std::sync::Arc;

use actix::{
    Actor, ActorContext, ActorFutureExt, Addr, AsyncContext, ContextFutureSpawner, Handler,
    ResponseActFuture, Running, StreamHandler, WrapFuture,
};

use crate::node::node_server::connections::{NodeConnectMessage, NodeDisconnectMessage};
use actix_web_actors::ws;
use audiotorium_core::{
    error::AppError,
    node::streams::{AudioNodeInfoStreamMessage, AudioNodeInfoStreamType},
    schema::audio::AudioInfoPartial,
    stream_utils::HeartBeat,
};

use super::node_server::AudioNode;

pub struct AudioNodeSession {
    id: usize,
    node_addr: Addr<AudioNode>,
    wanted_info: Arc<[AudioNodeInfoStreamType]>,
    last_audio_info: AudioInfoPartial,
}

impl AudioNodeSession {
    pub fn new(node_addr: Addr<AudioNode>, wanted_info: Arc<[AudioNodeInfoStreamType]>) -> Self {
        return Self {
            id: usize::MAX,
            node_addr,
            wanted_info,
            last_audio_info: AudioInfoPartial::default(),
        };
    }
}

impl Actor for AudioNodeSession {
    type Context = ws::WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        tracing::info!("stared new 'NodSession'");

        let addr = ctx.address();
        self.node_addr
            .send(NodeConnectMessage {
                addr,
                wanted_info: Arc::clone(&self.wanted_info),
            })
            .into_actor(self)
            .then(|res, act, ctx| {
                match res {
                    Ok(res) => {
                        tracing::info!("'NodeSession' connected");
                        act.id = res.id;

                        ctx.text(
                            serde_json::to_string(&res.connection_response)
                                .unwrap_or("failed to serialize on server".to_owned()),
                        );

                        ctx.notify(HeartBeat);
                    }

                    Err(err) => {
                        tracing::error!(
                            info = "'NodeSession' failed to connect to 'AudioNode'.",
                            ?err,
                        );
                        ctx.stop();
                    }
                }

                return actix::fut::ready(());
            })
            .wait(ctx);
    }

    fn stopping(&mut self, _: &mut Self::Context) -> Running {
        tracing::info!(info = "'AudioNodeSession' stopping.", self.id);

        self.node_addr
            .do_send(NodeDisconnectMessage { id: self.id });

        return Running::Stop;
    }
}

impl Handler<HeartBeat> for AudioNodeSession {
    type Result = ResponseActFuture<Self, ()>;

    fn handle(&mut self, _msg: HeartBeat, ctx: &mut Self::Context) -> Self::Result {
        ctx.ping(b"heart-beat");
        return Box::pin(
            async {
                actix_rt::time::sleep(std::time::Duration::from_millis(333)).await;
            }
            .into_actor(self)
            .map(|_res, _act, ctx| ctx.notify(HeartBeat)),
        );
    }
}

impl Handler<AudioNodeInfoStreamMessage> for AudioNodeSession {
    type Result = ();

    /// used to receive multicast messages from nodes
    fn handle(&mut self, msg: AudioNodeInfoStreamMessage, ctx: &mut Self::Context) -> Self::Result {
        let msg = match msg {
            AudioNodeInfoStreamMessage::AudioStateInfo(state) => {
                let diff = self.last_audio_info.diff(state.clone());
                self.last_audio_info.update(state);

                AudioNodeInfoStreamMessage::AudioStateInfo(diff)
            }
            _ => msg,
        };

        if self.wanted_info.contains(&msg.msg_type()) {
            ctx.text(
                serde_json::to_string(&msg)
                    .unwrap_or(String::from("failed to serialize on server")),
            )
        }
    }
}

// TODO: is it a good idea to have 10,000 things that error is used for
// - internal msging for multicast to client
// - serialized msg to client (with dbg info removed)
// - automatic logging
// - etc
impl Handler<AppError> for AudioNodeSession {
    type Result = ();

    /// used to receive multicast messages from nodes
    fn handle(&mut self, msg: AppError, ctx: &mut Self::Context) -> Self::Result {
        ctx.text(
            serde_json::to_string(&msg).unwrap_or(String::from("failed to serialize on server")),
        )
    }
}

impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for AudioNodeSession {
    fn handle(&mut self, msg: Result<ws::Message, ws::ProtocolError>, ctx: &mut Self::Context) {
        if let Ok(ws::Message::Close(reason)) = msg {
            ctx.close(reason.clone());
            ctx.stop();
        }
    }
}
