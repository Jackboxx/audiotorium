use std::sync::Arc;

use crate::{
    audio_playback::{audio_item::AudioPlayerQueueItem, audio_player::AudioPlayerAction},
    db_pool,
    env::envs,
    gateway::server::request_audio_from_downloader,
    node::node_server::NodeRequestId,
};

use actix::{ActorFutureExt, Handler, ResponseActFuture, WrapFuture};
use audiotorium_core::{
    database::select_audio_metadata_from_db,
    error::AppError,
    identifier::{AudioIdentifier, Identifier, ItemState, ItemUid},
    logging_utils::log_msg_received,
    node::commands::AudioNodeCommand,
    path::data_shared_audio,
    schema::audio::{AudioQueueItem, AudioTrackMetadata, PlaybackState},
};

use super::AudioNode;

impl Handler<AudioNodeCommand> for AudioNode {
    type Result = ResponseActFuture<Self, ()>;

    fn handle(&mut self, msg: AudioNodeCommand, _ctx: &mut Self::Context) -> Self::Result {
        log_msg_received(&self, &msg);

        enum AsyncActionStep1 {
            None,
            CheckIfAudioItemExists {
                uid: Option<ItemUid<Arc<str>>>,
                identifier: AudioIdentifier,
            },
        }

        enum AsyncActionStep2 {
            None,
            SendDownloadRequest {
                identifier: AudioIdentifier,
            },
            QueryMetadataAndPushToQueue(
                (
                    Result<Option<AudioTrackMetadata>, AppError>,
                    ItemUid<Arc<str>>,
                ),
            ),
        }

        let mut action = AsyncActionStep1::None;

        match msg {
            AudioNodeCommand::AddQueueItem(params) => {
                action = AsyncActionStep1::CheckIfAudioItemExists {
                    uid: params.identifier.try_uid(),
                    identifier: params.identifier,
                };
            }
            AudioNodeCommand::RemoveQueueItem(params) => {
                self.player.r#do(AudioPlayerAction::RemoveFromQueue {
                    identifier: params.local_id,
                });
            }
            AudioNodeCommand::MoveQueueItem(params) => {
                // horrible naming clash :)
                // might be worth changing the public API to fix
                self.player.r#do(AudioPlayerAction::MoveQueueItems {
                    local_id_current: params.current_local_id,
                    idx_current: params.old_pos,
                    local_id_new: params.target_local_id,
                    idx_new: params.new_pos,
                });
            }
            AudioNodeCommand::ShuffleQueue => {
                self.player.r#do(AudioPlayerAction::ShuffleQueue);
            }
            AudioNodeCommand::ClearQueue => {
                self.player.r#do(AudioPlayerAction::ClearQueue);
            }
            AudioNodeCommand::SetAudioVolume(params) => {
                self.player.r#do(AudioPlayerAction::SetVolume {
                    volume: params.volume,
                });
            }
            AudioNodeCommand::SetAudioProgress(params) => {
                self.player.r#do(AudioPlayerAction::SetProgress {
                    progress: params.progress,
                });
            }
            AudioNodeCommand::PauseQueue => {
                self.player.r#do(AudioPlayerAction::SetPlaybackState {
                    state: PlaybackState::Paused,
                });
            }
            AudioNodeCommand::UnPauseQueue => {
                self.player.r#do(AudioPlayerAction::SetPlaybackState {
                    state: PlaybackState::Playing,
                });
            }
            AudioNodeCommand::PlayNext => {
                self.player.r#do(AudioPlayerAction::PlayNext);
            }
            AudioNodeCommand::PlayPrevious => {
                self.player.r#do(AudioPlayerAction::PlayPrev);
            }
            AudioNodeCommand::PlaySelected(params) => {
                self.player.r#do(AudioPlayerAction::PlaySelected {
                    idx: params.index,
                    allow_self_select: false,
                })
            }
        };

        let fut = async move {
            match action {
                AsyncActionStep1::None => {
                    return AsyncActionStep2::None;
                }
                AsyncActionStep1::CheckIfAudioItemExists { uid: None, identifier } => {
                    return AsyncActionStep2::SendDownloadRequest { identifier };
                }
                AsyncActionStep1::CheckIfAudioItemExists { uid: Some(uid), identifier } => {
                    let state = uid.query_state(&data_shared_audio(), db_pool()).await;
                    match state {
                        Ok(ItemState::ExistsEverywhere) => {
                            let res = select_audio_metadata_from_db(&uid, db_pool()).await;
                            return AsyncActionStep2::QueryMetadataAndPushToQueue((res, uid))
                        }
                        Ok(ItemState::DoesNotExist) => {
                            return AsyncActionStep2::SendDownloadRequest { identifier }
                        }
                        Ok(ItemState::ExistsOnlyInDatabase) => {
                            tracing::warn!(info = "Audio item exists in database but no matching audio file was found. Attempting to redownload the audio.", %uid);
                            return AsyncActionStep2::SendDownloadRequest { identifier }
                        }
                        Ok(ItemState::ExistsOnlyOnDisk) => {
                            tracing::warn!(info = "Audio item exists on disk but no matching database entry was found. Attempting to redownload the audio.", %uid);
                            return AsyncActionStep2::SendDownloadRequest { identifier }
                        }
                        Err(err) => {
                            tracing::error!(info = "Failed to get the state of an audio item. Item will not be added to the queue.", %err, %uid);
                            return AsyncActionStep2::None;
                        }
                    }
                }
            }
        }
        .into_actor(self)
        .map(|res, act, _| {
            match res {
                AsyncActionStep2::None => {}
                AsyncActionStep2::SendDownloadRequest { identifier } => {
                    let download_id = NodeRequestId::new(envs().device_id, act.node_id);
                    if let Some(uid) = identifier.try_uid() {
                        act.in_flight_downloads.insert((download_id.request_id(), uid));
                    }

                    request_audio_from_downloader(identifier, Some(download_id));
                }
                AsyncActionStep2::QueryMetadataAndPushToQueue((Ok(Some(metadata)), uid)) => {
                    let item = AudioPlayerQueueItem { item: AudioQueueItem::new(metadata) , uid };
                    act.player.r#do(AudioPlayerAction::PushToQueue { item });
                }
                AsyncActionStep2::QueryMetadataAndPushToQueue((Ok(None), uid)) => {
                    tracing::warn!(info = "Audio file exists locally but doesn't have a matching entry in the database.", %uid)
                }
                AsyncActionStep2::QueryMetadataAndPushToQueue((Err(err), uid)) => {
                    tracing::warn!(info = "Audio file exists locally but there was an error while fetching it's metdata from the database.", %uid, ?err)
                }
            };
        });

        return Box::pin(fut);
    }
}
