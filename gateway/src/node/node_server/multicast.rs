use std::sync::Arc;

use actix::{ActorFutureExt, Handler, ResponseActFuture, WrapFuture};
use audiotorium_core::{
    database::select_audio_metadata_from_db, download::DownloadUpdateNotification,
    identifier::ItemUid, logging_utils::log_msg_received, schema::audio::AudioQueueItem,
};

use crate::{
    audio_playback::{audio_item::AudioPlayerQueueItem, audio_player::AudioPlayerAction},
    db_pool,
    gateway::server::MulticastToNodes,
};

use super::AudioNode;

impl Handler<MulticastToNodes> for AudioNode {
    type Result = ResponseActFuture<Self, ()>;

    fn handle(&mut self, msg: MulticastToNodes, _ctx: &mut Self::Context) -> Self::Result {
        log_msg_received(self, &msg);

        enum AsyncActionStep1 {
            PushToQueue { id: ItemUid<Arc<str>> },
            None,
        }

        enum AsyncActionStep2 {
            PushToQueue { item: AudioPlayerQueueItem },
            None,
        }

        let mut async_action_1 = AsyncActionStep1::None;
        match msg {
            MulticastToNodes::DownloadUpdate {
                node_id,
                request_id,
                notification,
            } if self.node_id == node_id => match notification {
                DownloadUpdateNotification::SingleFinished { id, .. }
                    if self.in_flight_downloads.remove(&(request_id, id.clone())) =>
                {
                    async_action_1 = AsyncActionStep1::PushToQueue { id }
                }
                DownloadUpdateNotification::SingleFinished { id, .. } => {
                    let is_part_of_batch_download =
                        self.in_flight_downloads.iter().any(|in_flight| {
                            return in_flight.0 == request_id;
                        });

                    if is_part_of_batch_download {
                        async_action_1 = AsyncActionStep1::PushToQueue { id }
                    }
                }
                DownloadUpdateNotification::BatchFinished { id, .. } => {
                    self.in_flight_downloads.remove(&(request_id, id));
                }
                _ => {}
            },
            MulticastToNodes::DownloadUpdate { .. } => {}
        }

        let fut = async move {
            match async_action_1 {
                AsyncActionStep1::PushToQueue { id } => {
                    let metadata = match select_audio_metadata_from_db(&id, db_pool()).await {
                        Ok(metadata) => metadata,
                        Err(err) => {
                            tracing::error!(
                                info = "Failed to query database for audio metadata.",
                                ?id,
                                ?err,
                            );

                            return AsyncActionStep2::None;
                        }
                    };

                    let metadata = match metadata {
                        Some(metadata) => metadata,
                        None => {
                            tracing::warn!(
                                info =
                                    "No metadata found for audio that was previously downlaoded.",
                                ?id,
                            );

                            return AsyncActionStep2::None;
                        }
                    };

                    let item = AudioPlayerQueueItem {
                        item: AudioQueueItem {
                            metadata,
                            local_id: uuid::Uuid::new_v4(),
                        },
                        uid: id,
                    };

                    return AsyncActionStep2::PushToQueue { item };
                }
                AsyncActionStep1::None => return AsyncActionStep2::None,
            }
        }
        .into_actor(self)
        .map(
            |async_action_2: AsyncActionStep2, act, _| match async_action_2 {
                AsyncActionStep2::PushToQueue { item } => {
                    act.player.r#do(AudioPlayerAction::PushToQueue { item })
                }
                AsyncActionStep2::None => {}
            },
        );

        return Box::pin(fut);
    }
}
