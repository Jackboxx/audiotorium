use std::{
    collections::{HashMap, HashSet},
    sync::Arc,
};

use actix::{Actor, Addr, AsyncContext, Context, Handler, Message};
use audiotorium_core::{identifier::ItemUid, node::AudioNodeHealth, schema::audio::AudioInfoFull};
use rand::Rng;

use crate::{
    audio_playback::audio_player::{AudioPlayerAction, AudioPlayerInterface},
    gateway::server::CentralGateway,
    state_storage::restore_state_actor::RestoreStateActor,
};

use super::node_session::AudioNodeSession;

pub mod actor;
pub mod connections;
mod multicast;

pub type SourceName = Arc<str>;

#[derive(Debug)]
pub struct AudioNode {
    pub(super) node_id: u32,
    pub(super) source_name: SourceName,
    pub(super) audio_info: AudioInfoFull,
    pub(super) player: AudioPlayerInterface,
    pub(super) restore_state_addr: Addr<RestoreStateActor>,
    pub(super) server_addr: Addr<CentralGateway>,
    pub(super) sessions: HashMap<usize, Addr<AudioNodeSession>>,
    pub(super) in_flight_downloads: HashSet<(u64, ItemUid<Arc<str>>)>,
    pub(super) health: AudioNodeHealth,
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct AudioNodeMulticastMessage<M>
where
    M: Message + Send + Clone + 'static,
    M::Result: Send,
    AudioNodeSession: Handler<M>,
{
    pub msg: M,
}

impl Actor for AudioNode {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        tracing::info!("Stared new 'AudioNode'.");

        self.player.r#do(AudioPlayerAction::SetNodeAddress {
            addr: Some(ctx.address()),
        });
    }
}

impl<M> Handler<AudioNodeMulticastMessage<M>> for AudioNode
where
    M: Message + Send + Clone + 'static,
    M::Result: Send,
    AudioNodeSession: Handler<M>,
{
    type Result = ();

    fn handle(
        &mut self,
        AudioNodeMulticastMessage { msg }: AudioNodeMulticastMessage<M>,
        _: &mut Self::Context,
    ) -> Self::Result {
        self.multicast(msg);
    }
}

impl AudioNode {
    pub fn new(
        source_name: SourceName,
        player: AudioPlayerInterface,
        server_addr: Addr<CentralGateway>,
        restore_state_addr: Addr<RestoreStateActor>,
    ) -> Self {
        return Self {
            node_id: rand::thread_rng().gen(),
            source_name,
            audio_info: AudioInfoFull::default(),
            player,
            restore_state_addr,
            server_addr,
            sessions: HashMap::default(),
            in_flight_downloads: HashSet::default(),
            health: AudioNodeHealth::Good,
        };
    }

    pub fn name_to_id(source_name: &SourceName) -> u32 {
        return fnv_hash(source_name.as_bytes());

        #[inline]
        #[must_use]
        // copied from https://github.com/servo/rust-fnv/
        const fn fnv_hash(bytes: &[u8]) -> u32 {
            const INITIAL_STATE: u64 = 0xcbf2_9ce4_8422_2325;
            const PRIME: u64 = 0x0100_0000_01b3;

            let mut hash = INITIAL_STATE;
            let mut i = 0;
            while i < bytes.len() {
                hash ^= bytes[i] as u64;
                hash = hash.wrapping_mul(PRIME);
                i += 1;
            }

            return (hash & 0x0000_0000_FFFF_FFFF) as u32; // very lazy conversion to u32
        }
    }

    pub(super) fn multicast<M>(&self, msg: M)
    where
        M: Message + Send + Clone + 'static,
        M::Result: Send,
        AudioNodeSession: Handler<M>,
    {
        for addr in self.sessions.values() {
            addr.do_send(msg.clone());
        }
    }
}

/// Identifies what device and what node on that device a request belongs to.
/// 0-31bits = device id
/// 32-63bits = node id
/// remaining = random for request
#[derive(Debug, Clone, Copy)]
pub struct NodeRequestId {
    node_id: u32,
    device_id: u32,
    request_id: u64,
}

impl NodeRequestId {
    pub fn new(device_id: u32, node_id: u32) -> Self {
        let random: u64 = rand::thread_rng().gen_range(0..=u64::MAX);

        return Self {
            request_id: random,
            node_id,
            device_id,
        };
    }

    pub fn as_u128(&self) -> u128 {
        let device_id: u128 = self.device_id.into();
        let node_id: u128 = self.node_id.into();
        let request_id: u128 = self.request_id.into();

        return (device_id << 96) + (node_id << 64) + request_id;
    }

    pub fn node_id(&self) -> u32 {
        return self.node_id;
    }

    pub fn device_id(&self) -> u32 {
        return self.device_id;
    }

    pub fn request_id(&self) -> u64 {
        return self.request_id;
    }

    fn parse_device_id(value: u128) -> u32 {
        let mask = 0xFFFF_FFFF_0000_0000_0000_0000_0000_0000;
        let id = (value & mask) >> 96;
        let id = id.try_into().unwrap_or_else(|err| {
            panic!("Request id should always be between 0 and u32 Received {id}.\n{err}");
        });

        return id;
    }

    fn parse_node_id(value: u128) -> u32 {
        let mask = 0xFFFF_FFFF_0000_0000_0000_0000;
        let id = (value & mask) >> 64;
        let id = id.try_into().unwrap_or_else(|err| {
            panic!("Request id should always be between 0 and u32::MAX. Received {id}.\n{err}");
        });

        return id;
    }

    fn parse_request_id(value: u128) -> u64 {
        let mask = 0xFFFF_FFFF_FFFF_FFFF;
        let id = value & mask;
        let id = id.try_into().unwrap_or_else(|err| {
            panic!("Request id should always be between 0 and u64::MAX. Received {id}.\n{err}");
        });

        return id;
    }
}

impl From<u128> for NodeRequestId {
    fn from(value: u128) -> Self {
        return Self {
            node_id: Self::parse_node_id(value),
            device_id: Self::parse_device_id(value),
            request_id: Self::parse_request_id(value),
        };
    }
}

#[cfg(test)]
mod tests {
    use pretty_assertions::assert_eq;
    use rand::Rng;

    use super::NodeRequestId;

    #[test]
    fn test_node_request_id() {
        let device_id = 1;
        let node_id = 17;

        let req = NodeRequestId::new(device_id, node_id);
        assert_eq!(device_id, req.device_id());
        assert_eq!(node_id, req.node_id());

        for _ in 0..10_000 {
            let device_id: u32 = rand::thread_rng().gen();
            let node_id: u32 = rand::thread_rng().gen();
            let req = NodeRequestId::new(device_id, node_id);

            let req = NodeRequestId::from(req.as_u128());
            assert_eq!(device_id, req.device_id());
            assert_eq!(node_id, req.node_id());
        }
    }
}
