#![deny(clippy::implicit_return)]
#![allow(clippy::needless_return)]

// TODO: Future quality improvements:
// - check that required services are running (downloader, webscraper (optional))

use actix::Actor;
use client_api::rest::{
    get_audio, get_audio_in_audio_playlist, get_audio_playlists, get_single_audio,
    get_single_audio_playlist, receive_gateway_cmd, receive_node_cmd, search_audio_data,
};
use client_api::websockets::{start_gateway_websocket_con, start_node_websocket_con};
use downloader_tcp_msg::DownloaderNotificationHandler;
use env::{envs, init_envs};
use path::setup_paths;
use state_storage::restore_state_actor::RestoreStateActor;

use audiotorium_core::tcp_messaging::TcpMessageGateway;
use std::net::{IpAddr, SocketAddr};
use std::sync::Arc;
use tokio::sync::Mutex;
use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::util::SubscriberInitExt;

use std::sync::OnceLock;

use actix::Addr;
use gateway::server::CentralGateway;
use sqlx::PgPool;

mod audio_playback;
mod client_api;
mod database;
mod downloader_tcp_msg;
mod env;
mod gateway;
mod node;
mod path;
mod state_storage;
mod utils;

#[cfg(test)]
pub mod tests_utils;

pub static POOL: OnceLock<PgPool> = OnceLock::new();
pub static GATEWAY_ADDR: OnceLock<Addr<CentralGateway>> = OnceLock::new();

use actix_cors::Cors;
use actix_web::{App, HttpServer};
use sqlx::postgres::PgPoolOptions;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    tracing_subscriber::registry()
        .with(
            tracing_subscriber::fmt::layer()
                .with_file(true)
                .with_line_number(true),
        )
        .with(tracing_subscriber::EnvFilter::from_default_env())
        .init();

    init_envs();

    setup_paths()?;
    setup_db_con().await;

    tokio::task::spawn(async {
        let mut gateway = setup_tcp_gateway().await;
        gateway.listen().await.unwrap();
    });

    run_http_server().await
}

async fn run_http_server() -> Result<(), std::io::Error> {
    let restore_state_actor = RestoreStateActor::load_or_default().await;
    let restored_state = restore_state_actor.state();
    let restore_state_addr = restore_state_actor.start();

    let queue_server = CentralGateway::new(restore_state_addr, restored_state);
    let gateway_addr = queue_server.start();
    GATEWAY_ADDR.set(gateway_addr).expect("should never fail");

    return HttpServer::new(move || {
        let cors = Cors::default()
            .allow_any_origin()
            .allow_any_method()
            .allow_any_header();

        return App::new()
            .wrap(cors)
            .service(start_gateway_websocket_con)
            .service(start_node_websocket_con)
            .service(receive_gateway_cmd)
            .service(receive_node_cmd)
            .service(get_audio)
            .service(get_single_audio)
            .service(get_audio_playlists)
            .service(get_single_audio_playlist)
            .service(get_audio_in_audio_playlist)
            .service(search_audio_data);
    })
    .bind((envs().gateway_host, envs().gateway_http_port))?
    .run()
    .await;
}

async fn setup_tcp_gateway() -> TcpMessageGateway {
    let downloader_notfication_handler = Arc::new(Mutex::new(DownloaderNotificationHandler));

    let mut gateway = TcpMessageGateway::new(SocketAddr::new(
        IpAddr::V4(envs().gateway_host),
        envs().gateway_tcp_msg_port,
    ));

    gateway.bind(&downloader_notfication_handler).await;

    return gateway;
}

async fn setup_db_con() {
    let db_user = &envs().db_user;
    let db_password = &envs().db_password;
    let db_host = envs().db_host;

    let db_url = format!("postgres://{db_user}:{db_password}@{db_host}");
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&db_url)
        .await
        .expect("should be able to connect to database");

    sqlx::migrate!("./migrations")
        .run(&pool)
        .await
        .expect("all migrations should be valid");

    POOL.set(pool).expect("should never fail");
}

pub fn db_pool<'a>() -> &'a PgPool {
    return POOL.get().expect("Pool should be set at server start.");
}

pub fn gateway_addr<'a>() -> &'a Addr<CentralGateway> {
    return GATEWAY_ADDR
        .get()
        .expect("Gateway address should be set at server start.");
}
