use std::sync::{
    atomic::{AtomicBool, Ordering},
    Arc,
};

use audiotorium_core::{
    identifier::ItemUid,
    node::{commands::AudioNodeCommand, AudioNodeHealth, AudioNodeHealthMild, AudioNodeHealthPoor},
    schema::audio::PlaybackState,
};
use cpal::{
    traits::{DeviceTrait, StreamTrait},
    StreamError,
};
use rtrb::RingBuffer;

use crate::{
    audio_playback::audio_item::load_item,
    message_send_handler::{ChangeDetector, MessageSendHandler, RateLimiter},
    node::AudioProcessorToNodeMessage,
};

use super::{
    processor::{AudioProcessor, AudioProcessorMessage, AudioStreamState},
    AudioPlayer,
};

impl AudioPlayer {
    pub(super) async fn play_next(&mut self) -> anyhow::Result<()> {
        if self.readable_queue().queue.is_empty() {
            self.current_stream = None;
            return Ok(());
        }

        let queue_head_new = self.readable_queue().queue_head + 1;
        self.update_queue_head(queue_head_new);

        if self.readable_queue().queue_head >= self.readable_queue().queue.len() {
            self.update_queue_head(0);
        }

        if let Some(identifier) = self.get_identifier() {
            self.play(&identifier).await?;
        }

        return Ok(());
    }

    pub(super) async fn play_prev(&mut self) -> anyhow::Result<()> {
        if self.readable_queue().queue.is_empty() {
            self.current_stream = None;
            return Ok(());
        }

        let prev_head = self
            .readable_queue()
            .queue_head
            .checked_sub(1)
            .unwrap_or(self.readable_queue().queue.len() - 1);
        self.update_queue_head(prev_head);

        if let Some(identifier) = self.get_identifier() {
            self.play(&identifier).await?;
        }

        return Ok(());
    }

    pub(super) async fn play_selected(
        &mut self,
        index: usize,
        allow_self_select: bool,
    ) -> anyhow::Result<()> {
        if self.readable_queue().queue.is_empty() {
            self.current_stream = None;
            return Ok(());
        }

        if index == self.readable_queue().queue_head && !allow_self_select {
            return Ok(());
        }

        let new_head_pos = index.clamp(0, self.readable_queue().queue.len() - 1);
        self.update_queue_head(new_head_pos);

        if let Some(identifier) = self.get_identifier() {
            self.play(&identifier).await?;
        }

        return Ok(());
    }

    pub(super) fn set_stream_playback_state(&mut self, state: PlaybackState) {
        if let Some(buffer) = self.processor_msg_buffer.as_mut() {
            let _ = buffer.push(AudioProcessorMessage::SetState(state));
        }
    }

    // progress is clamped between `0.0` and `1.0`
    pub(super) fn set_stream_progress(&mut self, progress: f64) {
        let progress = progress.clamp(0.0, 1.0);
        if let Some(buffer) = self.processor_msg_buffer.as_mut() {
            let _ = buffer.push(AudioProcessorMessage::SetProgress(progress));
        }
    }

    pub(super) fn set_volume(&mut self, volume: f32) {
        let volume = volume.clamp(0.0, 1.0);
        self.current_volume = volume;

        if let Some(buffer) = self.processor_msg_buffer.as_mut() {
            let _ = buffer.push(AudioProcessorMessage::SetVolume(volume));
        }
    }

    pub(super) async fn play(&mut self, identifier: &ItemUid<Arc<str>>) -> anyhow::Result<()> {
        let audio_stream = match load_item(identifier).await {
            Ok(x) => x,
            Err(err) => {
                self.err_stopped_playback = true;
                return Err(err.into());
            }
        };

        // prevent bluez-alsa from throwing error 'device busy' by removing the stream accessing
        // the bluetooth device before creating a new stream
        self.current_stream = None;

        let (producer, consumer) = RingBuffer::<AudioProcessorMessage>::new(16);
        self.processor_msg_buffer = Some(producer);

        let mut processor = AudioProcessor::new(
            consumer,
            Some(audio_stream),
            self.node_addr.clone(),
            self.current_volume,
        );

        let mut msg_handler = MessageSendHandler::with_limiters(vec![
            Box::new(ChangeDetector::<AudioProcessorToNodeMessage>::new(Some(
                AudioProcessorToNodeMessage::Health(AudioNodeHealth::Good),
            ))),
            Box::<RateLimiter>::default(),
        ]);

        let mut msg_handler_for_err = MessageSendHandler::with_limiters(vec![
            Box::new(ChangeDetector::<AudioProcessorToNodeMessage>::new(Some(
                AudioProcessorToNodeMessage::Health(AudioNodeHealth::Good),
            ))),
            Box::<RateLimiter>::default(),
        ]);

        let addr_for_err = self.node_addr.clone();

        let fatal_err_in_stream = Arc::new(AtomicBool::new(false));
        let fatal_err_in_stream_clone = Arc::clone(&fatal_err_in_stream);

        let new_stream_res = self.device.build_output_stream(
            &self.config,
            move |data: &mut [f32], _| {
                if fatal_err_in_stream.load(Ordering::Relaxed) {
                    return;
                };

                match processor.try_process(data) {
                    Ok(state) => match state {
                        AudioStreamState::Finished => {
                            processor.read_disk_stream = None;

                            if let Some(addr) = processor.node_addr.as_ref() {
                                if let Err(err) = addr.try_send(AudioNodeCommand::PlayNext) {
                                    tracing::error!(
                                        info = "Failed to play next audio in queue.",
                                        ?err
                                    );
                                }
                            }
                        }
                        AudioStreamState::Buffering => {
                            let msg = AudioProcessorToNodeMessage::Health(AudioNodeHealth::Mild(
                                AudioNodeHealthMild::Buffering,
                            ));

                            if let Some(addr) = processor.node_addr.as_ref() {
                                msg_handler.send_msg(msg, addr);
                            }
                        }
                        AudioStreamState::Playing => {
                            let msg =
                                AudioProcessorToNodeMessage::AudioStateInfo(processor.info.clone());

                            if let Some(addr) = processor.node_addr.as_ref() {
                                msg_handler.send_msg(msg, addr);
                            }
                        }
                    },
                    Err(err) => {
                        tracing::error!(info = "Failed to process audio stream.", ?err);

                        let msg = AudioProcessorToNodeMessage::Health(AudioNodeHealth::Poor(
                            AudioNodeHealthPoor::AudioStreamReadFailed,
                        ));

                        if let Some(addr) = processor.node_addr.as_ref() {
                            msg_handler.send_msg(msg, addr);
                        }
                    }
                };
            },
            move |err| {
                if fatal_err_in_stream_clone.load(Ordering::Relaxed) {
                    return;
                }

                tracing::error!(info = "Failed to process audio stream.", ?err);
                let msg = match err {
                    StreamError::DeviceNotAvailable => AudioProcessorToNodeMessage::Health(
                        AudioNodeHealth::Poor(AudioNodeHealthPoor::DeviceNotAvailable),
                    ),

                    StreamError::BackendSpecific { err } => {
                        AudioProcessorToNodeMessage::Health(AudioNodeHealth::Poor(
                            AudioNodeHealthPoor::AudioBackendError(err.description),
                        ))
                    }
                };

                if let Some(addr) = addr_for_err.as_ref() {
                    msg_handler_for_err.send_msg(msg, addr);
                }

                fatal_err_in_stream_clone.store(true, Ordering::SeqCst);
            },
            None,
        );

        let new_stream = match new_stream_res {
            Ok(x) => x,
            Err(err) => {
                self.err_stopped_playback = true;
                return Err(err.into());
            }
        };

        match new_stream.play() {
            Ok(()) => {}
            Err(err) => {
                self.err_stopped_playback = true;
                return Err(err.into());
            }
        };

        self.err_stopped_playback = false;
        self.current_stream = Some(new_stream);
        return Ok(());
    }
}
