use std::{sync::Arc, time::Duration};

use actix::Addr;
use anyhow::Context;
use audiotorium_core::{
    identifier::ItemUid,
    logging_utils::log_msg_received,
    node::{streams::AudioNodeInfoStreamMessage, AudioNodeHealth},
    schema::audio::{AudioInfoFull, AudioInfoPartial, AudioQueueItem, PlaybackState},
    wrappers::ArcSlice,
};
use cpal::{Device, Stream, StreamConfig};
use rtrb::Producer;

use crate::{
    node::{
        node_server::{AudioNode, AudioNodeMulticastMessage, SourceName},
        RecoverDeviceResult,
    },
    utils::setup_device,
};

use self::processor::AudioProcessorMessage;
use super::audio_item::AudioPlayerQueueItem;

pub use self::processor::ProcessorInfo;

mod playback;
mod processor;
mod queue;

pub struct AudioPlayer {
    source_name: SourceName,
    device: Device,
    config: StreamConfig,
    queue: Arc<std::sync::RwLock<QueueState>>,
    current_stream: Option<Stream>,
    node_addr: Option<Addr<AudioNode>>,
    player_rx: tokio::sync::mpsc::Receiver<AudioPlayerAction>,
    processor_msg_buffer: Option<Producer<AudioProcessorMessage>>,
    current_volume: f32,
    err_stopped_playback: bool,
}

#[derive(Debug)]
pub struct AudioPlayerInterface {
    player_tx: tokio::sync::mpsc::Sender<AudioPlayerAction>,
    queue: Arc<std::sync::RwLock<QueueState>>,
}

#[derive(Debug)]
pub enum AudioPlayerAction {
    PlayNext,
    PlayPrev,
    PlaySelected {
        idx: usize,
        allow_self_select: bool,
    },
    SetPlaybackState {
        state: PlaybackState,
    },
    SetProgress {
        progress: f64,
    },
    SetVolume {
        volume: f32,
    },
    PushToQueue {
        item: AudioPlayerQueueItem,
    },
    RemoveFromQueue {
        identifier: uuid::Uuid,
    },
    MoveQueueItems {
        local_id_current: uuid::Uuid,
        idx_current: usize,
        local_id_new: uuid::Uuid,
        idx_new: usize,
    },
    ShuffleQueue,
    ClearQueue,
    SetNodeAddress {
        addr: Option<Addr<AudioNode>>,
    },
    TryRecoverDevice {
        progress_before_device_error: f64,
        wait_since_last_attempt: Option<Duration>,
    },
}

#[derive(Debug)]
struct QueueState {
    queue: Vec<AudioPlayerQueueItem>,
    queue_head: usize,
}

impl AudioPlayerInterface {
    pub fn r#do(&self, action: AudioPlayerAction) {
        log_msg_received(self, &action);

        let res = self.player_tx.try_send(action);
        if let Err(err) = res {
            tracing::error!(info = "Failed to send audio player action.", ?err);
        };
    }

    pub fn queue_items_mapped<T>(&self, map_fn: fn(&AudioPlayerQueueItem) -> T) -> Vec<T> {
        return self.readable_queue().queue.iter().map(map_fn).collect();
    }

    pub fn queue_head(&self) -> usize {
        return self.readable_queue().queue_head;
    }

    fn readable_queue(&self) -> std::sync::RwLockReadGuard<'_, QueueState> {
        return self
            .queue
            .read()
            .expect("Audio player has paniced while holding rw lock.");
    }
}

impl AudioPlayer {
    pub fn run(mut self, restored_state: AudioInfoFull) {
        let _handle = tokio::task::spawn_local(async move {
            self.restore_state(restored_state).await;

            while let Some(action) = self.player_rx.recv().await {
                let msg_for_clients_res = match action {
                    AudioPlayerAction::PlayNext => self.play_next().await.with_context(|| {
                        return "Failed to play next audio.";
                    }).map(|()| {
                        return Some(
                AudioNodeInfoStreamMessage::Queue(ArcSlice::from_iter(
                    self.queue_items_cloned(),
                )));
                    }),
                    AudioPlayerAction::PlayPrev => self.play_prev().await.with_context(|| {
                        return "Failed to play previous audio.";
                    }).map(|()| {
                        return Some(AudioNodeInfoStreamMessage::AudioStateInfo(AudioInfoPartial::with_queue_head(self.queue_head())))
                    }),
                    AudioPlayerAction::PlaySelected {
                        idx,
                        allow_self_select,
                    } => self
                        .play_selected(idx, allow_self_select)
                        .await
                        .with_context(|| {
                            return format!("Failed to play selected audio. [idx={idx}]");
                        }).map(|()| {
                        return Some(AudioNodeInfoStreamMessage::AudioStateInfo(AudioInfoPartial::with_queue_head(self.queue_head())));
                        }),
                    AudioPlayerAction::SetPlaybackState { state } => {
                        self.set_stream_playback_state(state.clone());
                        Ok(Some(AudioNodeInfoStreamMessage::AudioStateInfo(AudioInfoPartial::with_playback_state(state))))
                    }
                    AudioPlayerAction::SetProgress { progress } => {
                        self.set_stream_progress(progress);
                        Ok(Some(AudioNodeInfoStreamMessage::AudioStateInfo(AudioInfoPartial::with_progress(progress))))
                    }
                    AudioPlayerAction::SetVolume { volume } => {
                        self.set_volume(volume);
                        Ok(Some(AudioNodeInfoStreamMessage::AudioStateInfo(AudioInfoPartial::with_volume(volume))))
                    }
                    AudioPlayerAction::PushToQueue { item } => {
                        self.push_to_queue(item).await.with_context(|| {
                            return "Failed to push new item to the queue.";
                        }).map(|()| {
                            return Some(AudioNodeInfoStreamMessage::Queue(ArcSlice::from_iter(
                    self.queue_items_cloned(),
                )))
                        })
                    }
                    AudioPlayerAction::RemoveFromQueue { identifier } => {
                        self.remove_from_queue(&identifier).await.with_context(|| {
                            return format!(
                                "Failed to remove item from queue. [identifier={identifier}]"
                            );
                        }).map(|()| {
                        return Some(
                AudioNodeInfoStreamMessage::Queue(ArcSlice::from_iter(
                    self.queue_items_cloned(),
                )));
                        })
                    }
                    AudioPlayerAction::MoveQueueItems {
                        local_id_current,
                        idx_current,
                        local_id_new,
                        idx_new,
                    } => {
                        self.move_queue_item(&local_id_current, idx_current, &local_id_new, idx_new).with_context(|| {
                            return format!("Failed to swap queue item at position {local_id_current} with item at position {local_id_new}. [current_id={local_id_current}, target_id={local_id_new}]")
                        }).map(|()| {
                        return Some(
                AudioNodeInfoStreamMessage::Queue(ArcSlice::from_iter(
                    self.queue_items_cloned(),
                )));
                        })
                    }
                    AudioPlayerAction::ShuffleQueue => self.shuffle_queue().await.with_context(|| {
                        return "Failed to shuffle queue."
                    }).map(|()| {
                        return Some(
                AudioNodeInfoStreamMessage::Queue(ArcSlice::from_iter(
                    self.queue_items_cloned(),
                )));
                        }),
                    AudioPlayerAction::ClearQueue => self.clear_queue().await.with_context(|| {
                        return "Failed to clear queue."
                    }).map(|()| {
                        return Some(
                AudioNodeInfoStreamMessage::Queue(ArcSlice::from_iter([])));
                        }),
                    AudioPlayerAction::SetNodeAddress { addr } => {
                        self.set_addr(addr);
                        Ok(None)
                    }
                    AudioPlayerAction::TryRecoverDevice { progress_before_device_error, wait_since_last_attempt } => {
                        self.try_recover_device(progress_before_device_error, wait_since_last_attempt).await.with_context(|| {
                            return "Failed to recover device."
                        }).map(|()| {
                            return Some(AudioNodeInfoStreamMessage::Health(AudioNodeHealth::Good));
                        })
                    }
                };

                match msg_for_clients_res {
                    Ok(Some(msg)) => {
                        if let Some(addr) = &self.node_addr {
                            addr.do_send(AudioNodeMulticastMessage { msg })
                        }
                    }
                    Ok(None) => {}
                    Err(err) => {
                        // TODO: send error to clients
                        tracing::error!(info = "Failed to perform audio player action.", ?err);
                    }
                }
            }
        });
    }

    pub fn try_new(
        source_name: SourceName,
        node_addr: Option<Addr<AudioNode>>,
        restored_state: AudioInfoFull,
        restored_queue: Vec<AudioPlayerQueueItem>,
    ) -> anyhow::Result<AudioPlayerInterface> {
        let (device, config) = setup_device(&source_name)?;
        let (player_tx, player_rx) = tokio::sync::mpsc::channel(256);

        let queue = Arc::new(std::sync::RwLock::new(QueueState {
            queue: restored_queue,
            queue_head: restored_state.current_queue_index,
        }));

        let interface = AudioPlayerInterface {
            player_tx,
            queue: queue.clone(),
        };
        let player = Self {
            source_name,
            queue,
            device,
            config,
            current_stream: None,
            player_rx,
            processor_msg_buffer: None,
            node_addr,
            current_volume: restored_state.audio_volume,
            err_stopped_playback: false,
        };

        player.run(restored_state);

        return Ok(interface);
    }

    async fn try_recover_device(
        &mut self,
        current_progress: f64,
        wait_since_last_attempt: Option<Duration>,
    ) -> anyhow::Result<()> {
        let (device, config) = match setup_device(&self.source_name) {
            Ok(recovered) => {
                if let Some(addr) = &self.node_addr {
                    addr.do_send(RecoverDeviceResult::new(true, wait_since_last_attempt));
                }

                recovered
            }
            Err(err) => {
                if let Some(addr) = &self.node_addr {
                    addr.do_send(RecoverDeviceResult::new(false, wait_since_last_attempt));
                }

                return Err(err);
            }
        };
        self.device = device;
        self.config = config;

        let queue_head = self
            .queue
            .read()
            .expect("Audio player has paniced while holding rw lock.")
            .queue_head;

        self.play_selected(queue_head, true).await?;
        self.set_stream_progress(current_progress);

        return Ok(());
    }

    fn set_addr(&mut self, node_addr: Option<Addr<AudioNode>>) {
        self.node_addr.clone_from(&node_addr);

        if let Some(buffer) = self.processor_msg_buffer.as_mut() {
            let _ = buffer.push(AudioProcessorMessage::Addr(node_addr));
        }
    }

    fn get_identifier(&self) -> Option<ItemUid<Arc<str>>> {
        let lock = self
            .queue
            .read()
            .expect("Audio player has paniced while holding rw lock.");

        return lock
            .queue
            .get(lock.queue_head)
            .map(|audio| return audio.uid.clone());
    }

    pub fn queue_items_cloned(&self) -> Vec<AudioQueueItem> {
        return self
            .readable_queue()
            .queue
            .iter()
            .map(|x| {
                return x.item.clone();
            })
            .collect();
    }

    pub fn queue_head(&self) -> usize {
        return self.readable_queue().queue_head;
    }

    fn readable_queue(&self) -> std::sync::RwLockReadGuard<'_, QueueState> {
        return self
            .queue
            .read()
            .expect("Audio player has paniced while holding rw lock.");
    }

    fn writable_queue(&self) -> std::sync::RwLockWriteGuard<'_, QueueState> {
        return self
            .queue
            .write()
            .expect("Audio player has paniced while holding rw lock.");
    }

    fn update_queue_head(&mut self, value: usize) {
        let mut lock = self
            .queue
            .write()
            .expect("Audio player has paniced while holding rw lock.");

        lock.queue_head = value;
    }

    async fn restore_state(&mut self, info: AudioInfoFull) {
        self.update_queue_head(info.current_queue_index);

        if let Some(identifier) = self.get_identifier() {
            if let Err(err) = self.play(&identifier).await {
                tracing::error!(info = "Failed to play audio after restore.", ?err);
            }

            self.set_volume(info.audio_volume);
            self.set_stream_progress(info.audio_progress);
            self.set_stream_playback_state(info.playback_state);
        }
    }
}

impl std::fmt::Debug for AudioPlayer {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        return write!(f, "TODO");
    }
}
