use std::cmp::Ordering;

use anyhow::anyhow;
use rand::{seq::SliceRandom, thread_rng};
use uuid::Uuid;

use crate::audio_playback::audio_item::{AudioPlayerQueueItem, IdentifiableQueueItem};

use super::AudioPlayer;

#[derive(Debug, PartialEq, Eq)]
enum AfterRemoveQueueItemAction {
    Noop {
        idx_to_remove: usize,
    },
    PlayNothing {
        idx_to_remove: usize,
    },
    UpdateHead {
        idx_to_remove: usize,
        new_head: usize,
    },
    UpdateHeadAndPlayNew {
        idx_to_remove: usize,
        new_head: usize,
    },
}

impl AudioPlayer {
    /// Adds a song to the queue. If this is the first song to be added to the queue, it starts playing immediately.
    pub(super) async fn push_to_queue(&mut self, item: AudioPlayerQueueItem) -> anyhow::Result<()> {
        if self.readable_queue().queue.is_empty() {
            self.play(&item.uid).await?;
        }

        if self.err_stopped_playback {
            self.play(&item.uid).await?;

            let queue_head = self.readable_queue().queue.len();
            let mut lock = self.writable_queue();

            lock.queue.push(item);
            lock.queue_head = queue_head;
        } else {
            self.writable_queue().queue.push(item);
        }

        return Ok(());
    }

    pub(super) async fn clear_queue(&mut self) -> anyhow::Result<()> {
        self.writable_queue().queue.clear();
        return self.play_next().await;
    }

    pub(super) async fn remove_from_queue(&mut self, identifier: &Uuid) -> anyhow::Result<()> {
        let action = {
            let queue_head = self.readable_queue().queue_head;
            let read_lock = self.readable_queue();
            remove_from_queue_noop(&read_lock.queue, queue_head, identifier)
        }?;

        match action {
            AfterRemoveQueueItemAction::PlayNothing { idx_to_remove } => {
                self.writable_queue().queue.remove(idx_to_remove);
                return self.play_next().await; // plays empty
            }
            AfterRemoveQueueItemAction::UpdateHeadAndPlayNew {
                idx_to_remove,
                new_head,
            } => {
                self.writable_queue().queue.remove(idx_to_remove);

                self.update_queue_head(new_head);
                return self.play_selected(new_head, true).await;
            }
            AfterRemoveQueueItemAction::UpdateHead {
                idx_to_remove,
                new_head,
            } => {
                self.writable_queue().queue.remove(idx_to_remove);

                self.update_queue_head(new_head);
                return Ok(());
            }
            AfterRemoveQueueItemAction::Noop { idx_to_remove } => {
                self.writable_queue().queue.remove(idx_to_remove);
                return Ok(());
            }
        }
    }

    pub(super) async fn shuffle_queue(&mut self) -> anyhow::Result<()> {
        if self.readable_queue().queue.len() < 3 {
            return Ok(());
        }

        self.writable_queue().queue.shuffle(&mut thread_rng());
        self.update_queue_head(0);
        return self.play_selected(0, true).await;
    }

    pub(super) fn move_queue_item(
        &mut self,
        current_local_id: &Uuid,
        old: usize,
        target_local_id: &Uuid,
        new: usize,
    ) -> anyhow::Result<()> {
        let mut queue_head = self.readable_queue().queue_head;
        let queue = &mut self.writable_queue().queue;

        move_queue_items(
            queue,
            &mut queue_head,
            old,
            new,
            current_local_id,
            target_local_id,
        )?;

        self.writable_queue().queue_head = queue_head;
        return Ok(());
    }
}

fn remove_from_queue_noop<T: IdentifiableQueueItem>(
    queue: &[T],
    queue_head: usize,
    uuid: &Uuid,
) -> Result<AfterRemoveQueueItemAction, anyhow::Error> {
    let Some(idx) = queue
        .iter()
        .position(|item| return item.identifier().eq(uuid))
    else {
        return Err(anyhow!("no element with identifier {uuid:?} found"));
    };

    if queue.len() == 1 {
        return Ok(AfterRemoveQueueItemAction::PlayNothing { idx_to_remove: idx });
    }

    match idx.cmp(&queue_head) {
        Ordering::Equal => {
            if queue_head == queue.len() - 1 {
                return Ok(AfterRemoveQueueItemAction::UpdateHeadAndPlayNew {
                    idx_to_remove: idx,
                    new_head: queue_head - 1,
                });
            } else {
                return Ok(AfterRemoveQueueItemAction::UpdateHeadAndPlayNew {
                    idx_to_remove: idx,
                    new_head: queue_head,
                });
            }
        }

        Ordering::Less => {
            return Ok(AfterRemoveQueueItemAction::UpdateHead {
                idx_to_remove: idx,
                new_head: queue_head - 1,
            })
        }
        Ordering::Greater => return Ok(AfterRemoveQueueItemAction::Noop { idx_to_remove: idx }),
    }
}

/// Shifts elements between 'old'..='new' over to move 'old' to the position of 'new'.
///
/// ### Example
///
/// Move element at index 1 to index 4.
///
/// ```ignore
/// queue = [0, 1, 2, 3, 4, 5];
///             ^ ->  -> ^
///             |        |
///
/// queue = [0, 1,      2, 3, 4, 5]; // swap 1 and 2
///             ^  <->  ^
///             |       |
///
/// queue = [0, 2, 1, 3, 4, 5]; // results in
///
///
/// queue = [0, 2, 3, 1, 4, 5]; // keep swapping elements
///
/// queue = [0, 2, 3, 4, 1, 5]; // until index 4 is reached
/// ````
///
fn move_queue_items<T: IdentifiableQueueItem>(
    queue: &mut [T],
    queue_head: &mut usize,
    old: usize,
    new: usize,
    current_local_id: &Uuid,
    target_local_id: &Uuid,
) -> anyhow::Result<()> {
    if old == new {
        return Ok(());
    }

    let current_exists = queue
        .get(old)
        .map(|item| return item.identifier().eq(current_local_id))
        .unwrap_or(false);

    if !current_exists {
        return Err(anyhow!(
            "trying to move current element with identifier {current_local_id:?} that doesn't exist"
        ));
    }

    let target_exists = queue
        .get(new)
        .map(|item| return item.identifier().eq(target_local_id))
        .unwrap_or(false);

    if !target_exists {
        return Err(anyhow!("trying to move to target element with identifier {target_local_id:?} which doesn't exist"));
    }

    if old > new {
        for i in (new + 1..=old).rev() {
            if *queue_head == i - 1 {
                *queue_head = i;
            } else if *queue_head == i {
                *queue_head = i - 1;
            }

            queue.swap(i - 1, i);
        }
    } else {
        for i in old..new {
            if *queue_head == i {
                *queue_head = i + 1;
            } else if *queue_head == i + 1 {
                *queue_head = i;
            }

            queue.swap(i, i + 1);
        }
    }

    return Ok(());
}

#[cfg(test)]
mod tests {
    use std::str::FromStr;

    use crate::audio_playback::{
        audio_item::IdentifiableQueueItem,
        audio_player::queue::{
            move_queue_items, remove_from_queue_noop, AfterRemoveQueueItemAction,
        },
    };

    use pretty_assertions::assert_eq;
    use uuid::Uuid;

    impl IdentifiableQueueItem for &str {
        fn identifier(&self) -> Uuid {
            return Uuid::from_str(&format!("497e1d51-d254-4327-bdd6-c7583e09bd6{self}")).unwrap();
        }
    }

    #[test]
    fn test_remove_from_queue() {
        struct TestCase<'a> {
            description: &'a str,

            in_queue: Vec<&'a str>,
            in_head: usize,
            in_ident: &'a str,

            out_action: AfterRemoveQueueItemAction,
            should_error: bool,
        }

        let mut cases = vec![
            TestCase {
                description: "remove element after current head",
                in_queue: vec!["0", "1", "2", "3", "4"],
                in_head: 3,
                in_ident: "4",
                out_action: AfterRemoveQueueItemAction::Noop { idx_to_remove: 4 },
                should_error: false,
            },
            TestCase {
                description: "remove element before current head",
                in_queue: vec!["0", "1", "2", "3", "4"],
                in_head: 4,
                in_ident: "2",
                out_action: AfterRemoveQueueItemAction::UpdateHead {
                    idx_to_remove: 2,
                    new_head: 3,
                },
                should_error: false,
            },
            TestCase {
                description: "remove element at current head and at end of list",
                in_queue: vec!["0", "1", "2", "3", "4"],
                in_head: 4,
                in_ident: "4",
                out_action: AfterRemoveQueueItemAction::UpdateHeadAndPlayNew {
                    idx_to_remove: 4,
                    new_head: 3,
                },
                should_error: false,
            },
            TestCase {
                description: "remove element before current head and at start of list",
                in_queue: vec!["0", "1", "2", "3", "4"],
                in_head: 4,
                in_ident: "0",
                out_action: AfterRemoveQueueItemAction::UpdateHead {
                    idx_to_remove: 0,
                    new_head: 3,
                },
                should_error: false,
            },
            TestCase {
                description: "no element found",
                in_queue: vec!["0", "1", "2", "3", "4"],
                in_head: 4,
                in_ident: "5",
                out_action: AfterRemoveQueueItemAction::UpdateHead {
                    idx_to_remove: 0,
                    new_head: 3,
                },
                should_error: true,
            },
        ];

        for case in cases.iter_mut() {
            let out =
                remove_from_queue_noop(&case.in_queue, case.in_head, &case.in_ident.identifier());

            dbg!(case.description);
            if case.should_error {
                out.unwrap_err();
            } else {
                let out_action = out.unwrap();
                assert_eq!(out_action, case.out_action);
            }
        }
    }

    #[test]
    fn test_move_queue_items() {
        struct TestCase<'a> {
            description: &'a str,

            in_queue: Vec<&'a str>,
            in_head: usize,
            in_params: (usize, usize, &'a str, &'a str),

            out_queue: Vec<&'a str>,
            out_head: usize,

            should_error: bool,
        }

        let mut cases = vec![
            TestCase {
                description: "basic swap",
                in_queue: vec!["0", "1", "2", "3"],
                in_head: 3,
                in_params: (0, 1, "0", "1"),
                out_queue: vec!["1", "0", "2", "3"],
                out_head: 3,
                should_error: false,
            },
            TestCase {
                description: "basic swap with head change",
                in_queue: vec!["0", "1", "2", "3"],
                in_head: 1,
                in_params: (0, 1, "0", "1"),
                out_queue: vec!["1", "0", "2", "3"],
                out_head: 0,
                should_error: false,
            },
            TestCase {
                description: "multi element swap",
                in_queue: vec!["0", "1", "2", "3", "4"],
                in_head: 0,
                in_params: (1, 4, "1", "4"),
                out_queue: vec!["0", "2", "3", "4", "1"],
                out_head: 0,
                should_error: false,
            },
            TestCase {
                description: "multi element swap with head change",
                in_queue: vec!["0", "1", "2", "3", "4"],
                in_head: 1,
                in_params: (1, 4, "1", "4"),
                out_queue: vec!["0", "2", "3", "4", "1"],
                out_head: 4,
                should_error: false,
            },
            TestCase {
                description: "multi element swap backwards",
                in_queue: vec!["0", "1", "2", "3", "4"],
                in_head: 0,
                in_params: (3, 0, "3", "0"),
                out_queue: vec!["3", "0", "1", "2", "4"],
                out_head: 1,
                should_error: false,
            },
            TestCase {
                description: "multi element swap with head change #2",
                in_queue: vec!["0", "1", "2", "3", "4", "5"],
                in_head: 2,
                in_params: (1, 4, "1", "4"),
                out_queue: vec!["0", "2", "3", "4", "1", "5"],
                out_head: 1,
                should_error: false,
            },
            TestCase {
                description: "index out of bounds",
                in_queue: vec!["0", "1", "2", "3"],
                in_head: 2,
                in_params: (1, 5, "1", "5"),
                out_queue: vec!["0", "1", "2", "3"],
                out_head: 2,
                should_error: true,
            },
            TestCase {
                description: "head out of bounds",
                in_queue: vec!["0", "1", "2", "3"],
                in_head: 4,
                in_params: (1, 0, "1", "0"),
                out_queue: vec!["1", "0", "2", "3"],
                out_head: 4,
                should_error: false,
            },
            TestCase {
                description: "identifier mismatch",
                in_queue: vec!["0", "1", "2", "3"],
                in_head: 3,
                in_params: (0, 1, "0", "2"),
                out_queue: vec!["0", "1", "2", "3"],
                out_head: 3,
                should_error: true,
            },
        ];

        for case in cases.iter_mut() {
            let (old_idx, new_idx, current_ident, target_ident) = case.in_params;
            let out = move_queue_items(
                &mut case.in_queue,
                &mut case.in_head,
                old_idx,
                new_idx,
                &current_ident.identifier(),
                &target_ident.identifier(),
            );

            dbg!(case.description);
            if case.should_error {
                out.unwrap_err();
                assert_eq!(case.in_queue, case.out_queue);
                assert_eq!(case.in_head, case.out_head);
            } else {
                out.unwrap();
                assert_eq!(case.in_queue, case.out_queue);
                assert_eq!(case.in_head, case.out_head);
            }
        }
    }
}
