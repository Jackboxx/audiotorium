#[allow(clippy::module_name_repetitions)]
#[cfg(feature = "actor-tcp")]
pub struct MessageTags;

#[cfg(feature = "actor-tcp")]
impl MessageTags {
    pub const DOWNLOADER_DOWNLOAD_AUDIO_REQUEST: u32 = 0x0100;
    pub const DOWNLOADER_NOTIFY_DOWNLOAD_UPDATE: u32 = 0x0101;
    pub const DOWNLOADER_PLAYBACK_ACTION: u32 = 0x0200;
}

#[cfg(feature = "events")]
use crate::events::EventQueueUniqueId;

#[cfg(feature = "events")]
pub struct EventQueueIds;

#[cfg(feature = "events")]
impl EventQueueIds {
    pub const DOWNLOADER_DOWNLOAD_QUEUE: EventQueueUniqueId = 0x1100;
}
