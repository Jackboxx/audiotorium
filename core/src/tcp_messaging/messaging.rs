use std::{collections::HashMap, net::SocketAddr, sync::Arc};

use async_trait::async_trait;
use serde::{de::DeserializeOwned, Serialize};
use thiserror::Error;
use tokio::{
    io::AsyncWriteExt,
    net::{TcpListener, TcpStream},
    sync::{
        mpsc::{channel, error::SendError, Sender},
        Mutex,
    },
    task::JoinHandle,
};
use tracing::{debug, error};

use crate::tcp_messaging::binary::{
    build_msg_response, parse_msg_incoming, read_stream_until_msg_end,
};

use self::internal::spawn;

pub type MessageTag = u32;
type HandlerChannelData = (RawTcpMessage, TcpStream);

pub const MESSAGE_VERSION: u32 = 1;
pub const MESSAGE_END: [u8; 4] = [255, 255, 255, 255];
pub const MESSAGE_MAX_SIZE: usize = 0xffff; // 16K bits -> 2 KB

pub trait TcpMessage {
    type Response;

    const TAG: MessageTag;
}

#[derive(Debug)]
pub enum MessageResponseType {
    Ok,
    Error,
    Invalid,
}

#[derive(Debug, Error)]
pub enum TcpMessageGatewayError {
    #[error("No handler is registered to process the tag {0:?}")]
    NoHandlerForTag(MessageTag),
    #[error("The handler is not receiving messages anymore. {0}")]
    HandlerNotReceiving(#[from] SendError<HandlerChannelData>),
}

// Only the body of a TCP message without header fields, tags, ...
#[derive(Debug)]
pub struct RawTcpMessage(Vec<u8>);

#[derive(Debug)]
pub struct TcpMessageGateway {
    addr: SocketAddr,
    handlers: Arc<Mutex<HashMap<MessageTag, Sender<HandlerChannelData>>>>,
}

impl TcpMessageGateway {
    #[must_use]
    pub fn new(addr: SocketAddr) -> Self {
        return Self {
            addr,
            handlers: Arc::default(),
        };
    }

    /// Binds a handler 'handle' function for a specific message tag. Spawns a new green thread
    /// to listen for incoming TCP messages matching the messages tag.
    ///
    /// ### Returns
    ///
    /// The send handle for the previously registered handle function or 'None'.
    pub async fn bind<Msg, Handler>(
        &mut self,
        handler: &Arc<Mutex<Handler>>,
    ) -> Option<Sender<HandlerChannelData>>
    where
        Msg: TcpMessage + DeserializeOwned,
        <Msg as TcpMessage>::Response: Serialize,
        Handler: TcpMessageHandler<Msg>,
    {
        let (tx, _join_handle) = spawn(handler);
        return self.handlers.lock().await.insert(Msg::TAG, tx);
    }

    #[tracing::instrument]
    pub async fn listen(&mut self) -> Result<(), tokio::io::Error> {
        let listener = TcpListener::bind(self.addr).await?;

        loop {
            let mut stream = match listener.accept().await {
                Ok((stream, _)) => stream,
                Err(err) => {
                    error!(info = "failed to connect to client", %err);
                    continue;
                }
            };

            debug!(info = "received raw message");

            let handlers = self.handlers.clone();
            let task = async move {
                let byte_buf = match Box::pin(read_stream_until_msg_end(&mut stream)).await {
                    Ok(buf) => buf,
                    Err(err) => {
                        error!(info = "failed to read bytes from TCP stream", %err);

                        let err_msg = build_msg_response(
                            MESSAGE_VERSION,
                            MessageResponseType::Error,
                            b"failed to read bytes from TCP stream",
                        );
                        let _ = stream.write_all(&err_msg).await;
                        let _ = stream.flush().await;
                        return;
                    }
                };

                let (msg_tag, msg_data) = match parse_msg_incoming(&byte_buf) {
                    Ok(data) => data,
                    Err(err) => {
                        error!(info = "invalid binary layout for TCP message", %err);

                        let err_msg = build_msg_response(
                            MESSAGE_VERSION,
                            MessageResponseType::Error,
                            b"invalid binary layout in TCP message",
                        );
                        let _ = stream.write_all(&err_msg).await;
                        let _ = stream.flush().await;
                        return;
                    }
                };

                debug!(msg_tag);
                if let Some(tx) = handlers.lock().await.get_mut(&msg_tag) {
                    let msg_bytes = RawTcpMessage(msg_data.to_vec());
                    let res = tx.send((msg_bytes, stream)).await;

                    if let Err(err) = res {
                        error!(info = "failed to send message to handler", %err);
                    } else {
                        debug!(info = "message forwarded to handler", msg_tag);
                    }
                } else {
                    let err = TcpMessageGatewayError::NoHandlerForTag(msg_tag);
                    error!(info = "no handler for TCP message", %err);

                    let err_msg = build_msg_response(
                        MESSAGE_VERSION,
                        MessageResponseType::Error,
                        b"no handler for TCP message",
                    );
                    let _ = stream.write_all(&err_msg).await;
                    let _ = stream.flush().await;
                }
            };

            tokio::task::spawn(task);
        }
    }
}

#[async_trait]
pub trait TcpMessageHandler<Msg>
where
    Self: Sized + Send + 'static,
    Msg: TcpMessage + DeserializeOwned,
    <Msg as TcpMessage>::Response: Serialize,
{
    async fn handle(&mut self, msg: Msg) -> <Msg as TcpMessage>::Response;
}

impl From<u8> for MessageResponseType {
    fn from(value: u8) -> Self {
        return match value {
            0 => MessageResponseType::Ok,
            1 => MessageResponseType::Error,
            _ => MessageResponseType::Invalid,
        };
    }
}

impl MessageResponseType {
    #[must_use]
    pub fn byte(self) -> [u8; 1] {
        return match self {
            MessageResponseType::Ok => 0_u8.to_be_bytes(),
            MessageResponseType::Error => 1_u8.to_be_bytes(),
            MessageResponseType::Invalid => u8::MAX.to_be_bytes(),
        };
    }
}

pub(crate) mod internal {
    use std::borrow::BorrowMut;

    #[allow(clippy::wildcard_imports)]
    use super::*;

    pub fn spawn<Msg, Handler>(
        handler: &Arc<Mutex<Handler>>,
    ) -> (Sender<HandlerChannelData>, JoinHandle<()>)
    where
        Handler: TcpMessageHandler<Msg> + Sized + Send + 'static,
        Msg: TcpMessage + DeserializeOwned,
        <Msg as TcpMessage>::Response: Serialize,
    {
        let (tx, mut rc) = channel::<HandlerChannelData>(128);
        let handler = handler.clone();

        let task = async move {
            while let Some((bytes, mut stream)) = rc.recv().await {
                let mut handler = handler.lock().await;
                let res = process_raw_msg::<Msg, Handler>(handler.borrow_mut(), &bytes.0).await;
                match res {
                    Ok(resp_bytes) => {
                        let res = stream.write_all(&resp_bytes).await;
                        let _ = stream.flush().await;
                        if let Err(err) = res {
                            error!(
                                info = "failed send TCP response",
                                %err
                            );
                        }
                    }
                    Err(err) => {
                        error!(
                            info = "failed to process raw TCP message from gateway",
                            %err,
                            ?bytes
                        );
                    }
                }
            }
        };

        let join_handle = tokio::task::spawn(task);

        return (tx, join_handle);
    }

    async fn process_raw_msg<Msg, Handler>(
        handler: &mut Handler,
        bytes: &[u8],
    ) -> Result<Vec<u8>, bincode::Error>
    where
        Handler: TcpMessageHandler<Msg> + Sized + Send + 'static,
        Msg: TcpMessage + DeserializeOwned,
        <Msg as TcpMessage>::Response: Serialize,
    {
        let msg = bincode::deserialize::<Msg>(bytes)?;
        let resp = handler.handle(msg).await;
        let msg_bytes = bincode::serialize(&resp)?;

        return Ok(build_msg_response(
            MESSAGE_VERSION,
            MessageResponseType::Ok,
            &msg_bytes,
        ));
    }
}
