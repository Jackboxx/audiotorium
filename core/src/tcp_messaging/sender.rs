use std::net::SocketAddr;

use async_trait::async_trait;
use serde::{de::DeserializeOwned, Serialize};
use thiserror::Error;
use tokio::{io::AsyncWriteExt, net::TcpStream};
use tracing::{debug, warn};

use crate::tcp_messaging::binary::parse_msg_response;

use super::{
    binary::{build_msg, read_stream_until_msg_end, TcpMessageLayoutError},
    MessageResponseType, TcpMessage, MESSAGE_MAX_SIZE, MESSAGE_VERSION,
};

#[derive(Debug, Error)]
pub enum TcpMessageSenderError {
    #[error("{0}")]
    TcpLayout(#[from] TcpMessageLayoutError),
    #[error("{0}")]
    TcpConnection(#[from] tokio::io::Error),
    #[error("{0}")]
    Serialization(#[from] bincode::Error),
    #[error("The remote tcp message handler returned a message marked as an error\n{0}")]
    RemoteReturnedErrorMessage(String),
    #[error("The message is to large. Maximum message size is {MESSAGE_MAX_SIZE}")]
    MessageToLarge,
}

#[async_trait]
#[allow(clippy::module_name_repetitions)]
pub trait TcpMessageSender<Msg>
where
    Msg: TcpMessage + Serialize + Send + Sync + std::fmt::Debug + 'static,
    Msg::Response: DeserializeOwned + std::fmt::Debug,
    Self: Clone + Into<SocketAddr> + std::fmt::Debug,
{
    /// Fire and forget message. Errors if message could not be serialized.
    /// # Errors
    /// - If the message cannot be serialized
    fn do_send(&self, msg: &Msg) -> Result<(), TcpMessageSenderError> {
        let addr: SocketAddr = self.clone().into();
        tracing::info!(info = "Sending TCP message", ?msg, destination = ?addr);

        let msg_bytes = build_msg(MESSAGE_VERSION, msg)?;
        tokio::task::spawn(async move {
            match TcpStream::connect(addr).await {
                Ok(mut stream) => {
                    let res = stream.write_all(&msg_bytes).await;
                    if let Err(err) = res {
                        warn!(
                            info = "failed to send TCP message to gateway",
                            ?msg_bytes,
                            %addr,
                            %err
                        );
                    }
                }
                Err(err) => {
                    warn!(
                    info = "failed to connect TCP message gateway",
                    ?msg_bytes,
                    %addr,
                    %err
                    );
                }
            }
        });

        return Ok(());
    }

    #[allow(clippy::implicit_return)]
    async fn send(&self, msg: &Msg) -> Result<Msg::Response, TcpMessageSenderError> {
        let addr: SocketAddr = self.clone().into();
        tracing::info!(info = "Sending TCP message", ?msg, destination = ?addr);

        let msg_bytes = build_msg(MESSAGE_VERSION, msg)?;

        debug!(info = "connecting to tcp gateway to send message", ?msg);
        let mut stream = TcpStream::connect(addr).await?;

        debug!(info = "sending tcp message", ?msg, ?msg_bytes);
        stream.write_all(&msg_bytes).await?;
        stream.flush().await?;

        let resp_bytes = Box::pin(read_stream_until_msg_end(&mut stream)).await?;
        debug!(info = "received raw tcp message response");

        let (msg_type, msg_data) = parse_msg_response(&resp_bytes)?;
        if let MessageResponseType::Error = msg_type {
            let err = String::from_utf8_lossy(msg_data);
            return Err(TcpMessageSenderError::RemoteReturnedErrorMessage(
                err.to_string(),
            ));
        };

        let resp = bincode::deserialize::<Msg::Response>(msg_data)?;
        debug!(info = "deserialized raw tcp message response", ?resp);
        return Ok(resp);
    }
}

impl<Msg> TcpMessageSender<Msg> for SocketAddr
where
    Msg: TcpMessage + Serialize + Send + Sync + std::fmt::Debug + 'static,
    Msg::Response: DeserializeOwned + std::fmt::Debug,
{
}
