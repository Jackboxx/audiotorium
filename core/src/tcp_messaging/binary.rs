use super::{
    MessageResponseType, TcpMessage, TcpMessageSenderError, MESSAGE_END, MESSAGE_MAX_SIZE,
    MESSAGE_VERSION,
};

use serde::Serialize;
use thiserror::Error;
use tokio::net::TcpStream;

#[derive(Debug, Error)]
pub enum TcpMessageLayoutError {
    #[error("Version mismatch. Expected {expected}. Received {received}.")]
    VersionMismatch { expected: u32, received: u32 },
    #[error("Expected 4 byte long version tag at offset 0. Received bytes: {}", .0.iter().map(|x| format!("{x:0>3}")).collect::<Vec<_>>().join(","))]
    MalformedVersion(Vec<u8>),
    #[error("Expected 4 byte long message tag at offset 4. Received bytes: [{}]",  .0.iter().map(|x| format!("{x:0>3}")).collect::<Vec<_>>().join(","))]
    MalformedTag(Vec<u8>),
    #[error("Missing 1 byte long message response type at offset 4.")]
    MissingResponseType,
}

/// Parse an incoming message, extracting the tag and message body.
///
/// # Errors
/// - If there is a version mismatch
/// - If the header is malformed
pub fn parse_msg_incoming(incoming: &[u8]) -> Result<(u32, &[u8]), TcpMessageLayoutError> {
    let len_version = 4;
    if incoming.len() < len_version {
        return Err(TcpMessageLayoutError::MalformedVersion(incoming.to_vec()));
    }

    let mut offset = 0;

    let version = u32::from_be_bytes(read_fixed_slice::<4>(incoming, offset));
    if version != MESSAGE_VERSION {
        return Err(TcpMessageLayoutError::VersionMismatch {
            expected: MESSAGE_VERSION,
            received: version,
        });
    }

    offset += 4;

    let len_version_and_tag = 8;
    if incoming.len() < len_version_and_tag {
        return Err(TcpMessageLayoutError::MalformedTag(incoming[4..].to_vec()));
    }

    let tag = u32::from_be_bytes(read_fixed_slice::<4>(incoming, offset));
    offset += 4;

    return Ok((tag, &incoming[offset..]));
}

/// Parse a response message, extracting the response type and message body.
///
/// # Errors
/// - If there is a version mismatch
/// - If the header is malformed
pub fn parse_msg_response(
    resp: &[u8],
) -> Result<(MessageResponseType, &[u8]), TcpMessageLayoutError> {
    let len_version = 4;
    if resp.len() < len_version {
        return Err(TcpMessageLayoutError::MalformedVersion(resp.to_vec()));
    }

    let version = u32::from_be_bytes(read_fixed_slice::<4>(resp, 0));
    if version != MESSAGE_VERSION {
        return Err(TcpMessageLayoutError::VersionMismatch {
            expected: MESSAGE_VERSION,
            received: version,
        });
    }

    let len_version_and_resp_type = 5;
    if resp.len() < len_version_and_resp_type {
        return Err(TcpMessageLayoutError::MissingResponseType);
    }

    return Ok((MessageResponseType::from(resp[4]), &resp[5..]));
}

/// Creates a message with a specific version and tag.
///
/// # Errors
/// - If the message cannot be serialized
/// - If the message + headers (version,tag,...) is larger than `MESSAGE_MAX_SIZE`
pub fn build_msg<Msg: Serialize + TcpMessage>(
    version: u32,
    msg: &Msg,
) -> Result<Vec<u8>, TcpMessageSenderError> {
    let tag = Msg::TAG.to_be_bytes();
    let data = bincode::serialize(msg)?;

    let msg_bytes = [&version.to_be_bytes(), &tag, data.as_slice(), &MESSAGE_END].concat();

    if msg_bytes.len() > MESSAGE_MAX_SIZE {
        return Err(TcpMessageSenderError::MessageToLarge);
    }

    return Ok(msg_bytes);
}

/// Creates a message response with a specific version and response type.
///
/// # Panics
/// - If the length of `data` is larger than `MESSAGE_MAX_SIZE` - `header length`
#[must_use]
pub fn build_msg_response(version: u32, msg_type: MessageResponseType, data: &[u8]) -> Vec<u8> {
    let len_header = 5;
    assert!(
        data.len() < MESSAGE_MAX_SIZE - len_header,
        "message reponse data should not be larger than MESSAGE_MAX_SIZE - HEADER_SIZE"
    );

    return [
        &version.to_be_bytes(),
        msg_type.byte().as_slice(),
        data,
        &MESSAGE_END,
    ]
    .concat();
}

/// Reads binary data from a TCP stream until a terminating sequence (`MESSAGE_END`) is
/// encountered.
///
/// # Errors
/// - When the message exceeds the maximum size of `MESSAGE_MAX_SIZE`
/// - When failing to read from the stream
pub async fn read_stream_until_msg_end(stream: &mut TcpStream) -> Result<Vec<u8>, std::io::Error> {
    let mut buf = [0; MESSAGE_MAX_SIZE];
    let mut read_bytes = 0;
    loop {
        stream.readable().await?;
        match stream.try_read(&mut buf) {
            Ok(n) => {
                if n == 0 {
                    return Ok(Vec::from(&buf[0..read_bytes]));
                }

                read_bytes += n;
                if read_bytes > MESSAGE_MAX_SIZE {
                    return Err(std::io::Error::new(
                        std::io::ErrorKind::InvalidData,
                        format!("The Message is to large. Maximum message size {MESSAGE_MAX_SIZE}"),
                    ));
                }

                if let Some(idx) = find_msg_end(&buf, &MESSAGE_END) {
                    return Ok(Vec::from(&buf[0..idx]));
                }
            }
            Err(ref e) if e.kind() == std::io::ErrorKind::WouldBlock => {
                continue;
            }
            Err(e) => {
                return Err(e);
            }
        }
    }
}

fn find_msg_end<T: PartialEq>(mut haystack: &[T], needle: &[T]) -> Option<usize> {
    if needle.is_empty() {
        return None;
    }

    let mut idx = 0;
    while !haystack.is_empty() {
        if haystack.starts_with(needle) {
            return Some(idx);
        }
        idx += 1;

        haystack = &haystack[1..];
    }

    return None;
}

fn read_fixed_slice<const N: usize>(bytes: &[u8], offset: usize) -> [u8; N] {
    assert!(
        bytes.len() >= N + offset,
        "len bytes must be >= to lenght of result slice"
    );

    let slice = &bytes[offset..N + offset];
    let mut buf = [0; N];
    buf.copy_from_slice(slice);

    return buf;
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_find_msg_end() {
        let all = [0, 0, 1, 0, 1, 0, 1, 255, 255, 255, 255];
        let target = [255, 255, 255, 255];

        let idx = find_msg_end(&all, &target).unwrap();

        assert_eq!(idx, 7);
        assert_eq!(&all[0..idx], &[0, 0, 1, 0, 1, 0, 1]);
    }
}
