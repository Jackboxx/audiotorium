use std::sync::Arc;

use sqlx::PgPool;

use crate::{
    error::{AppError, AppErrorKind, IntoAppError},
    identifier::ItemUid,
    schema::audio::{AudioPlaylistMetadata, AudioTrackMetadata},
};

#[allow(clippy::missing_errors_doc)]
#[allow(clippy::missing_panics_doc)]
pub async fn does_audio_playlist_exist<T: AsRef<str> + std::fmt::Debug>(
    uid: &ItemUid<T>,
    pool: &PgPool,
) -> Result<bool, AppError> {
    async fn inner(uid: &str, pool: &PgPool) -> Result<bool, AppError> {
        struct Exists {
            exists: Option<bool>,
        }

        let exists = sqlx::query_as!(
            Exists,
            "SELECT EXISTS (
            SELECT identifier
            FROM audio_playlist
            WHERE identifier = $1) as exists",
            uid
        )
        .fetch_one(pool)
        .await
        .into_app_err(
            "failed to determin if audio playlist already exists",
            AppErrorKind::Database,
            &[&format!("UID: {uid}")],
        )?
        .exists
        .unwrap_or(false);

        Ok(exists)
    }

    inner(uid.as_ref(), pool).await
}

#[allow(clippy::missing_errors_doc)]
pub async fn select_audio_metadata_from_db<T: AsRef<str> + std::fmt::Debug>(
    uid: &ItemUid<T>,
    pool: &PgPool,
) -> Result<Option<AudioTrackMetadata>, AppError> {
    let uid = uid.as_ref();

    async fn inner(uid: &str, pool: &PgPool) -> Result<Option<AudioTrackMetadata>, AppError> {
        sqlx::query_as!(
        AudioTrackMetadata,
        "SELECT identifier, name, author, duration, cover_art_url FROM audio_metadata where identifier = $1",
        uid
    )
        .fetch_optional(pool)
        .await
        .into_app_err(
            "failed to get audio metdata",
            AppErrorKind::Database,
            &[&format!("UID: {uid}")],
        )
    }

    inner(uid, pool).await
}

#[allow(clippy::missing_errors_doc)]
pub async fn select_audio_playlist_metadata_from_db<T: AsRef<str> + std::fmt::Debug>(
    uid: &ItemUid<T>,
    pool: &PgPool,
) -> Result<Option<AudioPlaylistMetadata>, AppError> {
    let uid = uid.as_ref();

    async fn inner(uid: &str, pool: &PgPool) -> Result<Option<AudioPlaylistMetadata>, AppError> {
        sqlx::query_as!(
        AudioPlaylistMetadata,
        "SELECT identifier, name, author, cover_art_url FROM audio_playlist where identifier = $1",
        uid
    )
        .fetch_optional(pool)
        .await
        .into_app_err(
            "failed to get audio metdata",
            AppErrorKind::Database,
            &[&format!("UID: {uid}")],
        )
    }

    inner(uid, pool).await
}

#[allow(clippy::missing_errors_doc)]
#[allow(clippy::missing_panics_doc)]
pub async fn select_all_audio_metadata_from_db(
    limit: Option<i64>,
    offset: Option<i64>,
    pool: &PgPool,
) -> Result<Arc<[AudioTrackMetadata]>, AppError> {
    let limit = limit.unwrap_or(50);
    let offset = offset.unwrap_or(0);

    sqlx::query_as!(
        AudioTrackMetadata,
        "SELECT identifier, name, author, duration, cover_art_url FROM audio_metadata
        LIMIT $1 OFFSET $2",
        limit,
        offset
    )
    .fetch_all(pool)
    .await
    .map(Into::into)
    .into_app_err(
        "failed to get all audio metdata from db",
        AppErrorKind::Database,
        &[&format!("LIMIT: {limit}"), &format!("OFFSET: {offset}")],
    )
}

#[allow(clippy::missing_errors_doc)]
#[allow(clippy::missing_panics_doc)]
pub async fn select_all_playlist_metadata_from_db(
    limit: Option<i64>,
    offset: Option<i64>,
    pool: &PgPool,
) -> Result<Arc<[AudioPlaylistMetadata]>, AppError> {
    let limit = limit.unwrap_or(50);
    let offset = offset.unwrap_or(0);

    sqlx::query_as!(
        AudioPlaylistMetadata,
        "SELECT identifier, name, author, cover_art_url FROM audio_playlist
        LIMIT $1 OFFSET $2",
        limit,
        offset,
    )
    .fetch_all(pool)
    .await
    .map(Into::into)
    .into_app_err(
        "failed to get all playlist metdata",
        AppErrorKind::Database,
        &[&format!("LIMIT: {limit}"), &format!("OFFSET: {offset}")],
    )
}

#[allow(clippy::missing_errors_doc)]
pub async fn select_playlist_items_from_db<T: AsRef<str> + std::fmt::Debug>(
    playlist_uid: &ItemUid<T>,
    limit: Option<i64>,
    offset: Option<i64>,
    pool: &PgPool,
) -> Result<Arc<[AudioTrackMetadata]>, AppError> {
    let playlist_uid = playlist_uid.as_ref();

    async fn inner(
        playlist_uid: &str,
        limit: Option<i64>,
        offset: Option<i64>,
        pool: &PgPool,
    ) -> Result<Arc<[AudioTrackMetadata]>, AppError> {
        let limit = limit.unwrap_or(50);
        let offset = offset.unwrap_or(0);

        sqlx::query_as!(
            AudioTrackMetadata,
            "SELECT audio.identifier, audio.name, audio.author, audio.duration, audio.cover_art_url
             FROM audio_metadata audio
                 INNER JOIN audio_playlist_item items
                 ON audio.identifier = items.item_identifier
             WHERE items.playlist_identifier = $1
             ORDER BY position
             LIMIT $2 OFFSET $3",
            playlist_uid,
            limit,
            offset,
        )
        .fetch_all(pool)
        .await
        .map(|vec| return vec.into_iter().map(Into::into).collect())
        .into_app_err(
            "failed to get all audio items in playlist ",
            AppErrorKind::Database,
            &[
                &format!("PLAYLIST_UID: {playlist_uid}"),
                &format!("LIMIT: {limit}"),
                &format!("OFFSET: {offset}"),
            ],
        )
    }

    inner(playlist_uid, limit, offset, pool).await
}

#[allow(clippy::missing_errors_doc)]
pub async fn select_next_position_item_for_playlist<T: AsRef<str> + std::fmt::Debug>(
    playlist_uid: &ItemUid<T>,
    pool: &PgPool,
) -> Result<i32, AppError> {
    let playlist_uid = playlist_uid.as_ref();

    async fn inner(playlist_uid: &str, pool: &PgPool) -> Result<i32, AppError> {
        struct Position {
            position: Option<i32>,
        }

        let position = sqlx::query_as!(
        Position,
        r#"SELECT MAX(position) as "position" FROM audio_playlist_item WHERE playlist_identifier = $1"#,
        playlist_uid
    )
    .fetch_one(pool)
    .await
    .into_app_err(
        "failed to audio item position in playlist ",
        AppErrorKind::Database,
        &[&format!("PLAYLIST_UID: {playlist_uid}")],
    )?;

        Ok(position.position.map_or(0, |x| return x + 1))
    }

    inner(playlist_uid, pool).await
}
