use std::{future::Future, sync::Arc};

use crate::{
    database::select_audio::select_next_position_item_for_playlist,
    error::{AppError, AppErrorKind, IntoAppError},
    identifier::{Identifier, ItemUid, YoutubePlaylistUrl},
    schema::audio::AudioPlaylistMetadata,
};
use sqlx::PgPool;

use super::select_audio::does_audio_playlist_exist;

#[allow(clippy::missing_errors_doc)]
#[allow(clippy::missing_panics_doc)]
pub async fn store_playlist_if_not_exists<'a, MetadataFetchReturn>(
    url: &'a YoutubePlaylistUrl<Arc<str>>,
    pool: &PgPool,
    metadata_fetch_func: impl Fn(&'a YoutubePlaylistUrl<Arc<str>>) -> MetadataFetchReturn,
) -> Result<(), AppError>
where
    MetadataFetchReturn: Future<Output = Result<AudioPlaylistMetadata, AppError>>,
{
    let uid = url.uid();

    let exists = does_audio_playlist_exist(&uid, pool).await?;
    if exists {
        return Ok(());
    }

    let metadata = metadata_fetch_func(url).await?;

    let mut tx = pool.begin().await.into_app_err(
        "failed to start transaction",
        AppErrorKind::Database,
        &[],
    )?;

    sqlx::query!(
        "INSERT INTO audio_playlist
        (identifier, name, author, cover_art_url)
        VALUES ($1, $2, $3, $4)",
        metadata.identifier.as_ref(),
        metadata.name.inner_as_ref(),
        metadata.author.inner_as_ref(),
        metadata.cover_art_url.inner_as_ref(),
    )
    .execute(&mut *tx)
    .await
    .into_app_err(
        "failed to create audio playlist",
        AppErrorKind::Database,
        &[&format!("UID: {uid:?}")],
    )?;

    tx.commit()
        .await
        .into_app_err("failed to commit transaction", AppErrorKind::Database, &[])
}

#[allow(clippy::missing_errors_doc)]
pub async fn store_playlist_item_relation_if_not_exists<T: AsRef<str> + std::fmt::Debug>(
    playlist_uid: &ItemUid<T>,
    audio_uid: &ItemUid<T>,
    pool: &PgPool,
) -> Result<(), AppError> {
    let position = select_next_position_item_for_playlist(playlist_uid, pool).await?;
    let playlist_uid = playlist_uid.as_ref();
    let audio_uid = audio_uid.as_ref();

    async fn inner(
        position: i32,
        playlist_uid: &str,
        audio_uid: &str,
        pool: &PgPool,
    ) -> Result<(), AppError> {
        let mut tx = pool.begin().await.into_app_err(
            "failed to start transaction",
            AppErrorKind::Database,
            &[],
        )?;

        sqlx::query!(
            "INSERT INTO audio_playlist_item
        (playlist_identifier, item_identifier, position) VALUES ($1, $2, $3)
        ON CONFLICT DO NOTHING",
            playlist_uid,
            audio_uid,
            position,
        )
        .execute(&mut *tx)
        .await
        .into_app_err(
            "failed to add audio to playlist",
            AppErrorKind::Database,
            &[
                &format!("PLAYLIST_UID: {playlist_uid}"),
                &format!("AUDIO_UID: {audio_uid}"),
            ],
        )?;

        tx.commit()
            .await
            .into_app_err("failed to commit transaction", AppErrorKind::Database, &[])
    }

    inner(position, playlist_uid, audio_uid, pool).await
}
