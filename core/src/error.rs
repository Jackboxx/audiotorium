use std::{fmt::Display, sync::Arc};

#[cfg(feature = "serde-impl")]
use serde::{Deserialize, Serialize};

#[cfg(all(feature = "serde-impl", test))]
use ts_rs::{Dependency, TS};

#[cfg(feature = "actix-impl")]
use actix::Message;

pub trait IntoAppError<R> {
    fn into_app_err<'a>(
        self,
        info: impl Into<Arc<str>>,
        kind: AppErrorKind,
        extra_details: &'a [&'a str],
    ) -> R;
}

#[derive(Debug)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Deserialize),
    serde(rename_all = "camelCase")
)]
#[cfg_attr(feature = "actix-impl", derive(Message))]
#[cfg_attr(feature = "actix-impl", rtype(result = "()"))]
pub struct AppError {
    kind: AppErrorKind,
    info: Arc<str>,
    detailed_info: Arc<str>,
}

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "kebab-case")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub enum AppErrorKind {
    Queue,
    Api,
    WebScraper,
    LocalData,
    Database,
    Download,
}

#[cfg(feature = "serde-impl")]
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub struct UserError {
    kind: AppErrorKind,
    info: Arc<str>,
}

#[cfg(feature = "serde-impl")]
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub enum UserErrorMessage {
    UserError(UserError),
}

impl Clone for AppError {
    fn clone(&self) -> Self {
        return Self {
            kind: self.kind.clone(),
            info: Arc::clone(&self.info),
            detailed_info: Arc::clone(&self.detailed_info),
        };
    }
}

impl Display for AppError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let width: usize = 80;

        let kind = self.kind.to_string();
        let header = format!(
            "\n--{kind}{sep}",
            sep = "-".repeat(width.saturating_sub(kind.len() + 2))
        );
        let body = format!(
            "\nINFO: {info}\n\n{details}",
            info = self.info,
            details = self.detailed_info
        );
        let footer = "-".repeat(width);

        let msg = format!("{header}{body}\n{footer}");
        return write!(f, "{msg}");
    }
}

impl Display for AppErrorKind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let str = match self {
            Self::Api => "API ERROR",
            Self::Queue => "QUEUE ERROR",
            Self::Database => "DATABASE ERROR",
            Self::Download => "DOWNLOAD ERROR",
            Self::LocalData => "LOCAL DATA ERROR",
            Self::WebScraper => "WEB SCRAPER ERROR",
        };

        return write!(f, "{str}");
    }
}

impl<E: Display> IntoAppError<AppError> for E {
    fn into_app_err<'a>(
        self,
        info: impl Into<Arc<str>>,
        kind: AppErrorKind,
        extra_details: &'a [&'a str],
    ) -> AppError {
        let app_err = AppError {
            kind,
            info: info.into(),
            detailed_info: AppError::format_detailed_info(self, extra_details),
        };

        tracing::error!(?app_err);
        return app_err;
    }
}

impl<T, E> IntoAppError<Result<T, AppError>> for Result<T, E>
where
    E: IntoAppError<AppError>,
{
    fn into_app_err<'a>(
        self,
        info: impl Into<Arc<str>>,
        kind: AppErrorKind,
        extra_details: &'a [&'a str],
    ) -> Result<T, AppError> {
        return self.map_err(|err| return err.into_app_err(info, kind, extra_details));
    }
}

#[cfg(feature = "serde-impl")]
impl Serialize for AppError {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let user_err = self.user_err();
        return UserErrorMessage::UserError(user_err).serialize(serializer);
    }
}

impl AppError {
    #[tracing::instrument]
    pub fn new(
        kind: AppErrorKind,
        info: impl Into<Arc<str>> + std::fmt::Debug,
        extra_details: &[&str],
    ) -> Self {
        let app_err = Self {
            kind,
            info: info.into(),
            detailed_info: AppError::format_detailed_info("", extra_details),
        };

        tracing::error!(?app_err);
        return app_err;
    }

    fn format_detailed_info<D: Display>(err: D, extra_details: &[&str]) -> Arc<str> {
        return format!(
            "{err}DETAILS:\n{extra}",
            err = if err.to_string().is_empty() {
                String::new()
            } else {
                format!("ERROR: {err}\n\n")
            },
            extra = extra_details.join("\n")
        )
        .into();
    }

    #[cfg(feature = "serde-impl")]
    fn user_err(&self) -> UserError {
        return UserError {
            kind: self.kind.clone(),
            info: Arc::clone(&self.info),
        };
    }
}

#[cfg(all(feature = "serde-impl", test))]
impl TS for AppError {
    fn name() -> String {
        return "UserErrorMessage".to_owned();
    }

    fn dependencies() -> Vec<Dependency>
    where
        Self: 'static,
    {
        return vec![Dependency::from_ty::<UserErrorMessage>().unwrap()];
    }

    fn transparent() -> bool {
        return true;
    }
}
