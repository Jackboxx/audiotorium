#[cfg(feature = "actor-tcp")]
pub use tcp_only::*;

pub use always::*;

mod always {
    use std::sync::Arc;

    use serde::de::Error;
    #[cfg(feature = "serde-impl")]
    use serde::{Deserialize, Serialize};

    #[derive(Debug, Clone, PartialEq, Eq, Hash)]
    pub enum DeviceId {
        Alsa { name: Arc<str> },
    }

    #[cfg(feature = "serde-impl")]
    impl Serialize for DeviceId {
        fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: serde::Serializer,
        {
            let encoded = match self {
                DeviceId::Alsa { name } => format!("alsa-{name}"),
            };

            return serializer.serialize_str(&encoded);
        }
    }

    impl<'de> Deserialize<'de> for DeviceId {
        fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where
            D: serde::Deserializer<'de>,
        {
            let decoded = String::deserialize(deserializer)?;
            match decoded.split_once('-') {
                Some(("alsa", name)) => return Ok(DeviceId::Alsa { name: name.into() }),
                _ => return Err(Error::custom(format!("Invalid device ID '{decoded}'"))),
            }
        }
    }
}

#[cfg(feature = "actor-tcp")]
mod tcp_only {
    use crate::{
        schema::audio::{AudioQueueItem, PlaybackState},
        tags::MessageTags,
        tcp_messaging::TcpMessage,
    };

    use serde::{Deserialize, Serialize};

    use super::DeviceId;

    #[derive(Debug, Serialize, Deserialize)]
    pub struct DevicePlaybackAction {
        device_id: DeviceId,
        action: PlaybackAction,
    }

    #[derive(Debug, Serialize, Deserialize)]
    pub enum PlaybackAction {
        PlayNext,
        PlayPrev,
        PlaySelected {
            idx: usize,
            allow_self_select: bool,
        },
        SetPlaybackState {
            state: PlaybackState,
        },
        SetProgress {
            progress: f64,
        },
        SetVolume {
            volume: f32,
        },
        PushToQueue {
            item: AudioQueueItem,
        },
        RemoveFromQueue {
            identifier: uuid::Uuid,
        },
        MoveQueueItems {
            local_id_current: uuid::Uuid,
            idx_current: usize,
            local_id_new: uuid::Uuid,
            idx_new: usize,
        },
        ShuffleQueue,
        ClearQueue,
        // TODO: don't want this to be an action on the audio player
        // TryRecoverDevice {
        //     progress_before_device_error: f64,
        //     wait_since_last_attempt: Option<Duration>,
        // },
    }

    impl TcpMessage for DevicePlaybackAction {
        const TAG: u32 = MessageTags::DOWNLOADER_PLAYBACK_ACTION;
        type Response = ();
    }
}
