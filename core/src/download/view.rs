use std::sync::Arc;

#[cfg(feature = "serde-impl")]
use serde::{Deserialize, Serialize};

#[cfg(all(feature = "serde-impl", test))]
use ts_rs::TS;

use crate::identifier::{YoutubePlaylistUrl, YoutubeVideoUrl};

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "kebab-case")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub enum DownloadState {
    Active,
    Pending,
    Failed { reason: Arc<str> },
}

#[derive(Debug, Clone, Eq)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "kebab-case")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub enum DownloadClientView {
    YoutubeVideo {
        #[cfg_attr(all(feature = "serde-impl", test), ts(type = "string"))]
        url: YoutubeVideoUrl<Arc<str>>,
    },
    YoutubePlaylist {
        #[cfg_attr(all(feature = "serde-impl", test), ts(type = "string"))]
        playlist_url: YoutubePlaylistUrl<Arc<str>>,
        #[cfg_attr(all(feature = "serde-impl", test), ts(type = "Array<string>"))]
        video_urls: Arc<[YoutubeVideoUrl<Arc<str>>]>,
    },
}

impl std::hash::Hash for DownloadClientView {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        match self {
            Self::YoutubeVideo { url } => url.hash(state),
            Self::YoutubePlaylist { playlist_url, .. } => playlist_url.hash(state),
        };
    }
}

impl PartialEq for DownloadClientView {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (
                DownloadClientView::YoutubeVideo { url },
                DownloadClientView::YoutubeVideo { url: url_other },
            ) => return url.eq(url_other),
            (
                DownloadClientView::YoutubePlaylist { playlist_url, .. },
                DownloadClientView::YoutubePlaylist {
                    playlist_url: playlist_url_other,
                    ..
                },
            ) => return playlist_url.eq(playlist_url_other),
            _ => return false,
        }
    }
}
