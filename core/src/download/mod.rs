mod request;
mod view;

use std::{net::IpAddr, str::FromStr, sync::Arc};

pub use request::*;
pub use view::{DownloadClientView, DownloadState};

use crate::identifier::ItemUid;

#[derive(Debug)]
pub struct MediaUrl {
    id: ItemUid<Arc<str>>,
    url: url::Url,
}

impl MediaUrl {
    #[must_use]
    #[allow(clippy::missing_panics_doc)]
    pub fn new(id: ItemUid<Arc<str>>, host: IpAddr, port: u16) -> Self {
        let url = format!("{}/{}", Self::base_path(host, port), &**id);
        let url = url::Url::from_str(&url).unwrap_or_else(|err| {
            panic!("Media url should always be a valid URL when created by item uid.\n{err}");
        });

        return Self { id, url };
    }

    #[must_use]
    pub fn base_path(host: IpAddr, port: u16) -> String {
        return format!("http://{host}:{port}/media");
    }

    #[must_use]
    pub fn url(&self) -> &url::Url {
        return &self.url;
    }

    #[must_use]
    pub fn id(&self) -> &ItemUid<Arc<str>> {
        return &self.id;
    }
}
