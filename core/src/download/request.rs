#[cfg(feature = "actor-tcp")]
pub use tcp_only::*;

pub use always::*;

#[cfg(feature = "actor-tcp")]
mod tcp_only {
    use crate::{tags::MessageTags, tcp_messaging::TcpMessage};
    use std::{net::SocketAddr, sync::Arc};

    use crate::identifier::{YoutubePlaylistUrl, YoutubeVideoUrl};

    use super::DownloadUpdateNotification;
    use serde::{Deserialize, Serialize};

    /// Used to request downloads from the `downloader` crate
    #[derive(Debug, Clone, Serialize, Deserialize)]
    pub struct DownloadAudioRequest {
        notification_subscriber: Option<DownloadRequestSubscriber>,
        required_info: DownloadRequestInfo,
    }

    /// TCP message encodeable response send after a download finishes.
    #[derive(Debug, Serialize, Deserialize)]
    pub struct NotifyDownloadUpdate {
        pub update: DownloadUpdateNotification,
        pub identifier: Option<u128>,
    }

    #[cfg(feature = "actor-tcp")]
    impl TcpMessage for DownloadAudioRequest {
        const TAG: u32 = MessageTags::DOWNLOADER_DOWNLOAD_AUDIO_REQUEST;
        type Response = ();
    }

    #[cfg(feature = "actor-tcp")]
    impl TcpMessage for NotifyDownloadUpdate {
        const TAG: u32 = MessageTags::DOWNLOADER_NOTIFY_DOWNLOAD_UPDATE;
        type Response = ();
    }

    /// Used to send back progress/success/failure notifications to the client that sent the initial
    /// download request
    #[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
    pub struct DownloadRequestSubscriber {
        addr: SocketAddr,
        identifier: u128,
    }

    #[derive(Debug, Clone, Deserialize, Serialize)]
    pub enum DownloadRequestInfo {
        YoutubeVideo { url: YoutubeVideoUrl<Arc<str>> },
        YoutubePlaylist { url: YoutubePlaylistUrl<Arc<str>> },
    }

    impl DownloadAudioRequest {
        #[must_use]
        pub fn new(
            notification_subscriber: Option<DownloadRequestSubscriber>,
            required_info: DownloadRequestInfo,
        ) -> Self {
            return Self {
                notification_subscriber,
                required_info,
            };
        }

        #[must_use]
        pub fn notification_subscriber(&self) -> Option<&DownloadRequestSubscriber> {
            return self.notification_subscriber.as_ref();
        }

        #[must_use]
        pub fn required_info(&self) -> &DownloadRequestInfo {
            return &self.required_info;
        }
    }

    impl DownloadRequestSubscriber {
        #[must_use]
        pub fn new(addr: SocketAddr, client_side_identifier: u128) -> Self {
            return Self {
                addr,
                identifier: client_side_identifier,
            };
        }

        #[must_use]
        pub fn identifier(&self) -> u128 {
            return self.identifier;
        }

        #[must_use]
        pub fn addr(&self) -> SocketAddr {
            return self.addr;
        }
    }
}

mod always {
    use std::sync::Arc;

    #[cfg(feature = "serde-impl")]
    use serde::{Deserialize, Serialize};

    use crate::{download::DownloadClientView, error::AppError, identifier::ItemUid};

    /// Used to asynchronously notify `downloader` clients of the state of their requested downloads
    #[derive(Debug, Clone)]
    #[cfg_attr(feature = "serde-impl", derive(Serialize, Deserialize))]
    pub enum DownloadUpdateNotification {
        SingleAdded {
            id: ItemUid<Arc<str>>,
            view: DownloadClientView,
        },
        BatchAdded {
            id: ItemUid<Arc<str>>,
            view: DownloadClientView,
        },
        SingleFinished {
            id: ItemUid<Arc<str>>,
            view: DownloadClientView,
        },
        SingleFailed {
            id: ItemUid<Arc<str>>,
            err: AppError,
            view: DownloadClientView,
        },
        BatchUpdated {
            id: ItemUid<Arc<str>>,
            batch_view: DownloadClientView,
        },
        BatchFinished {
            id: ItemUid<Arc<str>>,
        },
        BatchDownloadFailedToStart {
            id: ItemUid<Arc<str>>,
            err: AppError,
            batch_view: DownloadClientView,
        },
    }
}
