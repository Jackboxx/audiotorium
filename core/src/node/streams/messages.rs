#[cfg(feature = "serde-impl")]
use serde::{Deserialize, Serialize};

#[cfg(feature = "actix-impl")]
use actix::Message;

#[cfg(feature = "clap-impl")]
use clap::ValueEnum;

#[cfg(all(feature = "serde-impl", test))]
use ts_rs::TS;

use crate::{
    node::AudioNodeHealth,
    schema::audio::{AudioInfoPartial, AudioQueueItem},
    wrappers::ArcSlice,
};

#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "SCREAMING_SNAKE_CASE")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
#[cfg_attr(feature = "clap-impl", derive(ValueEnum))]
pub enum AudioNodeInfoStreamType {
    Queue,
    Health,
    AudioStateInfo,
}

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "SCREAMING_SNAKE_CASE")
)]
#[cfg_attr(feature = "actix-impl", derive(Message))]
#[cfg_attr(feature = "actix-impl", rtype(result = "()"))]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub enum AudioNodeInfoStreamMessage {
    // can't use SerializableQueue due to issue discussed
    // here: https://github.com/Aleph-Alpha/ts-rs/issues/70
    Queue(ArcSlice<AudioQueueItem>),
    Health(AudioNodeHealth),
    AudioStateInfo(AudioInfoPartial),
}

impl AudioNodeInfoStreamMessage {
    #[must_use]
    pub fn msg_type(&self) -> AudioNodeInfoStreamType {
        match self {
            AudioNodeInfoStreamMessage::Queue(_) => return AudioNodeInfoStreamType::Queue,
            AudioNodeInfoStreamMessage::Health(_) => return AudioNodeInfoStreamType::Health,
            AudioNodeInfoStreamMessage::AudioStateInfo(_) => {
                return AudioNodeInfoStreamType::AudioStateInfo
            }
        }
    }
}
