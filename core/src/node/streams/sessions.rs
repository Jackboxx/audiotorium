#[cfg(feature = "serde-impl")]
use serde::{Deserialize, Serialize};

#[cfg(all(feature = "serde-impl", test))]
use ts_rs::TS;

use crate::{
    node::AudioNodeHealth,
    schema::audio::{AudioInfoPartial, AudioQueueItem},
    wrappers::ArcSlice,
};

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "SCREAMING_SNAKE_CASE")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub enum NodeSessionConnectedMessage {
    #[cfg_attr(feature = "serde-impl", serde(rename_all = "camelCase"))]
    SessionConnectedResponse {
        // can't use SerializableQueue due to issue discussed
        // here: https://github.com/Aleph-Alpha/ts-rs/issues/70
        queue: Option<ArcSlice<AudioQueueItem>>,
        health: Option<AudioNodeHealth>,
        audio_state_info: Option<AudioInfoPartial>,
    },
}
