use std::sync::Arc;

#[cfg(feature = "serde-impl")]
use serde::{Deserialize, Serialize};

#[cfg(all(feature = "serde-impl", test))]
use ts_rs::TS;

pub mod commands;
mod health;
pub mod streams;

pub use health::{AudioNodeHealth, AudioNodeHealthMild, AudioNodeHealthPoor};

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "camelCase")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub struct AudioNodeInfo {
    pub source_name: Arc<str>,
    pub human_readable_name: String,
    pub health: AudioNodeHealth,
}
