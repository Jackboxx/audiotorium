use uuid::Uuid;

#[cfg(all(feature = "serde-impl", test))]
use ts_rs::TS;

#[cfg(feature = "serde-impl")]
use serde::{Deserialize, Serialize};

#[cfg(feature = "actix-impl")]
use actix::Message;

use crate::identifier::AudioIdentifier;

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "SCREAMING_SNAKE_CASE")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
#[cfg_attr(feature = "actix-impl", derive(Message))]
#[cfg_attr(feature = "actix-impl", rtype(result = "()"))]
pub enum AudioNodeCommand {
    AddQueueItem(AddQueueItemParams),
    RemoveQueueItem(RemoveQueueItemParams),
    MoveQueueItem(MoveQueueItemParams),
    ShuffleQueue,
    ClearQueue,
    SetAudioVolume(SetAudioVolumeParams),
    SetAudioProgress(SetAudioProgressParams),
    PauseQueue,
    UnPauseQueue,
    PlayNext,
    PlayPrevious,
    PlaySelected(PlaySelectedParams),
}

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "camelCase")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub struct AddQueueItemParams {
    pub identifier: AudioIdentifier,
}

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "camelCase")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub struct RemoveQueueItemParams {
    pub local_id: Uuid,
}

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "camelCase")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub struct PlaySelectedParams {
    pub index: usize,
}

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "camelCase")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub struct MoveQueueItemParams {
    pub current_local_id: Uuid,
    pub old_pos: usize,

    pub target_local_id: Uuid,
    pub new_pos: usize,
}

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "camelCase")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub struct SetAudioVolumeParams {
    pub volume: f32,
}

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "camelCase")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub struct SetAudioProgressParams {
    pub progress: f64,
}
