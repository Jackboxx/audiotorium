#[cfg(feature = "serde-impl")]
use serde::{Deserialize, Serialize};

#[cfg(all(feature = "serde-impl", test))]
use ts_rs::TS;

#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "kebab-case")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
#[allow(clippy::module_name_repetitions)]
pub enum AudioNodeHealth {
    Good,
    Mild(AudioNodeHealthMild),
    Poor(AudioNodeHealthPoor),
}

#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "kebab-case")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub enum AudioNodeHealthMild {
    Buffering,
}

#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "kebab-case")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub enum AudioNodeHealthPoor {
    DeviceNotAvailable,
    AudioStreamReadFailed,
    AudioBackendError(String),
}
