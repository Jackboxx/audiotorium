use std::time::{Duration, Instant};

#[derive(Debug)]
pub struct HeartBeat;

pub trait MessageLimiter<M>: Send
where
    M: Send,
{
    fn can_send(&self, msg: &M) -> bool;

    fn has_sent(&mut self, msg: &M);
}

pub struct MessageSendHandler<M>
where
    M: Send,
{
    limiters: Vec<Box<dyn MessageLimiter<M>>>,
}

impl<M> MessageSendHandler<M>
where
    M: Send,
{
    #[must_use]
    pub fn with_limiters(limiters: Vec<Box<dyn MessageLimiter<M>>>) -> Self {
        return Self { limiters };
    }

    pub fn send_msg<SendFunc>(&mut self, msg: M, do_send: SendFunc)
    where
        SendFunc: Fn(M),
    {
        let can_send = self
            .limiters
            .iter()
            .map(|l| return l.can_send(&msg))
            .reduce(|acc, x| return acc && x)
            .unwrap_or(false);

        if can_send {
            self.limiters.iter_mut().for_each(|l| l.has_sent(&msg));
            do_send(msg);
        }
    }
}

#[derive(Debug, Clone)]
pub struct RateLimiter {
    last_msg_sent_at: Instant,
    hard_rate_limit: Duration,
}

#[derive(Debug, Clone)]
pub struct ChangeDetector<M>
where
    M: Send + PartialEq + Clone,
{
    last_msg_sent: Option<M>,
}

impl<M> MessageLimiter<M> for RateLimiter
where
    M: Send,
{
    fn can_send(&self, _msg: &M) -> bool {
        return Instant::now().duration_since(self.last_msg_sent_at) > self.hard_rate_limit;
    }

    fn has_sent(&mut self, _msg: &M) {
        self.last_msg_sent_at = Instant::now();
    }
}

impl<M> MessageLimiter<M> for ChangeDetector<M>
where
    M: Send + PartialEq + Clone,
{
    fn can_send(&self, msg: &M) -> bool {
        return self
            .last_msg_sent
            .as_ref()
            .map_or(true, |lms| return lms != msg);
    }

    fn has_sent(&mut self, msg: &M) {
        self.last_msg_sent = Some(msg.clone());
    }
}

impl<M> ChangeDetector<M>
where
    M: Send + PartialEq + Clone,
{
    pub fn new(default_msg: Option<M>) -> Self {
        return Self {
            last_msg_sent: default_msg,
        };
    }
}

impl Default for RateLimiter {
    fn default() -> Self {
        return Self {
            last_msg_sent_at: Instant::now(),
            hard_rate_limit: Duration::from_millis(33),
        };
    }
}

#[cfg(all(test, feature = "actix-impl"))]
mod tests {
    use actix::{Actor, Context, Handler, Message};

    #[derive(Debug, Clone, PartialEq, Eq, Message)]
    #[rtype(result = "()")]
    pub struct TestMessage {
        pub value: String,
    }

    #[derive(Debug, Clone, Message)]
    #[rtype(result = "usize")]
    pub struct GetReceivedMessageCount;

    pub struct TestMessageHandler {
        msgs_received: usize,
        expected_msg: Option<TestMessage>,
    }
    impl From<&str> for TestMessage {
        fn from(value: &str) -> Self {
            return Self {
                value: value.to_owned(),
            };
        }
    }

    impl From<String> for TestMessage {
        fn from(value: String) -> Self {
            return Self { value };
        }
    }

    impl TestMessageHandler {
        pub fn new(expected_msg: Option<TestMessage>) -> Self {
            return Self {
                msgs_received: 0,
                expected_msg,
            };
        }
    }

    impl Actor for TestMessageHandler {
        type Context = Context<Self>;
    }

    impl Handler<TestMessage> for TestMessageHandler {
        type Result = ();

        fn handle(&mut self, msg: TestMessage, _ctx: &mut Self::Context) -> Self::Result {
            if let Some(expected_msg) = self.expected_msg.as_ref() {
                pretty_assertions::assert_eq!(&msg, expected_msg);
            }

            self.msgs_received += 1;
        }
    }

    impl Handler<GetReceivedMessageCount> for TestMessageHandler {
        type Result = usize;

        fn handle(
            &mut self,
            _msg: GetReceivedMessageCount,
            _ctx: &mut Self::Context,
        ) -> Self::Result {
            return self.msgs_received;
        }
    }

    use super::*;

    #[tokio::test]
    async fn test_change_notifier() {
        {
            let test_handler = TestMessageHandler::new(Some("test".into()));
            let addr = test_handler.start();

            let mut msg_handler = MessageSendHandler::with_limiters(vec![Box::new(
                ChangeDetector::<TestMessage>::new(None),
            )]);

            msg_handler.send_msg("test".into(), |msg| addr.do_send(msg));

            msg_handler.send_msg("test".into(), |msg| addr.do_send(msg)); // will not be received due to change

            let msg_count = addr.send(GetReceivedMessageCount).await.unwrap();
            pretty_assertions::assert_eq!(msg_count, 1);
        }

        {
            let test_handler = TestMessageHandler::new(Some("test".into()));
            let addr = test_handler.start();

            let mut msg_handler = MessageSendHandler::with_limiters(vec![Box::new(
                ChangeDetector::new(Some(TestMessage {
                    value: "test".to_owned(),
                })),
            )]);

            msg_handler.send_msg("test".into(), |msg| addr.do_send(msg));

            let msg_count = addr.send(GetReceivedMessageCount).await.unwrap();
            pretty_assertions::assert_eq!(msg_count, 0);
        }

        {
            let test_handler = TestMessageHandler::new(None);
            let addr = test_handler.start();

            let mut msg_handler = MessageSendHandler::with_limiters(vec![Box::new(
                ChangeDetector::<TestMessage>::new(None),
            )]);

            msg_handler.send_msg("test 1".into(), |msg| addr.do_send(msg)); // send
            msg_handler.send_msg("test 2".into(), |msg| addr.do_send(msg)); // send
            msg_handler.send_msg("test 1".into(), |msg| addr.do_send(msg)); // send
            msg_handler.send_msg("test 1".into(), |msg| addr.do_send(msg)); // ignore

            let msg_count = addr.send(GetReceivedMessageCount).await.unwrap();
            pretty_assertions::assert_eq!(msg_count, 3);
        }
    }

    #[tokio::test]
    async fn test_rate_limiter_and_change_notifier() {
        {
            let test_handler = TestMessageHandler::new(Some("test".into()));
            let addr = test_handler.start();

            let mut msg_handler = MessageSendHandler::with_limiters(vec![
                Box::new(ChangeDetector::<TestMessage>::new(None)),
                Box::new(RateLimiter {
                    last_msg_sent_at: Instant::now(),
                    hard_rate_limit: Duration::from_millis(50),
                }),
            ]);

            std::thread::sleep(Duration::from_millis(50));

            msg_handler.send_msg("test".into(), |msg| addr.do_send(msg));

            msg_handler.send_msg("test".into(), |msg| addr.do_send(msg)); // will not be received due to change
            msg_handler.send_msg("abc".into(), |msg| addr.do_send(msg)); // will not be received due to rate limit

            let msg_count = addr.send(GetReceivedMessageCount).await.unwrap();
            pretty_assertions::assert_eq!(msg_count, 1);
        }
    }
}
