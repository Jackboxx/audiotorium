use std::fmt::Debug;

pub fn log_msg_received<T, M: Debug>(handler: &T, msg: &M) {
    tracing::info!(
        info = "'Handler received message.",
        msg_type = type_as_str(msg),
        msg_handler = type_as_str(handler),
        ?msg,
    );
}

pub fn dbg_log_msg_received<T, M: Debug>(handler: &T, msg: &M) {
    tracing::debug!(
        info = "'Handler received message.",
        msg_type = type_as_str(msg),
        msg_handler = type_as_str(handler),
        ?msg,
    );
}

fn type_as_str<'a, T: Sized>(value: &T) -> &'a str {
    let type_str = std::any::type_name_of_val(value);
    return type_str.split("::").last().unwrap_or(type_str);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_type_as_str() {
        let input = type_as_str(&"hello");
        pretty_assertions::assert_eq!(input, "&str");

        struct TestStruct;
        let input = type_as_str(&TestStruct);
        pretty_assertions::assert_eq!(input, "TestStruct");
    }
}
