use std::sync::Arc;

#[cfg(feature = "serde-impl")]
use serde::{Deserialize, Serialize};

#[derive(Debug)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "camelCase")
)]
pub struct YoutubeVideoId(Arc<str>);

impl YoutubeVideoId {
    pub fn new(id: impl Into<Arc<str>>) -> Self {
        return Self(id.into());
    }

    #[must_use]
    pub fn id(&self) -> &Arc<str> {
        return &self.0;
    }
}

#[derive(Debug)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "camelCase")
)]
pub struct YoutubePlaylistId(Arc<str>);

impl YoutubePlaylistId {
    pub fn new(id: impl Into<Arc<str>>) -> Self {
        return Self(id.into());
    }

    #[must_use]
    pub fn id(&self) -> &str {
        return self.0.as_ref();
    }
}
