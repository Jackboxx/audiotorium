mod ids;
mod urls;

use std::sync::Arc;
pub use {
    ids::{YoutubePlaylistId, YoutubeVideoId},
    urls::{YoutubePlaylistUrl, YoutubeVideoUrl},
};

#[derive(Debug, Clone)]
pub enum YoutubeContentType {
    Video(YoutubeVideoUrl<Arc<str>>),
    Playlist(YoutubePlaylistUrl<Arc<str>>),
    Invalid { reason: String },
}

pub fn youtube_content_type<'a>(url: impl Into<&'a str>) -> YoutubeContentType {
    let value = url.into();
    return yt_type(value);
}

fn yt_type(value: &str) -> YoutubeContentType {
    match value {
        s if s.starts_with(YoutubeVideoUrl::prefix()) => {
            let url = match YoutubeVideoUrl::try_new(value) {
                Ok(url) => url,
                Err(err) => {
                    return YoutubeContentType::Invalid {
                        reason: err.to_string(),
                    }
                }
            };

            return YoutubeContentType::Video(url);
        }
        s if s.starts_with(YoutubePlaylistUrl::prefix()) => {
            let url = match YoutubePlaylistUrl::try_new(value) {
                Ok(url) => url,
                Err(err) => {
                    return YoutubeContentType::Invalid {
                        reason: err.to_string(),
                    }
                }
            };

            return YoutubeContentType::Playlist(url);
        }
        _ => {
            return YoutubeContentType::Invalid {
                reason: "Not a valid YouTube video or playlist URL".to_owned(),
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_youtube_content_type() {
        matches!(
            youtube_content_type("https://www.youtube.com/watch?v=HYd9B6YvIHM"),
            YoutubeContentType::Video(_)
        );

        matches!(
            youtube_content_type(
                "https://www.youtube.com/watch?v=JogLvpzvn4Q&list=PLGK-2zLAFymBMRyVJCmS2jg8x-P2I4Y-J&index=2"
            ),
            YoutubeContentType::Video(_)
        );

        matches!(
            youtube_content_type(
                "https://www.youtube.com/playlist?list=PLGK-2zLAFymBMRyVJCmS2jg8x-P2I4Y-J"
            ),
            YoutubeContentType::Playlist(_)
        );

        matches!(
            youtube_content_type("https://www.yt.com/watch?v=HYd9B6YvIHM"),
            YoutubeContentType::Invalid { .. }
        );

        matches!(
            youtube_content_type("https://www.youtube.com/short?s=HYd9B6YvIHM"),
            YoutubeContentType::Invalid { .. }
        );
    }
}
