use std::sync::Arc;

#[cfg(feature = "serde-impl")]
use serde::{Deserialize, Serialize};

use crate::identifier::{
    audio_ident::validate::{UrlSchemaEnforcer, UrlSchemaViolation},
    AudioKind, Identifier, ItemUid,
};

use super::ids::{YoutubePlaylistId, YoutubeVideoId};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct YoutubeVideoUrl<T: AsRef<str> + std::fmt::Debug>(T);

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct YoutubePlaylistUrl<T: AsRef<str> + std::fmt::Debug>(T);

impl<T: AsRef<str> + std::fmt::Debug> Identifier for YoutubeVideoUrl<T> {
    fn uid(&self) -> ItemUid<Arc<str>> {
        let prefix = AudioKind::YoutubeAudio.prefix();
        let hex_url = hex::encode(self.as_ref());

        return ItemUid::new(format!("{prefix}{hex_url}").into());
    }
}

impl<T: AsRef<str> + std::fmt::Debug> Identifier for YoutubePlaylistUrl<T> {
    fn uid(&self) -> ItemUid<Arc<str>> {
        let prefix = AudioKind::YoutubeAudioPlaylist.prefix();
        let hex_url = hex::encode(self.as_ref());

        return ItemUid::new(format!("{prefix}{hex_url}").into());
    }
}

impl<T: AsRef<str> + std::fmt::Debug> std::ops::Deref for YoutubeVideoUrl<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        return &self.0;
    }
}

impl<T: AsRef<str> + std::fmt::Debug> std::ops::Deref for YoutubePlaylistUrl<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        return &self.0;
    }
}

impl std::fmt::Display for YoutubeVideoUrl<Arc<str>> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        return write!(f, "{}", self.0);
    }
}

impl std::fmt::Display for YoutubePlaylistUrl<Arc<str>> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        return write!(f, "{}", self.0);
    }
}

impl From<YoutubeVideoId> for YoutubeVideoUrl<Arc<str>> {
    fn from(value: YoutubeVideoId) -> Self {
        let url = format!("{}{}", Self::prefix(), value.id());
        return unsafe { Self::from_raw_unchecked(url) };
    }
}

impl From<YoutubePlaylistId> for YoutubePlaylistUrl<Arc<str>> {
    fn from(value: YoutubePlaylistId) -> Self {
        let url = format!("{}{}", Self::prefix(), value.id());
        return unsafe { Self::from_raw_unchecked(url) };
    }
}

#[cfg(feature = "serde-impl")]
impl Serialize for YoutubeVideoUrl<Arc<str>> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        return self.0.serialize(serializer);
    }
}

#[cfg(feature = "serde-impl")]
impl Serialize for YoutubePlaylistUrl<Arc<str>> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        return self.0.serialize(serializer);
    }
}

#[cfg(feature = "serde-impl")]
impl<'de> Deserialize<'de> for YoutubeVideoUrl<Arc<str>> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let raw_url = Arc::<str>::deserialize(deserializer)?;
        match Self::try_new(raw_url) {
            Ok(url) => return Ok(url),
            Err(parse_err) => return Err(serde::de::Error::custom(parse_err)),
        };
    }
}

#[cfg(feature = "serde-impl")]
impl<'de> Deserialize<'de> for YoutubePlaylistUrl<Arc<str>> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let raw_url = Arc::<str>::deserialize(deserializer)?;
        match Self::try_new(raw_url) {
            Ok(url) => return Ok(url),
            Err(parse_err) => return Err(serde::de::Error::custom(parse_err)),
        };
    }
}

impl YoutubeVideoUrl<Arc<str>> {
    /// Tries to create a valid ``YouTube`` video URL from a string.
    ///
    /// # Errors
    /// - If the URL is not a valid ``YouTube`` video URL
    ///     - Invalid domain
    ///     - Invalid path (not `/watch`)
    ///     - Missing `v` query parameter
    pub fn try_new(value: impl AsRef<str>) -> Result<Self, UrlSchemaViolation> {
        let enforcer = UrlSchemaEnforcer::new(&["youtube.com", "www.youtube.com"])
            .path("/watch")
            .params(&[("v", None)]);
        let url = enforcer.validate(value)?;
        return Ok(Self(Arc::from(url.to_string())));
    }

    /// # Safety
    ///
    /// Does not error when invalid URLs (e.g. `https:::::::/hi :)`) are passed in!
    #[must_use]
    pub unsafe fn from_raw_unchecked(value: impl Into<Arc<str>>) -> Self {
        return Self(value.into());
    }

    #[must_use]
    pub fn into_inner(self) -> Arc<str> {
        return self.0;
    }

    #[must_use]
    pub fn prefix<'a>() -> &'a str {
        return "https://www.youtube.com/watch?v=";
    }

    #[must_use]
    #[allow(clippy::missing_panics_doc)]
    pub fn id(&self) -> &str {
        return self
            .0
            .split_once("v=")
            .expect("all youtube video urls should have a 'v' query parameter")
            .1;
    }
}

impl YoutubePlaylistUrl<Arc<str>> {
    /// Tries to create a valid ``YouTube`` playlist URL from a string.
    ///
    /// # Errors
    /// - If the URL is not a valid ``YouTube`` playlist URL
    ///     - Invalid domain
    ///     - Invalid path (not `/playlist`)
    ///     - Missing `list` query parameter
    pub fn try_new(value: impl AsRef<str>) -> Result<Self, UrlSchemaViolation> {
        let enforcer = UrlSchemaEnforcer::new(&["youtube.com", "www.youtube.com"])
            .path("/playlist")
            .params(&[("list", None)]);
        let url = enforcer.validate(value)?;

        return Ok(Self(Arc::from(url.to_string())));
    }

    /// # Safety
    /// Does not error when invalid URLs (e.g. ``https:::::::/hi :)``) are passed in!
    pub unsafe fn from_raw_unchecked(value: impl Into<Arc<str>>) -> Self {
        return Self(value.into());
    }

    #[must_use]
    pub fn into_inner(self) -> Arc<str> {
        return self.0;
    }

    #[must_use]
    pub fn prefix<'a>() -> &'a str {
        return "https://www.youtube.com/playlist?list=";
    }

    #[must_use]
    /// Gets the value of the ``list`` query parameter.
    ///
    /// # Panics
    /// - If the ``list`` field is not present. This will only happen if the value was created with
    /// ``from_raw_unchecked``
    pub fn id(&self) -> &str {
        return self
            .0
            .split_once("list=")
            .expect("all youtube playlist urls should have a 'list' query parameter")
            .1;
    }
}
