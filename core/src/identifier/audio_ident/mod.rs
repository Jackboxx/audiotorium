use std::sync::Arc;

#[cfg(feature = "serde-impl")]
use serde::{Deserialize, Serialize};

#[cfg(all(feature = "serde-impl", test))]
use ts_rs::TS;

mod validate;
pub mod youtube;

use self::youtube::{youtube_content_type, YoutubeContentType};

use super::{Identifier, ItemUid};

#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "kebab-case")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub enum AudioIdentifier {
    Local {
        #[cfg_attr(all(feature = "serde-impl", test), ts(type = "string"))]
        uid: ItemUid<Arc<str>>,
    },
    Youtube {
        #[cfg_attr(feature = "serde-impl", serde(rename = "url"))]
        raw_url: Arc<str>,
    },
}

#[derive(Debug, PartialEq, Eq, Hash)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "kebab-case")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub enum AudioOrigin {
    Local,
    Youtube,
}

impl AudioIdentifier {
    #[must_use]
    pub fn try_uid(&self) -> Option<ItemUid<Arc<str>>> {
        match self {
            Self::Local { uid } => return Some(uid.clone()),
            Self::Youtube { raw_url: url } => match youtube_content_type(url.as_ref()) {
                YoutubeContentType::Video(url) => return Some(url.uid()),
                YoutubeContentType::Playlist(url) => return Some(url.uid()),
                YoutubeContentType::Invalid { .. } => return None,
            },
        }
    }
}

impl PartialOrd for AudioIdentifier {
    #[allow(clippy::non_canonical_partial_ord_impl)]
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        return Some(self.cmp(other));
    }
}

impl Ord for AudioIdentifier {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match (self, other) {
            (Self::Local { uid: uid1 }, Self::Local { uid: uid2 }) => return uid1.cmp(uid2),
            (Self::Local { .. }, Self::Youtube { .. }) => return std::cmp::Ordering::Greater,

            (Self::Youtube { raw_url: url1 }, Self::Youtube { raw_url: url2 }) => {
                return url1.cmp(url2)
            }
            (Self::Youtube { .. }, Self::Local { .. }) => return std::cmp::Ordering::Less,
        }
    }
}

#[derive(Debug)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "snake_case")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub enum AudioKind {
    YoutubeAudio,
    YoutubeAudioPlaylist,
}

impl AudioKind {
    pub fn from_uid<T: AsRef<str> + std::fmt::Debug>(uid: &ItemUid<T>) -> Option<Self> {
        match uid {
            s if s
                .as_ref()
                .starts_with(AudioKind::YoutubeAudioPlaylist.prefix()) =>
            {
                return Some(AudioKind::YoutubeAudioPlaylist)
            }
            s if s.as_ref().starts_with(AudioKind::YoutubeAudio.prefix()) => {
                return Some(AudioKind::YoutubeAudio)
            }
            _ => return None,
        }
    }

    #[must_use]
    pub fn prefix(&self) -> &str {
        // should match serde snake case serialization/TS exports
        // trailing '_' is okay
        match self {
            Self::YoutubeAudio => return "youtube_audio_",
            Self::YoutubeAudioPlaylist => return "youtube_audio_playlist_",
        }
    }
}
