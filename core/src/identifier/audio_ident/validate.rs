use thiserror::Error;

pub struct UrlSchemaEnforcer<'a> {
    domains: &'a [&'a str],
    schema: &'a str,
    path: &'a str,
    params: &'a [QueryParamWithValidator<'a>],
}

type QueryParamWithValidator<'a> = (
    &'a str,
    Option<fn(&str) -> Result<(), Box<dyn std::error::Error>>>,
);

#[derive(Debug, Error)]
pub enum UrlSchemaViolation {
    #[error(
        "URL schema violated. Incorrect schema\nExpected: '{expected}'\nReceived: '{received}'"
    )]
    InvalidUrlSchema { expected: String, received: String },
    #[error(
        "URL schema violated. Incorrect domain\nExpected one of: [{list}]\nReceived: '{received}'",
        list = expected.iter().map(|str| format!("'{str}'")).collect::<Vec<_>>().join(",")
    )]
    InvalidDomain {
        expected: Vec<String>,
        received: String,
    },
    #[error("URL schema violated. Incorrect path\nExpected: '{expected}'\nReceived: '{received}'")]
    InvalidPath { expected: String, received: String },
    #[error("URL schema violated. Missing query parameter '{key}'")]
    MissingQueryParameter { key: String },
    #[error("URL schema violated. Incorret query parameter value for '{key}'\nReason: {reason}")]
    IncorrectQueryParameterarameter { key: String, reason: String },
    #[error("{0}")]
    InvalidUrlString(#[from] url::ParseError),
}

#[allow(unused)]
impl<'a> UrlSchemaEnforcer<'a> {
    pub fn new(domains: &'a [&'a str]) -> Self {
        return Self {
            domains,
            schema: "https",
            path: "/",
            params: &[],
        };
    }

    pub fn schema(mut self, schema: &'a str) -> Self {
        self.schema = schema;
        return self;
    }

    pub fn path(mut self, path: &'a str) -> Self {
        self.path = path;
        return self;
    }

    pub fn params(mut self, params: &'a [QueryParamWithValidator]) -> Self {
        self.params = params;
        return self;
    }

    pub fn validate(&self, raw_url: impl AsRef<str>) -> Result<url::Url, UrlSchemaViolation> {
        let url = url::Url::parse(raw_url.as_ref())?;

        let schema = url.scheme();
        if schema != self.schema {
            return Err(UrlSchemaViolation::InvalidUrlSchema {
                expected: self.schema.to_owned(),
                received: schema.to_owned(),
            });
        }

        let domain = match url.domain() {
            Some(str) if self.domains.contains(&str) => str,
            _ => {
                return Err(UrlSchemaViolation::InvalidDomain {
                    expected: self.domains.iter().map(ToString::to_string).collect(),
                    received: url.domain().unwrap_or("<no-domain>").to_owned(),
                })
            }
        };

        let path = url.path();
        if path != self.path {
            return Err(UrlSchemaViolation::InvalidPath {
                expected: self.path.to_owned(),
                received: path.to_owned(),
            });
        };

        let mut params = Vec::with_capacity(self.params.len());
        for (key, validate) in self.params {
            let Some(value) = url
                .query_pairs()
                .find_map(|(k, v)| return (k == *key).then_some(v))
            else {
                return Err(UrlSchemaViolation::MissingQueryParameter {
                    key: (*key).to_string(),
                });
            };

            if let Some(validate_fn) = validate {
                if let Err(err) = validate_fn(&value) {
                    return Err(UrlSchemaViolation::IncorrectQueryParameterarameter {
                        key: (*key).to_string(),
                        reason: err.to_string(),
                    });
                }
            }

            params.push((key, value));
        }

        let validated_url = format!("{schema}://{domain}{path}");
        return url::Url::parse_with_params(&validated_url, &params).map_err(Into::into);
    }
}

#[cfg(test)]
mod tests {
    use super::UrlSchemaEnforcer;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_url_schema_enforcer() {
        let enforce = UrlSchemaEnforcer::new(&["youtube.com", "www.youtube.com"])
            .path("/watch")
            .params(&[("v", None)]);

        assert_eq!(
            enforce.validate("https://youtube.com/watch?v=hi").is_ok(),
            true
        );
        assert_eq!(
            dbg!(enforce.validate("https://www.youtube.com/watch?v=hi")).is_ok(),
            true
        );
        assert_eq!(
            enforce
                .validate("https://www.google.com/watch?v=hi")
                .is_err(),
            true
        );
        assert_eq!(enforce.validate("https://youtube.com/watch").is_err(), true);
    }
}
