use std::{
    path::{Path, PathBuf},
    sync::Arc,
};

mod audio_ident;
mod uid;

use url::Url;

#[allow(clippy::module_name_repetitions)]
pub use {
    audio_ident::{
        youtube::{
            youtube_content_type, YoutubeContentType, YoutubePlaylistId, YoutubePlaylistUrl,
            YoutubeVideoId, YoutubeVideoUrl,
        },
        AudioIdentifier, AudioKind, AudioOrigin,
    },
    uid::ItemUid,
};

#[derive(Debug)]
pub enum ItemState {
    DoesNotExist,
    ExistsEverywhere,
    #[cfg(feature = "database")]
    ExistsOnlyOnDisk,
    #[cfg(feature = "database")]
    ExistsOnlyInDatabase,
}

pub trait Identifier {
    fn uid(&self) -> ItemUid<Arc<str>>;

    fn to_path(&self, audio_data_dir: &Path) -> PathBuf {
        return audio_data_dir.join(self.uid().as_ref());
    }

    fn to_path_with_ext(&self, audio_data_dir: &Path) -> PathBuf {
        return self.to_path(audio_data_dir).with_extension("wav");
    }

    #[allow(clippy::missing_errors_doc)]
    fn to_media_url(&self, media_host_url: Url) -> Result<Url, url::ParseError> {
        return media_host_url.join(&format!("media/{uid}", uid = self.uid().as_ref()));
    }

    #[cfg(not(feature = "database"))]
    fn query_state(&self, audio_data_dir: &Path) -> ItemState {
        let path = self.to_path_with_ext(audio_data_dir);
        if path.exists() {
            return ItemState::ExistsEverywhere;
        } else {
            return ItemState::DoesNotExist;
        }
    }

    #[cfg(feature = "database")]
    fn query_state(
        &self,
        audio_data_dir: &Path,
        db_pool: &sqlx::PgPool,
    ) -> impl std::future::Future<Output = Result<ItemState, crate::error::AppError>> {
        return async move {
            use crate::database::select_audio_metadata_from_db;

            let path = self.to_path_with_ext(audio_data_dir);
            let exists_on_disk = path.exists();
            let exists_in_db = select_audio_metadata_from_db(&self.uid(), db_pool)
                .await?
                .is_some();

            let state = match (exists_on_disk, exists_in_db) {
                (true, true) => ItemState::ExistsEverywhere,
                (true, false) => ItemState::ExistsOnlyOnDisk,
                (false, true) => ItemState::ExistsOnlyInDatabase,
                (false, false) => ItemState::DoesNotExist,
            };

            return Ok(state);
        };
    }
}
