use std::sync::Arc;

use super::Identifier;

#[cfg(feature = "serde-impl")]
use serde::{Deserialize, Serialize};

#[derive(Debug, Hash, PartialEq, Eq)]
#[allow(clippy::module_name_repetitions)]
pub struct ItemUid<T>(T)
where
    T: AsRef<str> + std::fmt::Debug;

impl<T> ItemUid<T>
where
    T: AsRef<str> + std::fmt::Debug,
{
    pub fn new(value: T) -> Self {
        return Self(value);
    }
}

impl<T> Identifier for ItemUid<T>
where
    T: AsRef<str> + std::fmt::Debug,
{
    fn uid(&self) -> ItemUid<Arc<str>> {
        return ItemUid(self.0.as_ref().into());
    }
}

impl<T> std::ops::Deref for ItemUid<T>
where
    T: AsRef<str> + std::fmt::Debug,
{
    type Target = T;
    fn deref(&self) -> &Self::Target {
        return &self.0;
    }
}

impl Clone for ItemUid<Arc<str>> {
    fn clone(&self) -> Self {
        return ItemUid(Arc::clone(&self.0));
    }
}

#[cfg(feature = "serde-impl")]
impl Serialize for ItemUid<Arc<str>> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        return self.0.serialize(serializer);
    }
}

#[cfg(feature = "serde-impl")]
impl<'de> Deserialize<'de> for ItemUid<Arc<str>> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        return Ok(Self(Arc::<str>::deserialize(deserializer)?));
    }
}

impl<T> From<T> for ItemUid<Arc<str>>
where
    Arc<str>: From<T>,
{
    fn from(value: T) -> Self {
        return ItemUid(value.into());
    }
}

impl std::fmt::Display for ItemUid<Arc<str>> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        return self.0.fmt(f);
    }
}
