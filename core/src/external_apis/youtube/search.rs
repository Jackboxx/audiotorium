use std::sync::Arc;

use serde::{Deserialize, Serialize};

use crate::{
    error::AppError,
    external_apis::youtube::parse_api_data,
    identifier::{YoutubePlaylistId, YoutubePlaylistUrl, YoutubeVideoId, YoutubeVideoUrl},
};

use super::{
    get_api_data, YoutubeApiDataFetcher, YoutubeApiResponse, YoutubeSearchId, YoutubeSnippet,
};

#[derive(Debug, Serialize, Deserialize)]
pub enum YoutubeSearchResult {
    Video((YoutubeVideoUrl<Arc<str>>, YoutubeSnippet)),
    Playlist((YoutubePlaylistUrl<Arc<str>>, YoutubeSnippet)),
    Invalid { reason: Arc<str> },
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct YoutubeSearchVideoId {
    pub video_id: Arc<str>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct YoutubeSearchPlaylistId {
    pub playlist_id: Arc<str>,
}

impl TryFrom<YoutubeSearchId> for YoutubeVideoId {
    type Error = Arc<str>;
    fn try_from(value: YoutubeSearchId) -> Result<Self, Self::Error> {
        let Some(video_id) = value.video_id.as_deref() else {
            return Err("value of field 'video_id' is 'None', cannot convert youtube id into youtube video id without field 'video_id'".into());
        };

        return Ok(YoutubeVideoId::new(video_id));
    }
}

impl TryFrom<YoutubeSearchId> for YoutubePlaylistId {
    type Error = Arc<str>;
    fn try_from(value: YoutubeSearchId) -> Result<Self, Self::Error> {
        let Some(playlist_id) = value.playlist_id.as_deref() else {
            return Err("value of field 'playlist_id' is 'None', cannot convert youtube id into youtube playlist id without field 'playlist_id'".into());
        };

        return Ok(Self::new(playlist_id));
    }
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
struct RawYoutubeSearchResult {
    pub id: YoutubeSearchId,
    pub snippet: YoutubeSnippet,
}

impl From<RawYoutubeSearchResult> for YoutubeSearchResult {
    fn from(value: RawYoutubeSearchResult) -> Self {
        match value.id.kind.as_ref() {
            "youtube#video" => {
                let id = match YoutubeVideoId::try_from(value.id) {
                    Ok(id) => id,
                    Err(err) => return YoutubeSearchResult::Invalid { reason: err },
                };

                return YoutubeSearchResult::Video((YoutubeVideoUrl::from(id), value.snippet));
            }
            "youtube#playlist" => {
                let id = match YoutubePlaylistId::try_from(value.id) {
                    Ok(id) => id,
                    Err(err) => return YoutubeSearchResult::Invalid { reason: err },
                };

                return YoutubeSearchResult::Playlist((
                    YoutubePlaylistUrl::from(id),
                    value.snippet,
                ));
            }
            _ => {
                return YoutubeSearchResult::Invalid {
                    reason: format!(
                        "invalid kind {} found for youtube search result",
                        value.id.kind
                    )
                    .into(),
                }
            }
        }
    }
}

impl YoutubeApiDataFetcher {
    #[allow(clippy::missing_errors_doc)]
    pub async fn fetch_search_results(
        api_key: &str,
        search: &str,
        limit: usize,
    ) -> Result<YoutubeApiResponse<Vec<YoutubeSearchResult>>, AppError> {
        let allowed_content_types = "video,playlist";
        let api_url =
            format!("https://youtube.googleapis.com/youtube/v3/search?q={search}&part=snippet&type={allowed_content_types}&maxResults={limit}&key={api_key}");

        match get_api_data(&api_url).await? {
            YoutubeApiResponse::Ok(text) => {
                #[derive(Debug, Deserialize)]
                #[serde(rename_all = "camelCase")]
                struct YoutubeSearchResults {
                    items: Vec<RawYoutubeSearchResult>,
                }

                let search_results: YoutubeSearchResults = parse_api_data(&text, &api_url)?;
                let results = search_results
                    .items
                    .into_iter()
                    .map(YoutubeSearchResult::from)
                    .collect();

                Ok(YoutubeApiResponse::Ok(results))
            }
            YoutubeApiResponse::Unauthorized => Ok(YoutubeApiResponse::Unauthorized),
            YoutubeApiResponse::ClientError => Ok(YoutubeApiResponse::ClientError),
        }
    }
}
