use std::sync::Arc;

use serde::Deserialize;

use crate::{
    error::{AppError, AppErrorKind},
    external_apis::youtube::parse_api_data,
    identifier::{Identifier, YoutubeVideoId, YoutubeVideoUrl},
    schema::audio::AudioTrackMetadata,
};

use super::{
    get_api_data, parse_iso_8601_duration, YoutubeApiDataFetcher, YoutubeApiResponse,
    YoutubeSnippet,
};

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
struct YoutubeVideoContentDetails {
    #[serde(rename = "duration")]
    pub duration_iso_8601: Arc<str>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
struct YoutubeVideo {
    id: YoutubeVideoId,
    snippet: YoutubeSnippet,
    content_details: YoutubeVideoContentDetails,
}

impl From<YoutubeVideo> for AudioTrackMetadata {
    fn from(value: YoutubeVideo) -> Self {
        let duration = parse_iso_8601_duration(&value.content_details.duration_iso_8601)
            .and_then(|dur| return dur.try_into().ok());

        return Self {
            identifier: YoutubeVideoUrl::from(value.id).uid(),
            name: Some(value.snippet.title).into(),
            author: Some(value.snippet.channel_title).into(),
            cover_art_url: value
                .snippet
                .thumbnails
                .max_res()
                .map(|max| return max.url.clone())
                .into(),
            duration,
        };
    }
}

impl YoutubeApiDataFetcher {
    #[allow(clippy::missing_errors_doc)]
    pub async fn fetch_audio_metadata(
        api_key: &str,
        url: &YoutubeVideoUrl<Arc<str>>,
    ) -> Result<super::YoutubeApiResponse<AudioTrackMetadata>, AppError> {
        let watch_id = url.id();
        let api_url = format!("https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails&maxResults=1&id={watch_id}&key={api_key}");

        match get_api_data(&api_url).await? {
            YoutubeApiResponse::Ok(text) => {
                #[derive(Debug, Deserialize)]
                #[serde(rename_all = "camelCase")]
                struct YoutubeVideoItems {
                    items: Vec<YoutubeVideo>,
                }

                let videos: YoutubeVideoItems = parse_api_data(&text, &api_url)?;
                let Some(video) = videos.items.into_iter().next() else {
                    return Err(AppError::new(
                        AppErrorKind::Download,
                        "failed to find youtube video",
                        &[&format!("URL: {url:?}")],
                    ));
                };

                Ok(YoutubeApiResponse::Ok(video.into()))
            }
            YoutubeApiResponse::Unauthorized => Ok(YoutubeApiResponse::Unauthorized),
            YoutubeApiResponse::ClientError => Ok(YoutubeApiResponse::ClientError),
        }
    }
}
