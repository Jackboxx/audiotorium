use std::sync::Arc;

use serde::Deserialize;

use crate::{
    error::{AppError, AppErrorKind},
    external_apis::youtube::{parse_api_data, YoutubeVisibility},
    identifier::{
        Identifier, YoutubePlaylistId, YoutubePlaylistUrl, YoutubeVideoId, YoutubeVideoUrl,
    },
    schema::audio::AudioPlaylistMetadata,
};

use super::{get_api_data, YoutubeApiDataFetcher, YoutubeApiResponse, YoutubeSnippet};

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
struct YoutubePlaylist {
    id: YoutubePlaylistId,
    snippet: YoutubeSnippet,
}

impl From<YoutubePlaylist> for AudioPlaylistMetadata {
    fn from(value: YoutubePlaylist) -> Self {
        return Self {
            identifier: YoutubePlaylistUrl::from(value.id).uid(),
            name: Some(value.snippet.title).into(),
            author: Some(value.snippet.channel_title).into(),
            cover_art_url: value
                .snippet
                .thumbnails
                .max_res()
                .map(|max| return max.url.clone())
                .into(),
        };
    }
}

impl YoutubeApiDataFetcher {
    #[allow(clippy::missing_errors_doc)]
    pub async fn fetch_audio_playlist_metadata(
        api_key: &str,
        url: &YoutubePlaylistUrl<Arc<str>>,
    ) -> Result<YoutubeApiResponse<AudioPlaylistMetadata>, AppError> {
        let playlist_id = url.id();
        let api_url = format!("https://youtube.googleapis.com/youtube/v3/playlists?part=snippet&maxResults=1&id={playlist_id}&key={api_key}");
        match get_api_data(&api_url).await? {
            YoutubeApiResponse::Ok(text) => {
                #[derive(Debug, Deserialize)]
                #[serde(rename_all = "camelCase")]
                struct YoutubePlaylistItems {
                    items: Vec<YoutubePlaylist>,
                }

                let playlists: YoutubePlaylistItems = parse_api_data(&text, &api_url)?;

                let Some(playlist) = playlists.items.into_iter().next() else {
                    return Err(AppError::new(
                        AppErrorKind::Api,
                        "failed to find youtube playlist",
                        &[&format!("URL: {url}")],
                    ));
                };

                Ok(YoutubeApiResponse::Ok(playlist.into()))
            }
            YoutubeApiResponse::Unauthorized => Ok(YoutubeApiResponse::Unauthorized),
            YoutubeApiResponse::ClientError => Ok(YoutubeApiResponse::ClientError),
        }
    }

    #[allow(clippy::missing_errors_doc)]
    pub async fn fetch_audio_playlist_urls(
        api_key: &str,
        url: &YoutubePlaylistUrl<Arc<str>>,
    ) -> Result<YoutubeApiResponse<Arc<[YoutubeVideoUrl<Arc<str>>]>>, AppError> {
        let playlist_id = url.id();

        #[derive(Debug, Deserialize)]
        #[serde(rename_all = "camelCase")]
        struct YoutubePlaylistItems {
            items: Vec<YoutubePlaylistItem>,
            next_page_token: Option<String>,
        }

        #[derive(Debug, Deserialize)]
        #[serde(rename_all = "camelCase")]
        struct YoutubePlaylistItem {
            snippet: YoutubePlaylistItemSnippet,
            status: YoutubeVisibility,
        }

        #[derive(Debug, Deserialize)]
        #[serde(rename_all = "camelCase")]
        struct YoutubePlaylistItemSnippet {
            resource_id: YoutubePlaylistItemResourceId,
        }

        #[derive(Debug, Deserialize)]
        #[serde(rename_all = "camelCase")]
        struct YoutubePlaylistItemResourceId {
            video_id: String,
        }

        let base_api_url = format!("https://youtube.googleapis.com/youtube/v3/playlistItems?part=snippet,status&maxResults=50&playlistId={playlist_id}&key={api_key}");

        use YoutubeApiResponse as YAR;

        match get_api_data(&base_api_url).await? {
            YAR::Ok(text) => {
                let mut response: YoutubePlaylistItems = parse_api_data(&text, &base_api_url)?;
                let mut playlist_items = response.items;

                while let Some(token) = response.next_page_token {
                    let api_url = format!("{base_api_url}&pageToken={token}");
                    let text = match get_api_data(&api_url).await? {
                        YAR::Ok(text) => text,
                        YAR::Unauthorized => return Ok(YAR::Unauthorized),
                        YAR::ClientError => return Ok(YAR::ClientError),
                    };

                    response = parse_api_data(&text, &api_url)?;
                    playlist_items.append(&mut response.items);
                }
                let urls = playlist_items
                    .into_iter()
                    .filter_map(|item| {
                        return is_public(&item.status).then_some(YoutubeVideoUrl::from(
                            YoutubeVideoId::new(item.snippet.resource_id.video_id),
                        ));
                    })
                    .collect();

                Ok(YAR::Ok(urls))
            }
            YAR::Unauthorized => Ok(YAR::Unauthorized),
            YAR::ClientError => Ok(YAR::ClientError),
        }
    }
}

fn is_public(status: &YoutubeVisibility) -> bool {
    return status.privacy_status.as_ref() == "public";
}
