use std::sync::Arc;

use reqwest::StatusCode;
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use tracing::warn;

use crate::{
    error::{AppError, AppErrorKind, IntoAppError},
    wrappers::OptionArcStr,
};

mod playlist;
mod search;
mod video;

pub use search::{YoutubeSearchPlaylistId, YoutubeSearchResult, YoutubeSearchVideoId};

pub enum YoutubeApiResponse<T> {
    Ok(T),
    ClientError,
    Unauthorized,
}

pub struct YoutubeApiDataFetcher;

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct YoutubeSearchId {
    kind: Arc<str>,
    video_id: OptionArcStr,
    playlist_id: OptionArcStr,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct YoutubeSnippet {
    pub title: Arc<str>,
    pub channel_title: Arc<str>,
    pub thumbnails: YoutubeThumbnails,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct YoutubeVisibility {
    privacy_status: Arc<str>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct YoutubeThumbnails {
    pub maxres: Option<YoutubeThumbnail>,
    pub high: Option<YoutubeThumbnail>,
    pub medium: Option<YoutubeThumbnail>,
    pub default: Option<YoutubeThumbnail>,
}

impl YoutubeThumbnails {
    #[must_use]
    pub fn max_res(&self) -> Option<&YoutubeThumbnail> {
        return self
            .maxres
            .as_ref()
            .or(self.high.as_ref())
            .or(self.medium.as_ref())
            .or(self.default.as_ref());
    }
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct YoutubeThumbnail {
    pub url: Arc<str>,
}

#[must_use]
pub fn parse_iso_8601_duration(duration_iso_8601: &str) -> Option<u128> {
    return parse_duration::parse(&duration_iso_8601.replace('M', "m"))
        .map(|t| return t.as_millis())
        .ok();
}

async fn get_api_data(url: &str) -> Result<YoutubeApiResponse<String>, AppError> {
    let resp = reqwest::get(url).await.into_app_err(
        "failed to fetch youtube data",
        AppErrorKind::Api,
        &[&format!("URL: {url}")],
    )?;

    let code = resp.status();
    match code {
        StatusCode::UNAUTHORIZED | StatusCode::FORBIDDEN => {
            warn!(
                info = "failed to fetch data from youtube API, unauthorized",
                ?resp
            );
            return Ok(YoutubeApiResponse::Unauthorized);
        }
        _ if code.is_client_error() => {
            warn!(
                info = "failed to fetch data from youtube API, 400 error",
                ?resp
            );
            return Ok(YoutubeApiResponse::ClientError);
        }
        _ => Ok(YoutubeApiResponse::Ok(resp.text().await.into_app_err(
            "failed to fetch youtube data",
            AppErrorKind::Api,
            &[&format!("URL: {url}")],
        )?)),
    }
}

fn parse_api_data<T: DeserializeOwned>(body: &str, url: &str) -> Result<T, AppError> {
    let decoded_body = urlencoding::decode_binary(body.as_bytes());

    return serde_json::from_slice(&decoded_body).into_app_err(
        "failed to parse youtube playlist metadata",
        AppErrorKind::Api,
        &[&format!("URL: {url}"), &format!("RESPONSE_TEXT: {body}")],
    );
}
