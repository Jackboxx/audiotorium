use std::sync::Arc;

#[cfg(feature = "serde-impl")]
use serde::{Deserialize, Serialize};

use uuid::Uuid;

use crate::{identifier::ItemUid, wrappers::OptionArcStr};

#[cfg(all(feature = "serde-impl", test))]
use ts_rs::TS;

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "camelCase")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub struct AudioTrackMetadata {
    #[cfg_attr(all(feature = "serde-impl", test), ts(type = "string"))]
    pub identifier: ItemUid<Arc<str>>,
    pub name: OptionArcStr,
    pub author: OptionArcStr,
    /// TODO: what unit???
    pub duration: Option<i64>,
    pub cover_art_url: OptionArcStr,
}

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "camelCase")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub struct AudioQueueItem {
    #[cfg_attr(feature = "serde-impl", serde(flatten))]
    pub metadata: AudioTrackMetadata,
    #[cfg_attr(all(feature = "serde-impl", test), ts(type = "string"))]
    pub local_id: Uuid,
}

#[derive(Debug, Clone, Default)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "camelCase")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub struct AudioInfoPartial {
    #[cfg_attr(feature = "serde-impl", serde(skip_serializing_if = "Option::is_none"))]
    pub playback_state: Option<PlaybackState>,
    #[cfg_attr(feature = "serde-impl", serde(skip_serializing_if = "Option::is_none"))]
    pub current_queue_index: Option<usize>,
    #[cfg_attr(feature = "serde-impl", serde(skip_serializing_if = "Option::is_none"))]
    pub audio_progress: Option<f64>,
    #[cfg_attr(feature = "serde-impl", serde(skip_serializing_if = "Option::is_none"))]
    pub audio_volume: Option<f32>,
}

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "camelCase")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub struct AudioInfoFull {
    pub playback_state: PlaybackState,
    pub current_queue_index: usize,
    pub audio_progress: f64,
    pub audio_volume: f32,
}

#[derive(Default, Debug, Clone, PartialEq, Eq)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "kebab-case")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub enum PlaybackState {
    #[default]
    Playing,
    Paused,
}

impl Default for AudioInfoFull {
    fn default() -> Self {
        return Self {
            audio_volume: 1.0,
            audio_progress: 0.0,
            current_queue_index: 0,
            playback_state: PlaybackState::default(),
        };
    }
}

impl AudioQueueItem {
    #[must_use]
    pub fn new(metadata: AudioTrackMetadata) -> Self {
        return Self {
            metadata,
            local_id: Uuid::new_v4(),
        };
    }
}

impl AudioInfoPartial {
    #[must_use]
    pub fn diff(&self, other: Self) -> Self {
        return Self {
            playback_state: self
                .playback_state
                .ne(&other.playback_state)
                .then_some(other.playback_state)
                .flatten(),
            current_queue_index: self
                .current_queue_index
                .ne(&other.current_queue_index)
                .then_some(other.current_queue_index)
                .flatten(),
            audio_progress: self
                .audio_progress
                .ne(&other.audio_progress)
                .then_some(other.audio_progress)
                .flatten(),
            audio_volume: self
                .audio_volume
                .ne(&other.audio_volume)
                .then_some(other.audio_volume)
                .flatten(),
        };
    }

    pub fn update(&mut self, other: Self) {
        if other.audio_progress.is_some() && self.audio_progress != other.audio_progress {
            self.audio_progress = other.audio_progress;
        }

        if other.audio_volume.is_some() && self.audio_volume != other.audio_volume {
            self.audio_volume = other.audio_volume;
        }

        if other.playback_state.is_some() && self.playback_state != other.playback_state {
            self.playback_state = other.playback_state;
        }

        if other.current_queue_index.is_some()
            && self.current_queue_index != other.current_queue_index
        {
            self.current_queue_index = other.current_queue_index;
        }
    }

    #[must_use]
    pub fn with_volume(volume: f32) -> Self {
        return Self {
            audio_volume: Some(volume),
            ..Default::default()
        };
    }

    #[must_use]
    pub fn with_progress(progress: f64) -> Self {
        return Self {
            audio_progress: Some(progress),
            ..Default::default()
        };
    }

    #[must_use]
    pub fn with_playback_state(playback_state: PlaybackState) -> Self {
        return Self {
            playback_state: Some(playback_state),
            ..Default::default()
        };
    }

    #[must_use]
    pub fn with_queue_head(queue_head: usize) -> Self {
        return Self {
            current_queue_index: Some(queue_head),
            ..Default::default()
        };
    }
}

impl From<AudioInfoFull> for AudioInfoPartial {
    fn from(
        AudioInfoFull {
            playback_state,
            current_queue_index,
            audio_progress,
            audio_volume,
        }: AudioInfoFull,
    ) -> Self {
        return Self {
            playback_state: Some(playback_state),
            current_queue_index: Some(current_queue_index),
            audio_progress: Some(audio_progress),
            audio_volume: Some(audio_volume),
        };
    }
}
