#[cfg(feature = "serde-impl")]
use serde::{Deserialize, Serialize};

#[cfg(all(feature = "serde-impl", test))]
use ts_rs::TS;

use crate::{identifier::AudioIdentifier, wrappers::OptionArcStr};

#[derive(Debug)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "camelCase")
)]
pub struct SearchAudioResult {
    pub score: f32,
    pub kind: SearchAudioResultKind,

    pub identifier: AudioIdentifier,
    pub name: OptionArcStr,
    pub author: OptionArcStr,
    pub cover_art_url: OptionArcStr,
}

#[derive(Debug)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "kebab-case")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub enum SearchAudioResultKind {
    Track,
    Playlist,
}

impl Ord for SearchAudioResult {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        return self.score.total_cmp(&other.score);
    }
}

impl PartialOrd for SearchAudioResult {
    #[allow(clippy::non_canonical_partial_ord_impl)]
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        return Some(self.cmp(other));
    }
}

impl PartialEq for SearchAudioResult {
    fn eq(&self, other: &Self) -> bool {
        return self.score.eq(&other.score);
    }
}

impl Eq for SearchAudioResult {}
