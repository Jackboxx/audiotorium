use std::sync::Arc;

#[cfg(feature = "serde-impl")]
use serde::{Deserialize, Serialize};

use crate::{identifier::ItemUid, wrappers::OptionArcStr};

#[cfg(all(feature = "serde-impl", test))]
use ts_rs::TS;

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "camelCase")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]

pub struct AudioPlaylistMetadata {
    #[cfg_attr(all(feature = "serde-impl", test), ts(type = "string"))]
    pub identifier: ItemUid<Arc<str>>,
    pub name: OptionArcStr,
    pub author: OptionArcStr,
    pub cover_art_url: OptionArcStr,
}
