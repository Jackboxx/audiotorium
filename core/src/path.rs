use std::{
    fs,
    path::{Path, PathBuf},
};

/// Creates all directories defined in `core`.
///
/// # Errors
/// - IO error
pub fn init() -> Result<(), std::io::Error> {
    fs::create_dir_all(base())?;
    fs::create_dir_all(data_shared())?;
    fs::create_dir_all(data_shared_audio())?;

    return Ok(());
}

/// Base directory shared by all crates.
#[must_use]
pub fn base() -> PathBuf {
    let dir = std::env::var("XDG_DATA_HOME")
        .unwrap_or_else(|_| format!("{}/.local/share", std::env!("HOME")));

    return Path::new(&dir).join("audiotorium");
}

#[must_use]
pub fn data_shared_audio() -> PathBuf {
    return data_shared().join("audio");
}

#[must_use]
pub fn data_shared() -> PathBuf {
    return base().join("data");
}
