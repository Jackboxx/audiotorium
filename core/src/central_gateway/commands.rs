#[cfg(feature = "serde-impl")]
use serde::{Deserialize, Serialize};

#[cfg(feature = "actix-impl")]
use {crate::error::AppError, actix::Message};

#[cfg(all(feature = "serde-impl", test))]
use ts_rs::TS;

use crate::identifier::AudioIdentifier;

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "SCREAMING_SNAKE_CASE")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
#[cfg_attr(feature = "actix-impl", derive(Message))]
#[cfg_attr(feature = "actix-impl", rtype(result = "Result<(), AppError>"))]
pub enum CentralGatewayCommand {
    DownloadAudio(DownloadAudioParams),
}

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "camelCase")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub struct DownloadAudioParams {
    pub identifier: AudioIdentifier,
}
