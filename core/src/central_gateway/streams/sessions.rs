#[cfg(feature = "serde-impl")]
use serde::{Deserialize, Serialize};

#[cfg(all(feature = "serde-impl", test))]
use ts_rs::TS;

use crate::{node::AudioNodeInfo, wrappers::ArcSlice};

use super::RunningDownload;

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "SCREAMING_SNAKE_CASE")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub enum CentralGatewaySessionConnectedMessage {
    #[cfg_attr(feature = "serde-impl", serde(rename_all = "camelCase"))]
    SessionConnectedResponse {
        node_info: Option<ArcSlice<AudioNodeInfo>>,
        downloads: Option<ArcSlice<RunningDownload>>,
    },
}
