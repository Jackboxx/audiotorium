use std::sync::Arc;

#[cfg(feature = "serde-impl")]
use serde::{Deserialize, Serialize};

#[cfg(feature = "clap-impl")]
use clap::ValueEnum;

#[cfg(all(feature = "serde-impl", test))]
use ts_rs::TS;

#[cfg(feature = "actix-impl")]
use actix::Message;

use crate::{
    download::{DownloadClientView, DownloadState},
    node::AudioNodeInfo,
};

#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "SCREAMING_SNAKE_CASE")
)]
#[cfg_attr(feature = "clap-impl", derive(ValueEnum))]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub enum CentralGatewayInfoStreamType {
    NodeInfo,
    Downloads,
}

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "SCREAMING_SNAKE_CASE")
)]
#[cfg_attr(feature = "actix-impl", derive(Message))]
#[cfg_attr(feature = "actix-impl", rtype(result = "()"))]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub enum CentralGatewayInfoStreamMessage {
    NodeInfo(Arc<[AudioNodeInfo]>),
    Downloads(Arc<[RunningDownload]>),
}

#[derive(Debug, Clone)]
#[cfg_attr(
    feature = "serde-impl",
    derive(Serialize, Deserialize),
    serde(rename_all = "camelCase")
)]
#[cfg_attr(
    all(feature = "serde-impl", test),
    derive(TS),
    ts(export, export_to = "../webapp/src/api-types/")
)]
pub struct RunningDownload {
    pub state: DownloadState,
    pub view: DownloadClientView,
}

impl CentralGatewayInfoStreamMessage {
    #[must_use]
    pub fn msg_type(&self) -> CentralGatewayInfoStreamType {
        match self {
            CentralGatewayInfoStreamMessage::NodeInfo(_) => {
                return CentralGatewayInfoStreamType::NodeInfo
            }
            CentralGatewayInfoStreamMessage::Downloads(_) => {
                return CentralGatewayInfoStreamType::Downloads
            }
        };
    }
}
