#![cfg(feature = "events")]

use std::{
    collections::VecDeque,
    fmt, fs, io,
    num::NonZeroUsize,
    path::{Path, PathBuf},
};

use actix::{dev::SendError, Actor, Context, Handler, Message, Recipient};
use serde::{de::DeserializeOwned, Serialize};

/// Globally unique and constant identifier for an event queue.
pub type EventQueueUniqueId = u64;

pub type PersistFunc<Event> = dyn Fn(EventQueueUniqueId, &[Event], &Path) -> Result<(), io::Error>;

pub struct EventQueue<Event>
where
    Event: Serialize + DeserializeOwned + fmt::Debug + Send + Clone + Unpin + 'static,
{
    id: EventQueueUniqueId,
    event_dir: PathBuf,
    events: VecDeque<Event>,
    changes_since_last_persist: usize,
    polling: Option<Recipient<EventQueuePop<Event>>>,
    persist_strat: PersistStrategy,
    persist_fn: Box<PersistFunc<Event>>,
}

#[derive(Debug, Message)]
#[rtype(result = "()")]
pub enum EventQueueMessage<Event>
where
    Event: Serialize + DeserializeOwned + fmt::Debug + Send + Clone + Unpin + 'static,
{
    Push(Event),
    Pop(Recipient<EventQueuePop<Event>>),
    Poll(Recipient<EventQueuePop<Event>>),
    PresistToDisk,
}

#[derive(Debug, Message)]
#[rtype(result = "()")]
pub struct EventQueuePop<Event>
where
    Event: Serialize + DeserializeOwned + fmt::Debug + Send + Clone + Unpin + 'static,
{
    inner: Event,
}

#[derive(Debug)]
pub enum PersistStrategy {
    ManualOnly,
    NthMessage(NonZeroUsize),
    EveryMessage,
}

impl<Event> Actor for EventQueue<Event>
where
    Event: Serialize + DeserializeOwned + fmt::Debug + Send + Clone + Unpin + 'static,
{
    type Context = Context<Self>;

    fn started(&mut self, _ctx: &mut Self::Context) {
        tracing::info!("Started event queue[id = {}]", self.id);
        tracing::debug!(info = format!("Info for event queue[id = {}]", self.id), ?self.event_dir, ?self.persist_strat, ?self.events);
    }
}

impl<Event> Handler<EventQueueMessage<Event>> for EventQueue<Event>
where
    Event: Serialize + DeserializeOwned + fmt::Debug + Send + Clone + Unpin + 'static,
{
    type Result = ();

    #[allow(clippy::too_many_lines)]
    fn handle(&mut self, msg: EventQueueMessage<Event>, _ctx: &mut Self::Context) -> Self::Result {
        let mut notify_pollers = false;
        match msg {
            EventQueueMessage::Push(event) => {
                self.changes_since_last_persist += 1;
                self.events.push_back(event);

                notify_pollers = true;
            }
            EventQueueMessage::Pop(recip) => {
                let res = pop(self, &recip);

                if let PopResult::FailedToSend(err) = res {
                    tracing::error!(
                        info = "Failed to send popped event to recipient.",
                        ?recip,
                        %self.id,
                        %err
                    );
                }
            }
            EventQueueMessage::Poll(recip) => match pop(self, &recip) {
                PopResult::Success => {}
                PopResult::QueueEmpty => {
                    self.polling = Some(recip);
                }
                PopResult::FailedToSend(err) => {
                    tracing::error!(
                        info = "Failed to send popped event to recipient.",
                        ?recip,
                        %self.id,
                        %err
                    );
                }
            },
            EventQueueMessage::PresistToDisk => {
                let res = self.persist();
                if let Err(err) = res {
                    tracing::error!(
                        info = "Failed to presist event queue to disk",
                        cause = "manual",
                        %self.id,
                        ?self.events,
                        %err
                    );
                }
            }
        }

        let res = self.persist_auto();
        if let Err(err) = res {
            tracing::error!(
                info = "Failed to presist event queue to disk",
                cause = "automatic",
                %self.id,
                ?self.events,
                %err
            );
        }

        let recip_if_should_poll = (notify_pollers)
            .then(|| {
                return self.polling.take();
            })
            .flatten();

        if let Some(recip) = recip_if_should_poll {
            let res = pop(self, &recip);

            if let PopResult::FailedToSend(err) = res {
                tracing::error!(
                    info = "Failed to send popped event to polling recipient.",
                    ?recip,
                    %self.id,
                    %err
                );
            }
        }

        #[derive(Debug)]
        enum PopResult<Event>
        where
            Event: Serialize + DeserializeOwned + fmt::Debug + Send + Clone + Unpin + 'static,
        {
            Success,
            QueueEmpty,
            FailedToSend(SendError<EventQueuePop<Event>>),
        }

        fn pop<Event>(
            queue: &mut EventQueue<Event>,
            recip: &Recipient<EventQueuePop<Event>>,
        ) -> PopResult<Event>
        where
            Event: Serialize + DeserializeOwned + fmt::Debug + Send + Clone + Unpin + 'static,
        {
            match queue.events.pop_front() {
                Some(event) => {
                    queue.changes_since_last_persist += 1;
                    let res = recip.try_send(EventQueuePop {
                        inner: event.clone(),
                    });

                    match res {
                        Ok(()) => {
                            return PopResult::Success;
                        }
                        Err(err) => {
                            queue.events.push_front(event);
                            return PopResult::FailedToSend(err);
                        }
                    }
                }
                None => {
                    return PopResult::QueueEmpty;
                }
            }
        }
    }
}

impl<Event> EventQueue<Event>
where
    Event: Serialize + DeserializeOwned + fmt::Debug + Send + Clone + Unpin + 'static,
{
    /// Create a new event queue that will persist events to disk based on the provided
    /// `PersistStrategy`.
    ///
    /// # Errors
    /// - If an IO error (other than not found) occurs when trying to load previously persisted
    /// events from disk
    pub fn new(
        id: EventQueueUniqueId,
        event_dir: PathBuf,
        strat: PersistStrategy,
    ) -> Result<Self, io::Error> {
        let events = Self::recover_from_disk(id, &event_dir)?;
        return Ok(Self {
            id,
            event_dir,
            events,
            polling: None,
            changes_since_last_persist: 0,
            persist_strat: strat,
            persist_fn: Box::new(Self::default_persist_fn),
        });
    }

    /// Create a new event queue that will persist events to disk based on the provided
    /// `PersistStrategy`. Events are persisted by calling the custom `persist_fn`.
    ///
    /// # Errors
    /// - If an IO error (other than not found) occurs when trying to load previously persisted
    /// events from disk
    pub fn with_custom_persist(
        id: EventQueueUniqueId,
        event_dir: PathBuf,
        strat: PersistStrategy,
        persist_fn: Box<PersistFunc<Event>>,
    ) -> Result<Self, io::Error> {
        let events = Self::recover_from_disk(id, &event_dir)?;
        return Ok(Self {
            id,
            event_dir,
            events,
            polling: None,
            changes_since_last_persist: 0,
            persist_strat: strat,
            persist_fn: Box::new(persist_fn),
        });
    }

    /// Convert the event id into a valid file path (0-padded 32digit hex string).
    /// No mangling is performed.
    #[must_use]
    pub fn id_to_file_name(id: EventQueueUniqueId) -> String {
        return format!("{id:032x}");
    }

    /// Persist current events to disk.
    ///
    /// # Errors
    ///
    /// - If writing to disk fails
    fn persist(&mut self) -> Result<(), io::Error> {
        self.changes_since_last_persist = 0;

        return (self.persist_fn)(self.id, self.events.as_slices().0, &self.event_dir);
    }

    /// Persist current events to disk depending on conditions determined by the
    /// `PersistStrategy`.
    ///
    /// # Errors
    ///
    /// - If a persist is triggered, but writing to disk fails
    fn persist_auto(&mut self) -> Result<(), io::Error> {
        let should_persist = match self.persist_strat {
            PersistStrategy::NthMessage(n) if self.changes_since_last_persist >= n.into() => true,
            PersistStrategy::ManualOnly | PersistStrategy::NthMessage(_) => false,
            PersistStrategy::EveryMessage => true,
        };

        if should_persist {
            self.persist()?;
        }

        return Ok(());
    }

    /// Recover events that were previously persisted to disk. If no file that matches the given
    /// `id` is found, an empty queue is returned.
    ///
    /// # Errors
    ///
    /// - If an IO error, other than `NotFound`, is encountered
    fn recover_from_disk(
        id: EventQueueUniqueId,
        event_dir: &Path,
    ) -> Result<VecDeque<Event>, io::Error> {
        let path = event_dir.join(Self::id_to_file_name(id));
        let bytes = match fs::read(path) {
            Ok(bytes) => bytes,
            Err(err) if err.kind() == io::ErrorKind::NotFound => {
                return Ok(VecDeque::new());
            }
            Err(err) => return Err(err),
        };

        let events = match bincode::deserialize(&bytes) {
            Ok(events) => events,
            Err(err) => {
                let err_io = io::Error::new(io::ErrorKind::InvalidData, err);
                return Err(err_io);
            }
        };

        return Ok(events);
    }

    fn default_persist_fn(
        id: EventQueueUniqueId,
        events: &[Event],
        event_dir: &Path,
    ) -> Result<(), io::Error> {
        let path = event_dir.join(Self::id_to_file_name(id));
        let bytes = match bincode::serialize(events) {
            Ok(events) => events,
            Err(err) => {
                let err_io = io::Error::new(io::ErrorKind::InvalidData, err);
                return Err(err_io);
            }
        };

        return fs::write(path, bytes);
    }
}

impl<Event> EventQueuePop<Event>
where
    Event: Serialize + DeserializeOwned + fmt::Debug + Send + Clone + Unpin + 'static,
{
    pub fn into_inner(self) -> Event {
        return self.inner;
    }
}

#[cfg(test)]
mod tests {
    use std::sync::{Arc, Mutex};

    use serde::{Deserialize, Serialize};

    use super::*;

    #[derive(Debug, Clone, Serialize, Deserialize)]
    struct TestEvent {
        data: i32,
    }

    struct TestEventPopper {
        expected: i32,
    }

    impl Actor for TestEventPopper {
        type Context = Context<Self>;
    }

    impl Handler<EventQueuePop<TestEvent>> for TestEventPopper {
        type Result = ();

        fn handle(
            &mut self,
            msg: EventQueuePop<TestEvent>,
            _ctx: &mut Self::Context,
        ) -> Self::Result {
            assert_eq!(self.expected, msg.into_inner().data);
            self.expected += 1;
        }
    }

    #[actix::test]
    async fn test_event_queue() {
        let four = NonZeroUsize::new(4).unwrap();
        let expected = Arc::new(Mutex::new(4));
        let expected_clone = expected.clone();

        let tester = TestEventPopper { expected: 0 };
        let queue = EventQueue::<TestEvent>::with_custom_persist(
            0,
            Path::new("").to_path_buf(),
            PersistStrategy::NthMessage(four),
            Box::new(move |_, events, _| {
                let mut expected = expected_clone.lock().unwrap();
                assert_eq!(*expected, events.len());

                *expected = 3;

                return Ok(());
            }),
        )
        .unwrap();

        let addr_queue = queue.start();
        let addr_tester = tester.start();

        let events = [
            TestEvent { data: 0 },
            TestEvent { data: 1 },
            TestEvent { data: 2 },
            TestEvent { data: 3 },
            TestEvent { data: 4 },
        ];

        for event in events {
            addr_queue
                .send(EventQueueMessage::Push(event))
                .await
                .unwrap();
        }

        addr_queue
            .send(EventQueueMessage::Pop(addr_tester.clone().recipient()))
            .await
            .unwrap();

        addr_queue
            .send(EventQueueMessage::Pop(addr_tester.recipient()))
            .await
            .unwrap();

        assert_eq!(*expected.lock().unwrap(), 3);
    }
}
