#![deny(clippy::implicit_return)]
#![deny(clippy::pedantic)]
#![allow(clippy::needless_return)]
#![allow(clippy::module_name_repetitions)]
#![allow(clippy::items_after_statements)]

pub mod central_gateway;
pub mod database;
pub mod download;
pub mod error;
pub mod events;
pub mod external_apis;
pub mod identifier;
pub mod logging_utils;
pub mod msg_utils;
pub mod node;
pub mod parsing;
pub mod path;
pub mod relay;
pub mod schema;
pub mod tags;
pub mod tcp_messaging;
pub mod wrappers;
