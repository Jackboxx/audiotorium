#![cfg(feature = "serde-impl")]

use core::fmt;
use std::{collections::HashSet, sync::Arc};

use serde::de::{self, IntoDeserializer};

#[allow(clippy::missing_errors_doc)]
pub fn deserialize_stringified_list<'de, D, I>(
    deserializer: D,
) -> std::result::Result<Arc<[I]>, D::Error>
where
    D: de::Deserializer<'de>,
    I: de::DeserializeOwned,
{
    struct StringVecVisitor<I>(std::marker::PhantomData<I>);

    impl<'de, I> de::Visitor<'de> for StringVecVisitor<I>
    where
        I: de::DeserializeOwned,
    {
        type Value = Vec<I>;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            return formatter.write_str("a string containing a list");
        }

        fn visit_str<E>(self, v: &str) -> std::result::Result<Self::Value, E>
        where
            E: de::Error,
        {
            let mut ids = Vec::new();
            for id in v.split(',') {
                let id = I::deserialize(id.into_deserializer())?;
                ids.push(id);
            }
            return Ok(ids);
        }
    }

    return deserializer
        .deserialize_any(StringVecVisitor(std::marker::PhantomData::<I>))
        .map(|vec| return vec.into());
}

#[allow(clippy::missing_errors_doc)]
pub fn deserialize_stringified_set<'de, D, I>(
    deserializer: D,
) -> std::result::Result<HashSet<I>, D::Error>
where
    D: de::Deserializer<'de>,
    I: de::DeserializeOwned + std::hash::Hash + std::cmp::Eq,
{
    struct StringVecVisitor<I>(std::marker::PhantomData<I>);

    impl<'de, I> de::Visitor<'de> for StringVecVisitor<I>
    where
        I: de::DeserializeOwned,
    {
        type Value = Vec<I>;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            return formatter.write_str("a string containing a list");
        }

        fn visit_str<E>(self, v: &str) -> std::result::Result<Self::Value, E>
        where
            E: de::Error,
        {
            let mut ids = Vec::new();
            for id in v.split(',') {
                let id = I::deserialize(id.into_deserializer())?;
                ids.push(id);
            }
            return Ok(ids);
        }
    }

    return deserializer
        .deserialize_any(StringVecVisitor(std::marker::PhantomData::<I>))
        .map(|vec| return HashSet::from_iter(vec));
}
