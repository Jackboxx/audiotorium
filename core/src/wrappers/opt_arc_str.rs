use std::{ops::Deref, sync::Arc};

#[cfg(feature = "serde-impl")]
use serde::{Deserialize, Serialize};

#[cfg(all(feature = "serde-impl", test))]
use ts_rs::TS;

#[derive(Debug)]
pub struct OptionArcStr {
    inner: Option<Arc<str>>,
}

impl Clone for OptionArcStr {
    fn clone(&self) -> Self {
        return Self {
            inner: self.inner.clone(),
        };
    }
}

impl Deref for OptionArcStr {
    type Target = Option<Arc<str>>;

    fn deref(&self) -> &Self::Target {
        return &self.inner;
    }
}

impl From<OptionArcStr> for Option<Arc<str>> {
    fn from(value: OptionArcStr) -> Self {
        return value.inner;
    }
}

impl<ArcStr: Into<Arc<str>>> From<Option<ArcStr>> for OptionArcStr {
    fn from(value: Option<ArcStr>) -> Self {
        return Self {
            inner: value.map(Into::into),
        };
    }
}

#[cfg(feature = "serde-impl")]
impl Serialize for OptionArcStr {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        match &self.inner {
            Some(str) => return serializer.serialize_str(str),
            None => return serializer.serialize_none(),
        }
    }
}

#[cfg(feature = "serde-impl")]
impl<'de> Deserialize<'de> for OptionArcStr {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let data = Option::<Arc<str>>::deserialize(deserializer)?;
        return Ok(Self { inner: data });
    }
}

#[cfg(all(feature = "serde-impl", test))]
impl TS for OptionArcStr {
    fn name() -> String {
        return "string | null".to_string();
    }

    fn dependencies() -> Vec<ts_rs::Dependency>
    where
        Self: 'static,
    {
        return vec![];
    }

    fn transparent() -> bool {
        return false;
    }
}

impl OptionArcStr {
    #[must_use]
    pub fn inner_as_ref(&self) -> Option<&str> {
        return self.inner.as_ref().map(|val| return val.as_ref());
    }
}

#[cfg(all(feature = "serde-impl", test))]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_serialize_opt_arc_str() {
        let opt_none = OptionArcStr { inner: None };
        let opt_some = OptionArcStr {
            inner: Some("something".into()),
        };

        let opt_none_serial = serde_json::to_string(&opt_none).unwrap();
        let opt_some_serial = serde_json::to_string(&opt_some).unwrap();

        assert_eq!(opt_none_serial, "null");
        assert_eq!(opt_some_serial, r#""something""#);

        let opt_none_deserial: OptionArcStr = serde_json::from_str(&opt_none_serial).unwrap();
        let opt_some_deserial: OptionArcStr = serde_json::from_str(&opt_some_serial).unwrap();

        assert_eq!(opt_none_deserial.inner, None);
        assert_eq!(opt_some_deserial.inner, Some("something".into()));
    }
}
