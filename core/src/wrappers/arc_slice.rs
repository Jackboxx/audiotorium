use std::sync::Arc;

#[cfg(feature = "serde-impl")]
use serde::{Deserialize, Serialize};

#[cfg(test)]
use ts_rs::{Dependency, TS};

#[derive(Debug, Clone)]
pub struct ArcSlice<T>(Arc<[T]>);

impl<T> From<Arc<[T]>> for ArcSlice<T> {
    fn from(value: Arc<[T]>) -> Self {
        return ArcSlice(value);
    }
}

impl<T> std::ops::Deref for ArcSlice<T> {
    type Target = [T];
    fn deref(&self) -> &Self::Target {
        return &self.0;
    }
}

impl<V> FromIterator<V> for ArcSlice<V> {
    fn from_iter<T: IntoIterator<Item = V>>(iter: T) -> Self {
        return iter.into_iter().collect::<Arc<[V]>>().into();
    }
}

#[cfg(feature = "serde-impl")]
impl<T: Serialize> Serialize for ArcSlice<T> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        return self.0.serialize(serializer);
    }
}

#[cfg(feature = "serde-impl")]
impl<'de, T: Deserialize<'de>> Deserialize<'de> for ArcSlice<T> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let v = Vec::<T>::deserialize(deserializer)?;
        return Ok(Self(v.into()));
    }
}

#[cfg(test)]
impl<T: TS> TS for ArcSlice<T> {
    fn name() -> String {
        return "Array".to_owned();
    }

    fn dependencies() -> Vec<Dependency>
    where
        Self: 'static,
    {
        return T::dependencies();
    }

    fn transparent() -> bool {
        return true;
    }
}
