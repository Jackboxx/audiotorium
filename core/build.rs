use std::path::Path;

fn main() {
    if cfg!(feature = "database") {
        let sqlx_offline =
            std::env::var("SQLX_OFFLINE").is_ok_and(|is_offline| is_offline == "true");

        if !sqlx_offline {
            let url = std::env::var("DATABASE_URL").unwrap_or_else(|_| {
                let out_dir = std::env::var("OUT_DIR").unwrap();
                let currently_building_pkg_path = out_dir.split_once("target").unwrap();
                let env_path = Path::new(currently_building_pkg_path.0).join(".env");
                dotenv::from_path(env_path).ok();

                dotenv::var("DATABASE_URL").unwrap_or_else(|_| {
                    panic!("DATABASE_URL should in .env file or in set as environment variable")
                })
            });

            println!("cargo:rustc-env=DATABASE_URL={url}");
        }
    }
}
