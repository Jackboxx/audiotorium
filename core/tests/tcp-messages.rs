#![cfg(feature = "actor-tcp")]

use std::net::{Ipv4Addr, SocketAddr};

use async_trait::async_trait;
use audiotorium_core::tcp_messaging::{
    TcpMessage, TcpMessageGateway, TcpMessageHandler, TcpMessageSender,
};
use pretty_assertions::assert_eq;
use rand::{distributions::Alphanumeric, Rng};
use serde::{Deserialize, Serialize};
use tokio::sync::Mutex;

#[derive(Debug)]
struct MessageHandlerEcho {
    hits: usize,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
struct MessageEcho {
    field_1: Option<i64>,
    field_2: String,
    field_3: Vec<f32>,
}

impl TcpMessage for MessageEcho {
    type Response = Self;

    const TAG: u32 = 0x1;
}

#[async_trait]
impl TcpMessageHandler<MessageEcho> for MessageHandlerEcho {
    async fn handle(&mut self, msg: MessageEcho) -> MessageEcho {
        self.hits += 1;

        return msg;
    }
}

#[tokio::test]
async fn test_msg_round_trip() {
    let iterations = 500;

    let port = get_available_port().unwrap();
    let addr = SocketAddr::new(std::net::IpAddr::V4(Ipv4Addr::LOCALHOST), port);

    let mut gateway = TcpMessageGateway::new(addr);
    let handler = std::sync::Arc::new(Mutex::new(MessageHandlerEcho { hits: 0 }));
    gateway.bind(&handler).await;
    let handle = tokio::task::spawn(async move {
        gateway.listen().await.unwrap();
    });

    tokio::time::sleep(std::time::Duration::from_millis(200)).await;

    for _ in 0..iterations {
        let str = rand::thread_rng()
            .sample_iter(&Alphanumeric)
            .take(rand::thread_rng().gen_range(0..256))
            .map(|x| x as char)
            .collect();

        let nums = vec![0_f32; rand::thread_rng().gen_range(0..512)]
            .into_iter()
            .map(|_| rand::random())
            .collect();

        let msg = MessageEcho {
            field_1: rand::random(),
            field_2: str,
            field_3: nums,
        };

        let echo_response = addr.send(&msg).await.unwrap();
        assert_eq!(
            msg, echo_response,
            "echo tcp message handler didn't returned the same message it was sent"
        );
    }

    handle.abort_handle().abort();
    assert_eq!(handler.try_lock().unwrap().hits, iterations);
}

fn get_available_port() -> Option<u16> {
    (1024..u16::MAX).find(|port| port_is_available(*port))
}

fn port_is_available(port: u16) -> bool {
    use std::net::TcpListener;
    TcpListener::bind(("127.0.0.1", port)).is_ok()
}
