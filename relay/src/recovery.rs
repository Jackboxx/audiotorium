use std::time::Duration;

use actix::{AsyncContext, Handler, Message};
use audiotorium_core::node::AudioNodeHealth;

use crate::audio_playback::audio_player::AudioPlayerAction;

use super::{node_server::AudioNode, processor_communication::AudioProcessorToNodeMessage};

const INITIAL_DEVICE_RECOVERY_ATTEMPT_INTERVAL: Duration = Duration::from_secs(2);
const MAX_DEVICE_RECOVERY_ATTEMPT_INTERVAL: Duration = Duration::from_secs(16);

/// Used to try and recover connections with devices. Mainly intended to be used with bluetooth
/// devices.
///
/// On successful recovery playback resumes at the current queue head index and at the audio
/// progress that was last saved by the server (this should be the same as the last audio progress
/// that was sent to any client).
#[derive(Debug, Clone, Message)]
#[rtype(result = "()")]
pub struct TryRecoverDevice {
    wait_since_last_attempt: Option<Duration>,
}

#[derive(Debug, Clone, Message)]
#[rtype(result = "()")]
pub struct RecoverDeviceResult {
    success: bool,
    wait_since_last_attempt: Option<Duration>,
}

impl RecoverDeviceResult {
    pub fn new(success: bool, wait_since_last_attempt: Option<Duration>) -> Self {
        return Self {
            success,
            wait_since_last_attempt,
        };
    }
}

impl Handler<TryRecoverDevice> for AudioNode {
    type Result = ();

    #[allow(clippy::collapsible_else_if)]
    fn handle(&mut self, msg: TryRecoverDevice, _ctx: &mut Self::Context) -> Self::Result {
        match self.health {
            AudioNodeHealth::Good => {}
            _ => {
                self.player.r#do(AudioPlayerAction::TryRecoverDevice {
                    progress_before_device_error: self.audio_info.audio_progress,
                    wait_since_last_attempt: msg.wait_since_last_attempt,
                });
            }
        };
    }
}

impl Handler<RecoverDeviceResult> for AudioNode {
    type Result = ();

    fn handle(&mut self, msg: RecoverDeviceResult, ctx: &mut Self::Context) -> Self::Result {
        match msg.success {
            true => {
                let snd_msg_rsp = ctx
                    .address()
                    .try_send(AudioProcessorToNodeMessage::Health(AudioNodeHealth::Good));

                if let Err(err) = snd_msg_rsp {
                    tracing::error!(
                        info = "Failed to inform node of device recovery.",
                        %self.source_name,
                        ?err,
                    )
                };
            }
            false => {
                let wait_time = msg
                    .wait_since_last_attempt
                    .unwrap_or(INITIAL_DEVICE_RECOVERY_ATTEMPT_INTERVAL)
                    .min(MAX_DEVICE_RECOVERY_ATTEMPT_INTERVAL);

                ctx.notify_later(msg, wait_time);
            }
        };
    }
}
