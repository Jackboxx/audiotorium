#![allow(clippy::needless_return)]
#![deny(clippy::implicit_return)]

use std::{collections::HashMap, path::Path, sync::Arc};

use anyhow::anyhow;
use audiotorium_core::{
    identifier::ItemUid,
    relay::{DeviceId, PlaybackAction},
    schema::audio::{AudioQueueItem, AudioTrackMetadata},
};
use cpal::{
    traits::{DeviceTrait, HostTrait},
    SampleRate,
};
use ctx::Ctx;
use serde::Deserialize;

mod audio_playback;
mod ctx;
mod env;
mod view;
// mod recovery;

#[derive(Debug)]
struct Relay {
    devices: HashMap<DeviceId, AudioPlaybackDevice>,
}

#[derive(Debug)]
struct AudioPlaybackDevice {
    display_name: Arc<str>,
    channels: u32,
    sample_rate: u32,
    internals: DeviceInternals,
}

enum DeviceInternals {
    Alsa {
        device: cpal::Device,
        cfg: cpal::StreamConfig,
    },
}

impl std::fmt::Debug for DeviceInternals {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            DeviceInternals::Alsa { device, cfg } => {
                return write!(
                    f,
                    r"DeviceInternals (ALSA) {{
    name: {name},
    channels: {channels},
    sample_rate: {sample_rate},
    buffer_size: {buffer_size:?},
}}",
                    name = device.name().as_deref().unwrap_or("<Failed to get name.>"),
                    channels = cfg.channels,
                    sample_rate = cfg.sample_rate.0,
                    buffer_size = cfg.buffer_size
                );
            }
        }
    }
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "kebab-case")]
struct RelayConfig {
    device_configs: HashMap<DeviceId, DeviceConfig>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "kebab-case")]
struct DeviceConfig {
    display_name: Arc<str>,
    kind: DeviceKind,
    channels: Option<u32>,
    sample_rate: Option<u32>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "kebab-case")]
enum DeviceKind {
    Alsa,
}

impl DeviceConfig {
    const DEFAULT_CHANNEL_COUNT: u32 = 2;
    const DEFAULT_SAMPLE_RATE: u32 = 48_000;
}

impl RelayConfig {
    fn load_or_default(path: &Path) -> Self {
        let cfg_from_file = || -> anyhow::Result<_> {
            let source_str = std::fs::read_to_string(path)?;
            let cfg = toml::from_str::<RelayConfig>(&source_str)?;
            return Ok(cfg);
        }();

        match cfg_from_file {
            Ok(cfg) => return cfg,
            Err(err) => {
                tracing::warn!("Failed to load config from path {path:#?}, using default config instead. {err}");
                return RelayConfig::default();
            }
        }
    }
}

pub fn setup_device(id: &DeviceId, cfg: DeviceConfig) -> anyhow::Result<AudioPlaybackDevice> {
    let channels = cfg.channels.unwrap_or(DeviceConfig::DEFAULT_CHANNEL_COUNT);
    let sample_rate = cfg.channels.unwrap_or(DeviceConfig::DEFAULT_SAMPLE_RATE);

    match (id, cfg.kind) {
        (DeviceId::Alsa { name }, DeviceKind::Alsa) => {
            let host = cpal::default_host();
            let device_internal = host
                .output_devices()?
                .find(|dev| {
                    return dev
                        .name()
                        .map(|dev_name| return *dev_name == **name)
                        .unwrap_or(false);
                })
                .ok_or(anyhow!("No device with source name {name} found"))?;

            let mut supported_configs_range = device_internal.supported_output_configs()?;
            let supported_config = supported_configs_range.next().ok_or(anyhow!("TODO"))?;

            let cfg_internal = supported_config
                .with_sample_rate(SampleRate(sample_rate * channels))
                .into();

            let device = AudioPlaybackDevice {
                display_name: cfg.display_name.clone(),
                channels,
                sample_rate,
                internals: DeviceInternals::Alsa {
                    device: device_internal,
                    cfg: cfg_internal,
                },
            };

            return Ok(device);
        }
    }
}

impl Default for RelayConfig {
    fn default() -> Self {
        return RelayConfig {
            device_configs: HashMap::from_iter([(
                DeviceId::Alsa {
                    name: "default".into(),
                },
                DeviceConfig {
                    display_name: "Default".into(),
                    kind: DeviceKind::Alsa,
                    channels: None,
                    sample_rate: None,
                },
            )]),
        };
    }
}

#[tokio::main]
async fn main() {
    Ctx::init().unwrap();

    let cfg = RelayConfig::load_or_default(Path::new("/nope.cfg"));
    for (id, cfg_device) in cfg.device_configs {
        Ctx::get()
            .try_send(ctx::CtxMsg::TryRegisterDevice((id.clone(), cfg_device)))
            .unwrap();

        let item = AudioQueueItem::new(AudioTrackMetadata {
            identifier: ItemUid::new("example".into()),
            name: Some("Heat Waves").into(),
            author: Some("Glass Animals").into(),
            duration: None,
            cover_art_url: None::<&str>.into(),
        });

        Ctx::get()
            .try_send(ctx::CtxMsg::DoDevicePlaybackAction((
                id,
                PlaybackAction::PushToQueue { item },
            )))
            .unwrap()
    }

    loop {
        std::thread::yield_now();
    }

    // TODO:
    // - load & validate config / create default
    // - start HTTP & Websocket server
}
