use std::sync::{
    atomic::{AtomicBool, Ordering},
    Arc,
};

use audiotorium_core::relay::{DeviceId, PlaybackAction};
use cpal::traits::{DeviceTrait, StreamTrait};

use crate::{
    audio_playback::audio_item::load_item,
    ctx::{
        Ctx,
        CtxMsg::{self, DoDevicePlaybackAction},
        DeviceChangeNotification,
    },
    setup_device,
    view::{View, ViewMut, ViewReadonly},
    DeviceConfig, DeviceInternals,
};

use super::{
    processor::{AudioProcessorMessage, AudioStreamProcessor, AudioStreamState},
    rb::{RBProducer, RingBuffer},
    PlaybackInfo,
};

use anyhow::Context;
use audiotorium_core::{
    identifier::ItemUid,
    schema::audio::{AudioInfoFull, AudioQueueItem, PlaybackState},
};
use cpal::{Device, Stream, StreamConfig};

/// [`AudioPlayer`] manages the "top-level" aspects of audio playback for a specific audio device
/// such as speakers connected via ALSA. "top-level" aspects include the current [`QueueState`]
/// (queue items, currently playing item index, ...) and device health & device recovery. The
/// playback of audio through a specific device is controlled by the [`AudioStreamProcessor`].
pub struct AudioPlayer {
    id: DeviceId,
    device: Device,
    config: StreamConfig,
    current_stream: Option<Stream>,
    pub(super) queue: ViewMut<QueueInfo>,
    volume: ViewMut<f32>,
    playback: ViewMut<Option<ViewReadonly<PlaybackInfo>>>,
    /// Used to receive async updates from other parts of the application.
    player_rx: tokio::sync::mpsc::Receiver<PlaybackAction>,
    /// Used to communicate with the [`AudioStreamProcessor`].
    processor_msg_buffer: Option<RBProducer<AudioProcessorMessage>>,
    pub(super) err_stopped_playback: bool,
}

#[derive(Debug)]
pub struct QueueInfo {
    pub(super) items: Vec<AudioQueueItem>,
    pub(super) head: usize,
}

/// TODO
pub struct AudioPlayerNewOut {
    pub playback_action_tx: tokio::sync::mpsc::Sender<PlaybackAction>,
    pub queue_info: ViewReadonly<QueueInfo>,
    pub playback_info: ViewReadonly<Option<ViewReadonly<PlaybackInfo>>>,
    pub volume: ViewReadonly<f32>,
}

impl AudioPlayer {
    pub async fn run(mut self) {
        // self.restore_state(restored_state).await;

        while let Some(action) = self.player_rx.recv().await {
            let res = do_action(&mut self, action).await;
            match res {
                Ok(ntfy) => {
                    let res = Ctx::get()
                        .send(CtxMsg::NotifyChangedDeviceInfo((self.id.clone(), ntfy)))
                        .await;
                    if let Err(err) = res {
                        tracing::error!(info = "Failed to send context update message.", ?err);
                    }
                }
                Err(err) => {
                    tracing::error!(info="Error while performing playback action", ?err, ?self.id);
                }
            }
        }

        async fn do_action(
            player: &mut AudioPlayer,
            action: PlaybackAction,
        ) -> anyhow::Result<DeviceChangeNotification> {
            match action {
                PlaybackAction::PlayNext => {
                    player
                        .play_next()
                        .await
                        .context("Failed to play next audio.")?;

                    return Ok(DeviceChangeNotification::QueueHead);
                }
                PlaybackAction::PlayPrev => {
                    player
                        .play_prev()
                        .await
                        .context("Failed to play previous audio.")?;

                    return Ok(DeviceChangeNotification::QueueHead);
                }
                PlaybackAction::PlaySelected {
                    idx,
                    allow_self_select,
                } => {
                    player
                        .play_selected(idx, allow_self_select)
                        .await
                        .with_context(|| {
                            return format!("Failed to play selected audio. [idx={idx}]");
                        })?;

                    return Ok(DeviceChangeNotification::QueueHead);
                }
                PlaybackAction::SetPlaybackState { state } => {
                    player.set_stream_playback_state(state.clone());

                    return Ok(DeviceChangeNotification::PlaybackState);
                }
                PlaybackAction::SetProgress { progress } => {
                    player.set_stream_progress(progress);

                    return Ok(DeviceChangeNotification::PlaybackProgress);
                }
                PlaybackAction::SetVolume { volume } => {
                    player.set_volume(volume);

                    return Ok(DeviceChangeNotification::Volume);
                }
                PlaybackAction::PushToQueue { item } => {
                    player
                        .push_to_queue(item)
                        .await
                        .context("Failed to push new item to the queue.")?;

                    return Ok(DeviceChangeNotification::Queue);
                }
                PlaybackAction::RemoveFromQueue { identifier } => {
                    player
                        .remove_from_queue(&identifier)
                        .await
                        .with_context(|| {
                            return format!(
                                "Failed to remove item from queue. [identifier={identifier}]"
                            );
                        })?;

                    return Ok(DeviceChangeNotification::Queue);
                }
                PlaybackAction::MoveQueueItems {
                    local_id_current,
                    idx_current,
                    local_id_new,
                    idx_new,
                } => {
                    player.move_queue_item(&local_id_current, idx_current, &local_id_new, idx_new)
                        .with_context(|| {
                            return format!("Failed to swap queue item at position {local_id_current} with item at position {local_id_new}. [current_id={local_id_current}, target_id={local_id_new}]")
                        })?;

                    return Ok(DeviceChangeNotification::Queue);
                }
                PlaybackAction::ShuffleQueue => {
                    player
                        .shuffle_queue()
                        .await
                        .context("Failed to shuffle queue.")?;

                    return Ok(DeviceChangeNotification::Queue);
                }
                PlaybackAction::ClearQueue => {
                    player
                        .clear_queue()
                        .await
                        .context("Failed to clear queue.")?;

                    return Ok(DeviceChangeNotification::Queue);
                } // PlaybackAction::TryRecoverDevice { progress_before_device_error, wait_since_last_attempt } => {
                  //     player.try_recover_device(progress_before_device_error, wait_since_last_attempt).await.with_context(|| {
                  //         return "Failed to recover device."
                  //     }).map(|()| {
                  //         return Some(AudioNodeInfoStreamMessage::Health(AudioNodeHealth::Good));
                  //     })
                  // }
            };
        }
    }

    pub fn try_new(
        id: DeviceId,
        cfg: DeviceConfig,
        restored_state: AudioInfoFull,
        restored_queue: Vec<AudioQueueItem>,
    ) -> anyhow::Result<AudioPlayerNewOut> {
        let playback_device = setup_device(&id, cfg)?;
        let DeviceInternals::Alsa { device, cfg } = playback_device.internals;

        let (player_tx, player_rx) = tokio::sync::mpsc::channel(256);

        let queue = QueueInfo {
            items: restored_queue,
            head: restored_state.current_queue_index,
        };

        let (queue, queue_readonly) = View::new(queue);
        let (playback, playback_readonly) = View::new::<Option<ViewReadonly<PlaybackInfo>>>(None);
        let (volume, volume_readonly) = View::new(restored_state.audio_volume);

        let player = AudioPlayer {
            id,
            device,
            config: cfg,
            current_stream: None,
            queue,
            volume,
            playback,
            player_rx,
            processor_msg_buffer: None,
            err_stopped_playback: false,
        };

        tokio::task::spawn_local(async move {
            let res = tokio::task::spawn_local(async move {
                player.run().await;
            })
            .await;

            if let Err(err) = res {
                tracing::error!(
                    info = "Failed to start playing audio on device. Thread error.",
                    ?err
                );
            }
        });

        return Ok(AudioPlayerNewOut {
            playback_action_tx: player_tx,
            queue_info: queue_readonly,
            playback_info: playback_readonly,
            volume: volume_readonly,
        });
    }

    // async fn try_recover_device(
    //     &mut self,
    //     current_progress: f64,
    //     wait_since_last_attempt: Option<Duration>,
    // ) -> anyhow::Result<()> {
    //     let (device, config) = match setup_device(&self.source_name) {
    //         Ok(recovered) => {
    //             if let Some(addr) = &self.node_addr {
    //                 addr.do_send(RecoverDeviceResult::new(true, wait_since_last_attempt));
    //             }
    //
    //             recovered
    //         }
    //         Err(err) => {
    //             if let Some(addr) = &self.node_addr {
    //                 addr.do_send(RecoverDeviceResult::new(false, wait_since_last_attempt));
    //             }
    //
    //             return Err(err);
    //         }
    //     };
    //     self.device = device;
    //     self.config = config;
    //
    //     let queue_head = self
    //         .queue
    //         .read()
    //         .expect("Audio player has paniced while holding rw lock.")
    //         .queue_head;
    //
    //     self.play_selected(queue_head, true).await?;
    //     self.set_stream_progress(current_progress);
    //
    //     return Ok(());
    // }

    fn get_identifier(&self) -> Option<ItemUid<Arc<str>>> {
        let head = self.queue.view().head;
        return self
            .queue
            .view()
            .items
            .get(head)
            .map(|audio| return audio.metadata.identifier.clone());
    }

    pub fn queue_items_cloned(&self) -> Vec<AudioQueueItem> {
        return self.queue.view().items.clone();
    }

    pub fn queue_head(&self) -> usize {
        return self.queue.view().head;
    }

    pub(super) fn update_queue_head(&mut self, value: usize) {
        self.queue.get_mut().head = value;
    }

    // async fn restore_state(&mut self, info: AudioInfoFull) {
    //     self.update_queue_head(info.current_queue_index);
    //
    //     if let Some(identifier) = self.get_identifier() {
    //         if let Err(err) = self.play(&identifier).await {
    //             tracing::error!(info = "Failed to play audio after restore.", ?err);
    //         }
    //
    //         self.set_volume(info.audio_volume);
    //         self.set_stream_progress(info.audio_progress);
    //         self.set_stream_playback_state(info.playback_state);
    //     }
    // }
}

impl std::fmt::Debug for AudioPlayer {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        return write!(f, "TODO");
    }
}

impl AudioPlayer {
    pub(super) async fn play_next(&mut self) -> anyhow::Result<()> {
        if self.queue.view().items.is_empty() {
            self.current_stream = None;
            return Ok(());
        }

        let queue_head_new = self.queue.view().head + 1;
        self.update_queue_head(queue_head_new);

        if self.queue.view().head >= self.queue.view().items.len() {
            self.update_queue_head(0);
        }

        if let Some(identifier) = self.get_identifier() {
            self.play(&identifier).await?;
        }

        return Ok(());
    }

    pub(super) async fn play_prev(&mut self) -> anyhow::Result<()> {
        if self.queue.view().items.is_empty() {
            self.current_stream = None;
            return Ok(());
        }

        let prev_head = self
            .queue
            .view()
            .head
            .checked_sub(1)
            .unwrap_or(self.queue.view().items.len() - 1);
        self.update_queue_head(prev_head);

        if let Some(identifier) = self.get_identifier() {
            self.play(&identifier).await?;
        }

        return Ok(());
    }

    pub(super) async fn play_selected(
        &mut self,
        index: usize,
        allow_self_select: bool,
    ) -> anyhow::Result<()> {
        if self.queue.view().items.is_empty() {
            self.current_stream = None;
            return Ok(());
        }

        if index == self.queue.view().head && !allow_self_select {
            return Ok(());
        }

        let new_head_pos = index.clamp(0, self.queue.view().items.len() - 1);
        self.update_queue_head(new_head_pos);

        if let Some(identifier) = self.get_identifier() {
            self.play(&identifier).await?;
        }

        return Ok(());
    }

    pub(super) fn set_stream_playback_state(&mut self, state: PlaybackState) {
        if let Some(buffer) = self.processor_msg_buffer.as_mut() {
            buffer.push(AudioProcessorMessage::SetState(state));
        }
    }

    // progress is clamped between `0.0` and `1.0`
    pub(super) fn set_stream_progress(&mut self, progress: f64) {
        let progress = progress.clamp(0.0, 1.0);
        if let Some(buffer) = self.processor_msg_buffer.as_mut() {
            buffer.push(AudioProcessorMessage::SetProgress(progress));
        }
    }

    pub(super) fn set_volume(&mut self, volume: f32) {
        *self.volume.get_mut() = volume.clamp(0.0, 1.0);
    }

    pub(super) async fn play(&mut self, identifier: &ItemUid<Arc<str>>) -> anyhow::Result<()> {
        let audio_stream = match load_item(identifier).await {
            Ok(x) => x,
            Err(err) => {
                self.err_stopped_playback = true;
                return Err(err.into());
            }
        };

        // prevent bluez-alsa from throwing error 'device busy' by removing the stream accessing
        // the bluetooth device before creating a new stream
        self.current_stream = None;

        let (producer, consumer) = RingBuffer::<AudioProcessorMessage>::new();
        self.processor_msg_buffer = Some(producer);

        let (mut processor, info_readonly) =
            AudioStreamProcessor::new(consumer, Some(audio_stream), self.volume.readonly());

        *self.playback.get_mut() = Some(info_readonly);

        // let mut msg_handler = MessageSendHandler::with_limiters(vec![
        //     Box::new(ChangeDetector::<AudioProcessorToNodeMessage>::new(Some(
        //         AudioProcessorToNodeMessage::Health(AudioNodeHealth::Good),
        //     ))),
        //     Box::<RateLimiter>::default(),
        // ]);
        //
        // let mut msg_handler_for_err = MessageSendHandler::with_limiters(vec![
        //     Box::new(ChangeDetector::<AudioProcessorToNodeMessage>::new(Some(
        //         AudioProcessorToNodeMessage::Health(AudioNodeHealth::Good),
        //     ))),
        //     Box::<RateLimiter>::default(),
        // ]);

        let fatal_err_in_stream = Arc::new(AtomicBool::new(false));
        let fatal_err_in_stream_clone = Arc::clone(&fatal_err_in_stream);

        let device_id = self.id.clone();

        let new_stream_res = self.device.build_output_stream(
            &self.config,
            move |data: &mut [f32], _| {
                if fatal_err_in_stream.load(Ordering::Relaxed) {
                    return;
                };

                match processor.try_process(data) {
                    Ok(state) => match state {
                        AudioStreamState::Finished => {
                            processor.read_disk_stream = None;

                            let res = Ctx::get().try_send(DoDevicePlaybackAction((
                                device_id.clone(),
                                PlaybackAction::PlayNext,
                            )));

                            if let Err(err) = res {
                                tracing::error!(info = "Failed to play next audio in queue.", ?err);
                            }
                        }
                        AudioStreamState::Buffering => {
                            // let msg = AudioProcessorToNodeMessage::Health(AudioNodeHealth::Mild(
                            //     AudioNodeHealthMild::Buffering,
                            // ));
                            //
                            // if let Some(addr) = processor.node_addr.as_ref() {
                            //     msg_handler.send_msg(msg, addr);
                            // }
                        }
                        AudioStreamState::Playing => {
                            //     let msg =
                            //         AudioProcessorToNodeMessage::AudioStateInfo(processor.info.clone());
                            //
                            //     if let Some(addr) = processor.node_addr.as_ref() {
                            //         msg_handler.send_msg(msg, addr);
                            //     }
                        }
                    },
                    Err(err) => {
                        tracing::error!(info = "Failed to process audio stream.", ?err);

                        // let msg = AudioProcessorToNodeMessage::Health(AudioNodeHealth::Poor(
                        //     AudioNodeHealthPoor::AudioStreamReadFailed,
                        // ));
                        //
                        // if let Some(addr) = processor.node_addr.as_ref() {
                        //     msg_handler.send_msg(msg, addr);
                        // }
                    }
                };
            },
            move |err| {
                if fatal_err_in_stream_clone.load(Ordering::Relaxed) {
                    return;
                }

                tracing::error!(info = "Failed to process audio stream.", ?err);
                // let msg = match err {
                //     StreamError::DeviceNotAvailable => AudioProcessorToNodeMessage::Health(
                //         AudioNodeHealth::Poor(AudioNodeHealthPoor::DeviceNotAvailable),
                //     ),
                //
                //     StreamError::BackendSpecific { err } => {
                //         AudioProcessorToNodeMessage::Health(AudioNodeHealth::Poor(
                //             AudioNodeHealthPoor::AudioBackendError(err.description),
                //         ))
                //     }
                // };

                // if let Some(addr) = addr_for_err.as_ref() {
                //     msg_handler_for_err.send_msg(msg, addr);
                // }

                fatal_err_in_stream_clone.store(true, Ordering::SeqCst);
            },
            None,
        );

        let new_stream = match new_stream_res {
            Ok(x) => x,
            Err(err) => {
                self.err_stopped_playback = true;
                return Err(err.into());
            }
        };

        match new_stream.play() {
            Ok(()) => {}
            Err(err) => {
                self.err_stopped_playback = true;
                return Err(err.into());
            }
        };

        self.err_stopped_playback = false;
        self.current_stream = Some(new_stream);
        return Ok(());
    }
}
