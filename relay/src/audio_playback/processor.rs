use audiotorium_core::schema::audio::PlaybackState;
use creek::{read::ReadError, ReadDiskStream, SymphoniaDecoder};

use crate::view::{View, ViewMut, ViewReadonly};

use super::rb::RBConsumer;

pub(super) struct AudioStreamProcessor {
    pub(super) msg_buffer: RBConsumer<AudioProcessorMessage>,
    pub(super) read_disk_stream: Option<ReadDiskStream<SymphoniaDecoder>>,
    pub(super) had_cache_miss_last_cycle: bool,
    pub(super) info: ViewMut<PlaybackInfo>,
    volume: ViewReadonly<f32>,
}

#[derive(Debug, Clone)]
pub(super) enum AudioProcessorMessage {
    SetState(PlaybackState),
    SetProgress(f64),
}

#[derive(Debug, Default, Clone, PartialEq)]
pub struct PlaybackInfo {
    pub(super) playback_state: PlaybackState,
    pub(super) audio_progress: f64,
}

#[derive(Debug)]
pub(super) enum AudioStreamState {
    Playing,
    Buffering,
    Finished,
}

impl PlaybackInfo {
    pub fn new() -> Self {
        return Self {
            audio_progress: Default::default(),
            playback_state: Default::default(),
        };
    }

    pub fn playback_state(&self) -> &PlaybackState {
        return &self.playback_state;
    }

    pub fn audio_progress(&self) -> f64 {
        return self.audio_progress;
    }
}
impl AudioStreamProcessor {
    pub(super) fn new(
        msg_buffer: RBConsumer<AudioProcessorMessage>,
        read_disk_stream: Option<ReadDiskStream<SymphoniaDecoder>>,
        volume: ViewReadonly<f32>,
    ) -> (Self, ViewReadonly<PlaybackInfo>) {
        let (info, info_readonly) = View::new(PlaybackInfo::default());
        let this = Self {
            msg_buffer,
            read_disk_stream,
            had_cache_miss_last_cycle: false,
            info,
            volume,
        };

        return (this, info_readonly);
    }

    pub(super) fn try_process(
        &mut self,
        mut data: &mut [f32],
    ) -> Result<AudioStreamState, ReadError<symphonia_core::errors::Error>> {
        let mut cache_missed_this_cycle = false;
        let mut stream_state = AudioStreamState::Playing;

        while let Some(msg) = self.msg_buffer.pop() {
            match msg {
                AudioProcessorMessage::SetState(state) => {
                    self.info.get_mut().playback_state = state
                }
                AudioProcessorMessage::SetProgress(percentage) => {
                    if let Some(read_disk_stream) = &mut self.read_disk_stream {
                        let num_frames = read_disk_stream.info().num_frames;
                        let seek_frame = (num_frames as f64 * percentage) as usize;
                        if let Ok(cache_found) =
                            read_disk_stream.seek(seek_frame, creek::SeekMode::Auto)
                        {
                            if !cache_found {
                                stream_state = AudioStreamState::Buffering;
                            }

                            self.info.get_mut().audio_progress = percentage;
                        }
                    }
                }
            }
        }

        if let Some(read_disk_stream) = &mut self.read_disk_stream {
            if self.info.get_mut().playback_state == PlaybackState::Paused {
                silence(data);
                return Ok(AudioStreamState::Playing);
            }

            if !read_disk_stream.is_ready().unwrap_or(false) {
                stream_state = AudioStreamState::Buffering;
                cache_missed_this_cycle = true;
            }

            let num_frames = read_disk_stream.info().num_frames;
            let num_channels = usize::from(read_disk_stream.info().num_channels);

            let vol = *self.volume.view();
            while data.len() >= num_channels {
                let read_frames = data.len() / 2;
                let mut playhead = read_disk_stream.playhead();

                let read_data = read_disk_stream.read(read_frames)?;
                playhead += read_data.num_frames();

                if playhead >= num_frames {
                    let to_end_of_loop = read_data.num_frames() - (playhead - num_frames);

                    if read_data.num_channels() == 1 {
                        let ch = read_data.read_channel(0);

                        for i in 0..to_end_of_loop {
                            data[i * 2] = ch[i] * vol;
                            data[(i * 2) + 1] = ch[i] * vol;
                        }
                    } else if read_data.num_channels() == 2 {
                        let ch1 = read_data.read_channel(0);
                        let ch2 = read_data.read_channel(1);

                        for i in 0..to_end_of_loop {
                            data[i * 2] = ch1[i] * vol;
                            data[(i * 2) + 1] = ch2[i] * vol;
                        }
                    }

                    data = &mut data[to_end_of_loop * 2..];

                    stream_state = AudioStreamState::Finished;
                    break;
                } else {
                    if read_data.num_channels() == 1 {
                        let ch = read_data.read_channel(0);

                        for i in 0..read_data.num_frames() {
                            data[i * 2] = ch[i] * vol;
                            data[(i * 2) + 1] = ch[i] * vol;
                        }
                    } else if read_data.num_channels() == 2 {
                        let ch1 = read_data.read_channel(0);
                        let ch2 = read_data.read_channel(1);

                        for i in 0..read_data.num_frames() {
                            data[i * 2] = ch1[i] * vol;
                            data[(i * 2) + 1] = ch2[i] * vol;
                        }
                    }

                    data = &mut data[read_data.num_frames() * 2..];

                    stream_state = AudioStreamState::Playing;
                }

                self.info.get_mut().audio_progress = playhead as f64 / num_frames as f64;
            }
        } else {
            silence(data);
        }

        // When the cache misses, the buffer is filled with silence. So the next
        // buffer after the cache miss is starting from silence. To avoid an audible
        // pop, apply a ramping gain from 0 up to unity.
        if self.had_cache_miss_last_cycle {
            let buffer_size = data.len() as f32;
            for (i, sample) in data.iter_mut().enumerate() {
                *sample *= i as f32 / buffer_size;
            }
        }

        self.had_cache_miss_last_cycle = cache_missed_this_cycle;
        return Ok(stream_state);
    }
}

fn silence(data: &mut [f32]) {
    for sample in data.iter_mut() {
        *sample = 0.0;
    }
}
