use std::{io, net::IpAddr, path::PathBuf, sync::Arc};

use audiotorium_core::{
    download::MediaUrl,
    identifier::{Identifier, ItemUid},
    path::data_shared_audio,
    schema::audio::AudioQueueItem,
};
use creek::{OpenError, ReadDiskStream, SymphoniaDecoder};
use uuid::Uuid;

use crate::env::envs;

pub(super) async fn load_item(
    id: &ItemUid<Arc<str>>,
) -> Result<ReadDiskStream<SymphoniaDecoder>, OpenError> {
    let path = id.to_path_with_ext(&data_shared_audio());

    if path.exists() {
        return load_item_from_path(&path);
    }

    let host = envs().downloader_host;
    let port = envs().downloader_http_port;
    let url = MediaUrl::new(id.clone(), IpAddr::V4(host), port);

    return load_item_from_url(&url).await;
}

fn load_item_from_path(path: &PathBuf) -> Result<ReadDiskStream<SymphoniaDecoder>, OpenError> {
    return ReadDiskStream::<SymphoniaDecoder>::new(path, 0, Default::default());
}

#[tracing::instrument]
async fn load_item_from_url(
    media_url: &MediaUrl,
) -> Result<ReadDiskStream<SymphoniaDecoder>, OpenError> {
    let response = match reqwest::get(media_url.url().as_str())
        .await
        .and_then(reqwest::Response::error_for_status)
    {
        Ok(x) => x,
        Err(err) => {
            return Err(custom_open_err(format!(
                "Failed to download audio file.\n{err}"
            )));
        }
    };

    let file_path = media_url.id().to_path_with_ext(&data_shared_audio());
    let mut file = match std::fs::File::create(&file_path) {
        Ok(x) => x,
        Err(err) => {
            return Err(custom_open_err(format!(
                "Failed to create audio file at {file_path:#?}.\n{err}"
            )));
        }
    };
    let bytes = match response.bytes().await {
        Ok(x) => x,
        Err(err) => {
            return Err(custom_open_err(format!(
                "Failed to read download response bytes.\n{err}"
            )));
        }
    };

    let mut content = io::Cursor::new(bytes);
    match io::copy(&mut content, &mut file) {
        Ok(_) => {}
        Err(err) => {
            return Err(custom_open_err(format!(
                "Failed to write download response bytes to file at {file_path:#?}.\n{err}"
            )));
        }
    };

    return load_item_from_path(&file_path);

    fn custom_open_err(err: impl AsRef<str>) -> OpenError {
        return OpenError::Io(io::Error::new(io::ErrorKind::Other, err.as_ref()));
    }
}

pub trait IdentifiableQueueItem {
    fn identifier(&self) -> Uuid;
}

impl IdentifiableQueueItem for AudioQueueItem {
    fn identifier(&self) -> Uuid {
        return self.local_id;
    }
}
