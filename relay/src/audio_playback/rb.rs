use std::{
    collections::VecDeque,
    marker::PhantomData,
    sync::{Arc, Mutex},
};

/// Very basic single-threaded/sync ring buffer implementation. Only supports push/pop
/// operations.
#[derive(Debug)]
pub(super) struct RingBuffer<T> {
    phantom: PhantomData<T>,
}

#[derive(Debug)]
pub(super) struct RBConsumer<T> {
    buf: Arc<Mutex<VecDeque<T>>>,
}

#[derive(Debug)]
pub(super) struct RBProducer<T> {
    buf: Arc<Mutex<VecDeque<T>>>,
}

impl<T> RingBuffer<T> {
    #[allow(clippy::new_ret_no_self)]
    pub fn new() -> (RBProducer<T>, RBConsumer<T>) {
        let buf = Arc::new(Mutex::new(VecDeque::new()));

        let p = RBProducer { buf: buf.clone() };
        let c = RBConsumer { buf };

        return (p, c);
    }
}

impl<T> RBConsumer<T> {
    pub fn pop(&mut self) -> Option<T> {
        let mut buf = self.buf.lock().expect(
            "Single threaded RingBuffer shouldn't be used in a context that allows lock poisoning.",
        );

        return buf.pop_front();
    }
}

impl<T> RBProducer<T> {
    pub fn push(&mut self, item: T) {
        let mut buf = self.buf.lock().expect(
            "Single threaded RingBuffer shouldn't be used in a context that allows lock poisoning.",
        );

        return buf.push_back(item);
    }
}
