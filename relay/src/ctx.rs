use std::{collections::HashMap, sync::OnceLock};

use anyhow::{anyhow, Context};
use audiotorium_core::{
    relay::{DeviceId, PlaybackAction},
    schema::audio::AudioInfoFull,
};

use crate::{
    audio_playback::{AudioPlayer, AudioPlayerNewOut, PlaybackInfo, QueueInfo},
    view::ViewReadonly,
    DeviceConfig,
};

static CTX: OnceLock<tokio::sync::mpsc::Sender<CtxMsg>> = OnceLock::new();

/// TODO
#[derive(Debug)]
pub struct Ctx {
    channels: CtxChannels,
    views: CtxViews,
}

/// TODO
#[derive(Debug, Default)]
struct CtxChannels {
    playback_channels: HashMap<DeviceId, PlaybackChannel>,
}

/// TODO
#[derive(Debug, Default)]
struct CtxViews {
    device_state: HashMap<DeviceId, DeviceView>,
}

#[derive(Debug)]
struct DeviceView {
    queue: ViewReadonly<QueueInfo>,
    playback: ViewReadonly<Option<ViewReadonly<PlaybackInfo>>>,
    volume: ViewReadonly<f32>,
}

#[derive(Debug)]
struct PlaybackChannel {
    tx: tokio::sync::mpsc::Sender<PlaybackAction>,
}

/// TODO
#[derive(Debug)]
pub enum CtxMsg {
    TryRegisterDevice((DeviceId, DeviceConfig)),
    DoDevicePlaybackAction((DeviceId, PlaybackAction)),
    NotifyChangedDeviceInfo((DeviceId, DeviceChangeNotification)),
}

/// TODO
#[derive(Debug)]
pub enum DeviceChangeNotification {
    QueueHead,
    Queue,
    Volume,
    PlaybackState,
    PlaybackProgress,
}

impl Ctx {
    pub fn get() -> &'static tokio::sync::mpsc::Sender<CtxMsg> {
        return CTX.get().expect("CTX must be set.");
    }

    /// TODO
    pub fn init() -> anyhow::Result<()> {
        let mut ctx = Ctx {
            channels: CtxChannels::default(),
            views: CtxViews::default(),
        };

        let (tx, mut rx) = tokio::sync::mpsc::channel::<CtxMsg>(128);

        let rt = tokio::runtime::Builder::new_current_thread()
            .enable_all()
            .build()
            .context("Failed to create current thread runtime for messaging context.")?;

        // We create a single threaded tokio runtime here because the internal logic for handling
        // ALSA audio devices uses `*mut ()` and other mutable pointers which are not `Send`.
        std::thread::spawn(move || {
            let local = tokio::task::LocalSet::new();
            local.spawn_local(async move {
                while let Some(msg) = rx.recv().await {
                    ctx.handle_msg(msg).await;
                }
            });

            rt.block_on(local);
        });

        CTX.set(tx).map_err(|_| {
            return anyhow!("Ctx::init should only be called once during application startup.");
        })?;
        return Ok(());
    }

    pub async fn handle_msg(&mut self, msg: CtxMsg) {
        match msg {
            CtxMsg::TryRegisterDevice((id, cfg)) => {
                // TODO
                let restored_state = AudioInfoFull::default();
                let restored_queue = Vec::new();

                match AudioPlayer::try_new(id.clone(), cfg, restored_state, restored_queue) {
                    Ok(AudioPlayerNewOut {
                        playback_action_tx,
                        queue_info,
                        playback_info,
                        volume,
                    }) => {
                        let chan = PlaybackChannel {
                            tx: playback_action_tx,
                        };

                        self.channels.playback_channels.insert(id.clone(), chan);
                        self.views.device_state.insert(
                            id,
                            DeviceView {
                                queue: queue_info,
                                playback: playback_info,
                                volume,
                            },
                        );
                    }
                    Err(err) => {
                        panic!("TODO: {err}");
                    }
                };
            }
            CtxMsg::DoDevicePlaybackAction((id, action)) => {
                if let Some(chan) = self.channels.playback_channels.get(&id) {
                    let res = chan.tx.send(action).await;

                    if let Err(err) = res {
                        todo!("{}", err);
                    }
                }
            }
            CtxMsg::NotifyChangedDeviceInfo(_) => {
                // TODO
            }
        }
    }
}
