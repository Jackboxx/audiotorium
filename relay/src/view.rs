use std::{
    ops::{Deref, DerefMut},
    sync::{Arc, RwLock},
};

pub struct View;

/// Mutable access handle to data that can have an arbitrary number of readonly viewers.
#[derive(Debug)]
pub struct ViewMut<T> {
    data: Arc<RwLock<T>>,
}

/// Readonly access handle to data that can have only on owning writer.
#[derive(Debug, Clone)]
pub struct ViewReadonly<T> {
    data: Arc<RwLock<T>>,
}

impl View {
    #[allow(clippy::new_ret_no_self)]
    pub fn new<T>(data: T) -> (ViewMut<T>, ViewReadonly<T>) {
        let data = Arc::new(RwLock::new(data));
        return (ViewMut { data: data.clone() }, ViewReadonly { data });
    }
}

impl<T> ViewMut<T> {
    pub fn view(&self) -> impl Deref<Target = T> + '_ {
        return self.data.read().expect("Thread holding data paniced.");
    }

    pub fn get_mut(&mut self) -> impl DerefMut<Target = T> + '_ {
        return self.data.write().expect("Thread holding data paniced.");
    }

    /// Return a new readonly handle to the data stored in this [`ViewMut`].
    pub fn readonly(&self) -> ViewReadonly<T> {
        return ViewReadonly {
            data: self.data.clone(),
        };
    }
}

impl<T> ViewReadonly<T> {
    pub fn view(&self) -> impl Deref<Target = T> + '_ {
        return self.data.read().expect("Thread holding data paniced.");
    }
}
