#! /bin/sh

sqlx_offline="$1"

# webscraper
docker buildx build --tag audiotorium-webscraper . -f docker/webscraper/Dockerfile

# gateway
docker buildx build --tag audiotorium-gateway . -f docker/gateway/Dockerfile --build-arg SQLX_OFFLINE=$sqlx_offline
