CREATE TABLE IF NOT EXISTS audio_playlist (
    identifier VARCHAR(512) PRIMARY KEY,
    name VARCHAR(255),
    author VARCHAR(255),
    cover_art_url VARCHAR(512)
);


CREATE TABLE IF NOT EXISTS audio_playlist_item (
    playlist_identifier VARCHAR(512),
    item_identifier VARCHAR(512),
    CONSTRAINT fk_audio_playlist FOREIGN key (playlist_identifier) REFERENCES audio_playlist (identifier) ON DELETE cascade,
    CONSTRAINT fk_audio_metadata FOREIGN key (item_identifier) REFERENCES audio_metadata (identifier) ON DELETE cascade,
    PRIMARY KEY (playlist_identifier, item_identifier)
);
