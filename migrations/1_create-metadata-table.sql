CREATE TABLE IF NOT EXISTS audio_metadata (
    identifier VARCHAR(512) PRIMARY KEY,
    name VARCHAR(255),
    author VARCHAR(255),
    duration BIGINT,
    cover_art_url VARCHAR(512)
);
