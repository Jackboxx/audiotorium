ALTER TABLE audio_playlist_item
ADD CONSTRAINT unq_audio_playlist_item_position_in_playlist UNIQUE (playlist_identifier, POSITION);
