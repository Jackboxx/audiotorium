CREATE SEQUENCE audio_playlist_item_position_seq;


ALTER TABLE audio_playlist_item
ALTER COLUMN POSITION
SET DEFAULT nextval('audio_playlist_item_position_seq');


ALTER SEQUENCE audio_playlist_item_position_seq owned by audio_playlist_item.position;
